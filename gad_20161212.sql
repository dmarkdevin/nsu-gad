-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3307
-- Generation Time: Dec 12, 2016 at 07:57 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gad`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_calendar`
--

CREATE TABLE `tbl_calendar` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  `user_id` int(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `publish` int(255) NOT NULL,
  `url` varchar(255) COLLATE utf8_bin NOT NULL,
  `allDay` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(255) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `details` varchar(1000) NOT NULL,
  `date_modified` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  `user_id` int(255) NOT NULL,
  `publish` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment`
--

CREATE TABLE `tbl_comment` (
  `id` int(255) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  `date_modified` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  `user_id` int(255) NOT NULL,
  `forum_id` int(255) NOT NULL,
  `type` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_downloads`
--

CREATE TABLE `tbl_downloads` (
  `id` int(255) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `content` text NOT NULL,
  `date_modified` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  `user_id` int(255) NOT NULL,
  `publish` int(255) NOT NULL,
  `category_id` int(255) NOT NULL,
  `file` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_forum`
--

CREATE TABLE `tbl_forum` (
  `id` int(255) NOT NULL,
  `topic` varchar(1000) DEFAULT NULL,
  `content` text,
  `category` varchar(1000) DEFAULT NULL,
  `publish` int(255) DEFAULT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_messages`
--

CREATE TABLE `tbl_messages` (
  `id` int(255) NOT NULL,
  `subject` varchar(1000) DEFAULT NULL,
  `message` varchar(1000) DEFAULT NULL,
  `sender_id` varchar(1000) DEFAULT NULL,
  `sender_type` varchar(1000) DEFAULT NULL,
  `receiver_id` varchar(1000) DEFAULT NULL,
  `receiver_type` varchar(1000) DEFAULT NULL,
  `read_message` varchar(1000) DEFAULT '0',
  `type` varchar(1000) DEFAULT NULL,
  `detail` text,
  `active` int(255) DEFAULT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE `tbl_news` (
  `id` int(255) NOT NULL,
  `title` varchar(1000) DEFAULT NULL,
  `content` text,
  `image` varchar(1000) DEFAULT NULL,
  `category` varchar(1000) DEFAULT NULL,
  `publish` int(255) DEFAULT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `file` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_students`
--

CREATE TABLE `tbl_students` (
  `id` int(255) NOT NULL,
  `name` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `username` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `contact_number` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `birthdate` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `type` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `detail` text CHARACTER SET latin1,
  `active` int(255) NOT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `CODE` varchar(10000) CHARACTER SET latin1 DEFAULT NULL,
  `SEX` varchar(10000) CHARACTER SET latin1 DEFAULT NULL,
  `CRSCD` varchar(10000) CHARACTER SET latin1 DEFAULT NULL,
  `YEAR` varchar(10000) CHARACTER SET latin1 DEFAULT NULL,
  `SECCD` varchar(10000) CHARACTER SET latin1 DEFAULT NULL,
  `COLLEGE` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_teachers`
--

CREATE TABLE `tbl_teachers` (
  `id` int(255) NOT NULL,
  `name` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `username` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `contact_number` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `birthdate` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `type` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `detail` text CHARACTER SET latin1,
  `active` int(255) NOT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `CODE` varchar(10000) CHARACTER SET latin1 DEFAULT NULL,
  `SEX` varchar(10000) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(255) NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `username` varchar(1000) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `email` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `detail` text,
  `active` int(255) NOT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `contact_number` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `name`, `username`, `password`, `email`, `type`, `detail`, `active`, `user_id`, `date_added`, `date_modified`, `contact_number`) VALUES
(1, 'administrator', 'admin', 'muHxjrjm4UEplENV9URMeRCbmIEgLlBxi/EunxlPWz0=', 'administrator@gad.nsu.edu.ph', 'administrator', NULL, 1, 1, '2016-12-12 02:56:40', '2016-10-21 00:00:00', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_calendar`
--
ALTER TABLE `tbl_calendar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_downloads`
--
ALTER TABLE `tbl_downloads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_forum`
--
ALTER TABLE `tbl_forum`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_news`
--
ALTER TABLE `tbl_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_students`
--
ALTER TABLE `tbl_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_teachers`
--
ALTER TABLE `tbl_teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_calendar`
--
ALTER TABLE `tbl_calendar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_downloads`
--
ALTER TABLE `tbl_downloads`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_forum`
--
ALTER TABLE `tbl_forum`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tbl_news`
--
ALTER TABLE `tbl_news`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_students`
--
ALTER TABLE `tbl_students`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_teachers`
--
ALTER TABLE `tbl_teachers`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
