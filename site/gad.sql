-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3307
-- Generation Time: Nov 29, 2016 at 12:42 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gad`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_calendar`
--

CREATE TABLE `tbl_calendar` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  `user_id` int(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `publish` int(255) NOT NULL,
  `url` varchar(255) COLLATE utf8_bin NOT NULL,
  `allDay` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_calendar`
--

INSERT INTO `tbl_calendar` (`id`, `title`, `start`, `end`, `user_id`, `date_added`, `date_modified`, `publish`, `url`, `allDay`) VALUES
(1, 'asd', '2016-11-27 00:00:00', '2016-11-29 23:59:59', 1, '2016-11-27 02:16:30', '0000-00-00 00:00:00', 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(255) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `details` varchar(1000) NOT NULL,
  `date_modified` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  `user_id` int(255) NOT NULL,
  `publish` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment`
--

CREATE TABLE `tbl_comment` (
  `id` int(255) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  `date_modified` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  `user_id` int(255) NOT NULL,
  `forum_id` int(255) NOT NULL,
  `type` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_comment`
--

INSERT INTO `tbl_comment` (`id`, `comment`, `date_modified`, `date_added`, `user_id`, `forum_id`, `type`) VALUES
(1, 'sample sample', '0000-00-00 00:00:00', '2016-11-09 20:38:55', 1, 1, 'admin'),
(2, 'sample ', '0000-00-00 00:00:00', '2016-11-09 20:48:29', 1, 2, 'admin'),
(3, 'asdasdadadsasd', '0000-00-00 00:00:00', '2016-11-09 21:04:00', 1, 2, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_downloads`
--

CREATE TABLE `tbl_downloads` (
  `id` int(255) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `content` text NOT NULL,
  `date_modified` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  `user_id` int(255) NOT NULL,
  `publish` int(255) NOT NULL,
  `category_id` int(255) NOT NULL,
  `file` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_downloads`
--

INSERT INTO `tbl_downloads` (`id`, `name`, `content`, `date_modified`, `date_added`, `user_id`, `publish`, `category_id`, `file`) VALUES
(1, 'SAMPLE', '', '0000-00-00 00:00:00', '2016-11-26 04:38:28', 1, 1, 0, 'DIRECTORY.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_forum`
--

CREATE TABLE `tbl_forum` (
  `id` int(255) NOT NULL,
  `topic` varchar(1000) DEFAULT NULL,
  `content` text,
  `category` varchar(1000) DEFAULT NULL,
  `publish` int(255) DEFAULT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_forum`
--

INSERT INTO `tbl_forum` (`id`, `topic`, `content`, `category`, `publish`, `user_id`, `date_added`, `date_modified`) VALUES
(2, 'http://localhost:8080/gad/site/main/forum', 'sample topic', NULL, 1, 1, '2016-11-09 20:46:37', NULL),
(3, 'dumanjug', 'dumanjug', NULL, 1, 1, '2016-11-09 20:47:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE `tbl_news` (
  `id` int(255) NOT NULL,
  `title` varchar(1000) DEFAULT NULL,
  `content` text,
  `image` varchar(1000) DEFAULT NULL,
  `category` varchar(1000) DEFAULT NULL,
  `publish` int(255) DEFAULT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `file` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_news`
--

INSERT INTO `tbl_news` (`id`, `title`, `content`, `image`, `category`, `publish`, `user_id`, `date_added`, `date_modified`, `file`) VALUES
(1, 'SAMPLE', 'E-Learning Module is a tool that provides course materials in a logical, sequential, order, guiding students through the content and assessments in the order specified by the instructor. Instructors can insert formatted text, files, Discussion Topics, Tests & Quizzes. Content can be structured in such a way as to require students to complete content before they are allowed to proceed to the next content. It is also possible for instructors to set up a place for students to add content to the Learning Module.\r\n\r\n', NULL, NULL, 1, 1, '2016-11-08 21:48:49', NULL, 'xoiawSR.jpg'),
(2, 'ABOUT COMPANY', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. \r\n\r\nLorem Ipsum has been the industryÃ¢â‚¬â„¢s standard dummy text ever since the 1500s.', NULL, NULL, 1, 1, '2016-11-08 21:49:15', '2016-11-09 20:21:35', 'Picture 001.jpg'),
(3, 'WIKIHOW TO WRITE A NEWSPAPER', 'Creating your own a newspaper is the dream of journalists worldwide. Controlling your message, seeing your name in print, and exposing injustices other publications have yet to write about are just a few of the benefits to writing your own paper, but it will not come easily. You need staff, time, money, and dedication to your message to survive in the competitive media market, but you''ll be halfway there if you follow these steps.', NULL, NULL, 1, 1, '2016-11-10 07:44:26', NULL, 'siq.jpg'),
(4, 'SAMPLE', 'sample', NULL, NULL, 1, 1, '2016-11-28 05:59:43', NULL, 'images.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_students`
--

CREATE TABLE `tbl_students` (
  `id` int(255) NOT NULL,
  `name` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `username` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `contact_number` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `birthdate` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `type` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `detail` text CHARACTER SET latin1,
  `active` int(255) NOT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `CODE` varchar(10000) CHARACTER SET latin1 DEFAULT NULL,
  `SEX` varchar(10000) CHARACTER SET latin1 DEFAULT NULL,
  `CRSCD` varchar(10000) CHARACTER SET latin1 DEFAULT NULL,
  `YEAR` varchar(10000) CHARACTER SET latin1 DEFAULT NULL,
  `SECCD` varchar(10000) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `tbl_students`
--

INSERT INTO `tbl_students` (`id`, `name`, `username`, `password`, `email`, `contact_number`, `birthdate`, `type`, `detail`, `active`, `user_id`, `date_added`, `date_modified`, `CODE`, `SEX`, `CRSCD`, `YEAR`, `SECCD`) VALUES
(1031, 'NAME', 'CODE', 'hhUHdLlzmKVfZj3qZcR7LrCOHFZoi4u2LsHy40NeUd0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 'CODE', 'SEX', 'CRSCD', 'YEAR', 'SECCD'),
(1032, 'ARAGON, REALYN A.', '16100572', 'BNSiGTeF3AK+ekv3f7+potqDFy1tZ2TbEiLTLdmDERo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00572', 'F', 'BSCS', '1', 'BSCS 1A'),
(1033, 'Asugas, John Kim Tarrayo', '12101886', 'vMpcICl0+b1NrzTc5xMrarjf5GESXsPWNGdAgRWdzxc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01886', 'M', 'BSCS', '1', 'BSCS 3B'),
(1034, 'Azur, Jefferson Agas', '13100903', 'xOdHbvBAbgx2uLa+ZPBQse2D3vDWvDTju+40v7KS7P4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00903', 'M', 'BSCS', '1', 'BSCS 3B'),
(1035, 'Bentoy, Gerwin Dadizon', '13101983', 'Fe11dLaW/aedHH4bYO8bteiPvlVLZqzdgKcr2y9jy3o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01983', 'M', 'BSCS', '1', 'BSCS 4A'),
(1036, 'Bornillo, Kent Ian Mendoza', '16100333', 'WaMbJslAv56CeG5pbh3Q/ajPb40iFfZIi1zrm/kL70s=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00333', 'M', 'BSCS', '1', 'BSCS 1A'),
(1037, 'Caballero, Irene Narciso', '15200022', 'jQn0CUTOSMaj2yflIBdiDBoQ/TM1Al54HKkUvs8zwfk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00022', 'F', 'BSCS', '1', 'BSCS 1A'),
(1038, 'CALVEZ, JONATHAN C.', '16100559', 'J6P1z3OKxK2MIwy3Fs+BTZFif+BT2XYE/0ZJKrmBx/o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00559', 'M', 'BSCS', '1', 'BSCS 1A'),
(1039, 'Catigbe, Caroline  Garin', '07100258', 'tBhxO/E4BqUG96uGq9DRsGpw2EWbLHoBMMlytPJTFvk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '07-1-00258', 'F', 'BSCS', '1', 'BSCS 4A'),
(1040, 'Ceniza, Mycka Balame', '13101643', 'zI95YTFJ63VYMIimmn7N4T1bbgvUeg0Uihn6CC1T1jU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01643', 'F', 'BSCS', '1', 'BSCS 3B'),
(1041, 'Cezar, James Carlo Jamin', '09101456', 'AG6AqIKwQcS2OU8JqmU/fGcwKPGsiSjI1zxuwLS6cBc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '09-1-01456', 'M', 'BSCS', '1', 'BSCS 1A'),
(1042, 'COMIA, CHRISTINE KEY', '16100590', '3J4EoODdWrvoCv3xVV/ail3fShsBT0Y5Md1kwzhKnFk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00590', 'F', 'BSCS', '1', 'BSCS 1A'),
(1043, 'Cosmiano, Myrian Joyce Lovitos', '16100331', 't0CNd2o/+O+hiDI4OLzVhcVfwm7aAICipRjJakMzJ+o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00331', 'F', 'BSCS', '1', 'BSCS 1A'),
(1044, 'Del Rosario, Mabelle  Ceb', '05101121', 'J9G5GmnX6J4aTzuMsrrzenEyWtk/vKfsHRcyhuq9WVc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '05-1-01121', 'F', 'BSCS', '1', 'BSCS 1A'),
(1045, 'Diaz, Allan Fem  Sumayan', '07100887', '1jTEy+LDeBy4te0w9eaGMfjLBdgpg2N51rfV/cMm2J4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '07-1-00887', 'M', 'BSCS', '1', 'BSCS 1A'),
(1046, 'Donghil, Janus Van', '16100925', '/ZzjZy479BvXAslKvI114dD5xaJfJhoyX7W20MarF4U=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00925', 'M', 'BSCS', '1', 'BSCS 1A'),
(1047, 'Dotimas, Jacquiline C.', '12100463', 'sqNM/gxT7HWHaCF1GO7N2BrwAm+HGwP9/vL/NJu0BhU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00463', 'F', 'BSCS', '1', 'BSCS 4A'),
(1048, 'Ejorcadas, Judy O.', '14102290', 'pXZhqe3UeM1rnZSNYnTJkiNPWdPdcg7bnPyuq528KlI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02290', 'M', 'BSCS', '1', 'BSCS 1A'),
(1049, 'Elardo, Julito', '16100006', '9sEbDBljdmrD27HsUGitwVhpC4FC9PayeWg43qL4gOY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00006', 'M', 'BSCS', '1', 'BSCS 1A'),
(1050, 'ESPUERTA, SWEET R.', '16100574', 'o7Ov7AvZXQIn5xhsXEvO/IgIfTixYHdKyuhFxz+9QkY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00574', 'F', 'BSCS', '1', 'BSCS 1A'),
(1051, 'Etang, Daryl', '16100017', '/Pm5BlDjVLHEWD5bqcLtklNECXXgpawWpq2iVineBao=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00017', 'M', 'BSCS', '1', 'BSCS 1A'),
(1052, 'Fuertes, Leonard Vaporoso', '16100329', 'muuHtAQ8dWlZEePEhL0jFVH+KlFvypIfkNYoqGCbO30=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00329', 'M', 'BSCS', '1', 'BSCS 1A'),
(1053, 'Gallosa, Orly', '15101113', 'bCxxq5elwyPKhrVW+ySeM972+ErT9wx7Ollk6nzG1ZE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01113', 'M', 'BSCS', '1', 'BSCS 1A'),
(1054, 'Gamana, Jezrel C.', '14100877', '0I/IBMeIPH0udEYvyoDfK5acHSzEsw2GJQQjoPkRwlM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00877', 'M', 'BSCS', '1', 'BSCS 1A'),
(1055, 'Geraldez, Flordelito,', '12101771', 'tBhxO/E4BqUG96uGq9DRsGpw2EWbLHoBMMlytPJTFvk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01771', 'M', 'BSCS', '1', 'BSCS 1A'),
(1056, 'Juntilla, Rizza Ferleene A.', '13200007', 'KzpBI6+XLE/j9AshFw5F3YAKx7quNA67saP8GYAQadk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-2-00007', 'F', 'BSCS', '1', 'BSCS 1A'),
(1057, 'Lloveras, Leonilyn Logro', '16100336', 'vMpcICl0+b1NrzTc5xMrarjf5GESXsPWNGdAgRWdzxc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00336', 'F', 'BSCS', '1', 'BSCS 1A'),
(1058, 'LOMOT, MA. NIKKIE C.', '16100879', 'OsN2LysCVox6NAZ9Z121aZkpdyscMpYQxe6TkwCEaBQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00879', 'F', 'BSCS', '1', 'BSCS 1A'),
(1059, 'Lucenio, Herchie Montecillo', '13102401', 'hhUHdLlzmKVfZj3qZcR7LrCOHFZoi4u2LsHy40NeUd0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02401', 'M', 'BSCS', '1', 'BSCS 4A'),
(1060, 'Macabacyao, Angelou Baylon', '13101057', 'r0DWcN7r7+is9qjBR7NKMVf7VLezvJ1EmjbeCHnLCQk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01057', 'F', 'BSCS', '1', 'BSCS 1A'),
(1061, 'MADEJA, RENZ JOVEN S', '16100850', 'Iy4Ts2sRBUJNpssvc5s6RiRLwiC8IQkWJd4oQ9v8bxg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00850', 'M', 'BSCS', '1', 'BSCS 1A'),
(1062, 'Mangubat, Wilson Patillas', '16100757', 'J6P1z3OKxK2MIwy3Fs+BTZFif+BT2XYE/0ZJKrmBx/o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00757', 'M', 'BSCS', '1', 'BSCS 1A'),
(1063, 'MAQUINANO, MELANNE V.', '16100568', 'GQ9NjQSn7UclAbJJkgdrb2TUXD7z/BZRv0ra9vT3emw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00568', 'F', 'BSCS', '1', 'BSCS 1A'),
(1064, 'Meno, Nico Andrew C.', '15200009', 'yDqUiaGE2D7RIU6XOL+gaTeOYZfshBflp93ETMfOFAY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00009', 'M', 'BSCS', '1', 'BSCS 1A'),
(1065, 'Mollare, Bernard Pobadora', '16100335', 'FcfzchHUwsEL/fph5kUvmAuN4NEDeIYqWUx7Xz2llVk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00335', 'M', 'BSCS', '1', 'BSCS 1A'),
(1066, 'Niedo, Von Joshua Salentes', '15200040', 'lWZRAn/K8YqTChTqHOoYPmpkv4TnbaTXPICM0ktXNIQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00040', 'M', 'BSCS', '1', 'BSCS 1A'),
(1067, 'Nies, Bryan Gerzon', '15101721', '6pJ7aKwyhGtIfn0fqI/23W+TybsfPfAQle7A+Qx/Afo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01721', 'M', 'BSCS', '1', 'BSCS 1A'),
(1068, 'Olaer, Frances Arneliah Orcullo', '13102407', 'OiKrEgLd470jNrLdtRo22GCN4mFmX1Nq7qk6t6/63ZM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02407', 'F', 'BSCS', '1', 'BSCS 1A'),
(1069, 'Oledan, Annaliza Avelino', '15200025', 'gRGYy2X7bpLa4rNP5znhj8ioxQPwaO0jVS5UIbonfk8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00025', 'F', 'BSCS', '1', 'BSCS 1A'),
(1070, 'Omeriz, Loreni', '16100016', 'iR++0ZuowHS3r4K3L8MHMHVoWUh/OOPXcU18MBkGDeA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00016', 'M', 'BSCS', '1', 'BSCS 1A'),
(1071, 'Opeña, John Chester', '15102067', 'sLQU6I+fqPelcAkbevVsCaw6VXzF+TnX16D8WTnIrTo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02067', 'M', 'BSCS', '1', 'BSCS 1A'),
(1072, 'Payos, Syros M.', '16100326', 's+BodtGax40ETbHn16/1gUkohLSNZi/F6qtUuvIVUEM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00326', 'M', 'BSCS', '1', 'BSCS 1A'),
(1073, 'Po, Francis Jeff', '13100038', 'an4VPFL/mWybyr91YXe0Mi6vaUYmQ69KtCgyPRBC88Y=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00038', 'M', 'BSCS', '1', 'BSCS 1A'),
(1074, 'Rosal, Bryan T.', '15100897', 'VOPeC0a++ktnk9hkX1cdowS5NI8JZVe0k5L2EH2DEwA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00897', 'M', 'BSCS', '1', 'BSCS 1A'),
(1075, 'Rosales, Roselyn Y.', '15200140', 'hhUHdLlzmKVfZj3qZcR7LrCOHFZoi4u2LsHy40NeUd0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00140', 'F', 'BSCS', '1', 'BSCS 1A'),
(1076, 'Sacedor, Colin', '16100013', 'Y0oP3RmIPC95g7Gs6TL5H+jmFBdpIxc8n2/vpgrFfR4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00013', 'F', 'BSCS', '1', 'BSCS 1A'),
(1077, 'SULAYAO, ANALIE ALVERIO', '16100575', 'IoBctdth1HzADGrAuQaL+rnRs0k/k4B2QvYydwr7baw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00575', 'F', 'BSCS', '1', 'BSCS 1A'),
(1078, 'Tagalog, Jebran', '12101405', 'yNlGLe1geNoXConxbt7j0dByFwOnJg9XMQs94ZkgK5E=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01405', 'M', 'BSCS', '1', 'BSCS 1A'),
(1079, 'VERBA, JENNY ROSE C.', '16100589', 'hhUHdLlzmKVfZj3qZcR7LrCOHFZoi4u2LsHy40NeUd0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00589', 'F', 'BSCS', '1', 'BSCS 1A'),
(1080, 'Yape, Joe Doble', '13100078', 'OhqOJtTJObj7RhnqiNgQxawErT4qvtipyrYqLf81rVc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00078', 'M', 'BSCS', '1', 'BSCS 1A'),
(1081, 'Yu, Philip Franck Rodvick Gervacio', '16100422', 'OsGL6SXIiD5IDAK+gka48Lvg/iXz4SvCs7EWAQuHvCs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00422', 'M', 'BSCS', '1', 'BSCS 1A'),
(1082, 'Abad, Gerald F.', NULL, 'muHxjrjm4UEplENV9URMeRCbmIEgLlBxi/EunxlPWz0=', '', NULL, '', NULL, NULL, 1, 1, NULL, '2016-11-09 22:13:00', '09-1-01497', 'M', 'BSCS', '2', 'BSCS 2C'),
(1083, 'Abrigo, Jonalyn S.', '15100291', '0UpMNH+1rov6fXgSh44HONBxj49a6hXpmu13jIX1AK8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00291', 'F', 'BSCS', '2', 'BSCS 2A'),
(1084, 'Agang, Janna T.', '15100134', 'Jku8dDscu/8FhWk6KHK5KX2lLLwvfzqA9P2Jx7v4YcY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00134', 'F', 'BSCS', '2', 'BSCS 2A'),
(1085, 'Aguila, Trixie Garcia', '11101482', 's4wsxsx0SUH7jfs5uRRKCSn2Vupvays/Xe+vZPG7RYw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-01482', 'F', 'BSCS', '2', 'BSCS 2A'),
(1086, 'Ali, Elias E.', '14100896', 'brBOynzWr1Hh+uL9v8piyjjfyDivr8gXSym+clUwNaI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00896', 'M', 'BSCS', '2', 'BSCS 3B'),
(1087, 'Alverio, Geraldine B.', '15100652', 'IjrPknVy/knvjNfmCCUdeDktfGjyD5TIg1XIsD2PPn8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00652', 'F', 'BSCS', '2', 'BSCS 2B'),
(1088, 'Anonat, Ma. Twinkle I.', '15102054', 'TpMr5MsSgPSPsWxNRVZHWGOalq4jBnyGbIxF11fJYQo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02054', 'F', 'BSCS', '2', 'BSCS 2C'),
(1089, 'Aranzado, Glenda Q.', '15100825', 'IdoKOPp2M+wTMSinJgoJwiD/PfiatqTEzGuQc8czJzk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00825', 'F', 'BSCS', '2', 'BSCS 2A'),
(1090, 'Avenido, Maribel Oniot', '13100748', 'Qik5EoP7etWJB+PHZaWgDYblu7WJ0h3znkYSLDEZ6TE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00748', 'F', 'BSCS', '2', 'BSCS 2A'),
(1091, 'Bacongallo, Shela Mae P.', '15100175', 'v5H284vcaAfaZQFsH1vBqmVxsvNRp6ICBx9thbwzGIs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00175', 'F', 'BSCS', '2', 'BSCS 2A'),
(1092, 'Boragay, Gilda G.', '15101769', 'tILKDBciQGAmGcCNPrJE4kmhjQLaeKNBYzuCuSHdv6o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01769', 'F', 'BSCS', '2', 'BSCS 2B'),
(1093, 'Brenga, Sunday C.', '15100230', 'AjXs1ER3HA+4KLBwtKiGnM4ouWqXDsZ1P+Hrc1lS+Ac=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00230', 'M', 'BSCS', '2', 'BSCS 2A'),
(1094, 'Burre, Joisie G.', '15100631', 'RsqxltYn/nORtUdNxBXc3dqKT7OOaPjKA77xJ5MhNH4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00631', 'F', 'BSCS', '2', 'BSCS 2A'),
(1095, 'Burre, Laurence  Sevillan', '10100767', 't0CNd2o/+O+hiDI4OLzVhcVfwm7aAICipRjJakMzJ+o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '10-1-00767', 'M', 'BSCS', '2', 'BSCS 2A'),
(1096, 'Cabalquinto Jonathan Narrido', '13101954', 'KzpBI6+XLE/j9AshFw5F3YAKx7quNA67saP8GYAQadk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01954', 'M', 'BSCS', '2', 'BSCS 2A'),
(1097, 'Cabillan, Rhey Dominic T.', '15100281', 'vMpcICl0+b1NrzTc5xMrarjf5GESXsPWNGdAgRWdzxc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00281', 'M', 'BSCS', '2', 'BSCS 2A'),
(1098, 'Canencia, Vince V.', '15101528', 'f4aja3A0OjbXHwU1JFJcsLtiCnMxLVg2gD7xbpPOfQk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01528', 'M', 'BSCS', '2', 'BSCS 2B'),
(1099, 'Cano, Ramil B.', '13100006', 'Rs0ExdPV605L+kHYmjGkU7BC6N+pU43hfMQkiB3lFxs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00006', 'M', 'BSCS', '2', 'BSCS 3A'),
(1100, 'Carbo,Domilyn M.', '15100097', '8x8OYEwFgk6ecUBF0vOx0H4rFl3UiUgQ72Tyj9EqIqw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00097', 'F', 'BSCS', '2', 'BSCS 2A'),
(1101, 'Cator, Marian N.', '15100186', 'AjXs1ER3HA+4KLBwtKiGnM4ouWqXDsZ1P+Hrc1lS+Ac=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00186', 'F', 'BSCS', '2', 'BSCS 2B'),
(1102, 'Cinco, Pearl Marie B.', '15100644', 'tIgkU0lPKZaIGfucdrMcNHU0xDxa49sGAeJZDKxdhSs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00644', 'F', 'BSCS', '2', 'BSCS 2A'),
(1103, 'Conise, Reywel Mae A.', '14102402', '6gipd8TjibmZ1EZLSI8p20h/+qxtVUmE7PlaQ9XLeGI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02402', 'F', 'BSCS', '2', 'BSCS 2A'),
(1104, 'Corpin, Dave D.', '15100671', 'Y0oP3RmIPC95g7Gs6TL5H+jmFBdpIxc8n2/vpgrFfR4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00671', 'M', 'BSCS', '2', 'BSCS 2A'),
(1105, 'Corpin, Erlinda R.', '15100697', 'lJCSvv/ZpLyJDuQWZJ+tMVpNvqz8x5CKNDxl8a/gMYo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00697', 'F', 'BSCS', '2', 'BSCS 2A'),
(1106, 'Corpin, Jane Claire D.', '15101812', 'vRDIZUMuS3+BYBFlQsxFRCBiOhn+blJQINsdZaZFpSU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01812', 'F', 'BSCS', '2', 'BSCS 2B'),
(1107, 'Corpin, Jeppy Ngoho', '08100761', 'pymVmQOUItBYUU8pNQZutwAV9fbMpmruttR2WVXvlhg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '08-1-00761', 'M', 'BSCS', '2', 'BSCS 2A'),
(1108, 'Corpin, Renalyn C.', '15101077', '+VwzdT96VlaxOj29CRtyTvhxR/Bv53fLqGz7eZq+nfY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01077', 'F', 'BSCS', '2', 'BSCS 2A'),
(1109, 'Correa, Valerie Jade P.', '15100768', 'FcfzchHUwsEL/fph5kUvmAuN4NEDeIYqWUx7Xz2llVk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00768', 'F', 'BSCS', '2', 'BSCS 2B'),
(1110, 'Costelo, Cristene Joy M.', '15100907', 'ecp1xsUpcrxBOWsFOgFWIq2NgjVrN8ovjFIkNpUyYNk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00907', 'F', 'BSCS', '2', 'BSCS 2A'),
(1111, 'Costelo, Jonna E.', '15101988', 'zN+5+2eKZwYlahFqPL0wJCU7y4PjClZ2ObzKylXTlOc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01988', 'F', 'BSCS', '2', 'BSCS 2A'),
(1112, 'Cuizon, Ednalyn', '15100740', '+VwzdT96VlaxOj29CRtyTvhxR/Bv53fLqGz7eZq+nfY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00740', 'F', 'BSCS', '2', 'BSCS 2A'),
(1113, 'Daragosa, Margie M.', '15102223', 'tBfqXoF/WPhTq98yLXkWgD0mpdcfKfN37AgUFnLDAck=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02223', 'F', 'BSCS', '2', 'BSCS 2B'),
(1114, 'De La Serna, Edmark F.', '15102215', 'CmkO98YynXS7Omp+ujakzl7xVZq+rZ5nsiCLEXYKyq8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02215', 'M', 'BSCS', '2', 'BSCS 2B'),
(1115, 'De Paz Leonardo Jr A.', '15102177', 'tBfqXoF/WPhTq98yLXkWgD0mpdcfKfN37AgUFnLDAck=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02177', 'M', 'BSCS', '2', 'BSCS 2B'),
(1116, 'Espuerta, Sedrick A.', '15100702', 'KzpBI6+XLE/j9AshFw5F3YAKx7quNA67saP8GYAQadk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00702', 'M', 'BSCS', '2', 'BSCS 2B'),
(1117, 'Esquillo, Princess Sarah Mae S.', '15101169', 'IdoKOPp2M+wTMSinJgoJwiD/PfiatqTEzGuQc8czJzk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01169', 'F', 'BSCS', '2', 'BSCS 2B'),
(1118, 'Estrada, Jewel-ann A.', '15100304', 'gqGjNioBfvXlvXDwuMnXqoeEdh7b1Sre2dadtVBe5U8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00304', 'F', 'BSCS', '2', 'BSCS 2B'),
(1119, 'Etang, Jennifer N.', '15102338', '/abA7h5Sy7FUbiUgM2WWpBUnCHLoxROFyzRncedQESM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02338', 'F', 'BSCS', '2', 'BSCS 2B'),
(1120, 'Fabros, Phil Joshua M.', '14100546', 'yTLXJS/7GC5ZqZOBVJBx4lE8oqt2gdDC71mdrcv+I7k=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00546', 'M', 'BSCS', '2', 'BSCS 2B'),
(1121, 'Fajardo, Nelia G.', '15100110', 's4wsxsx0SUH7jfs5uRRKCSn2Vupvays/Xe+vZPG7RYw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00110', 'F', 'BSCS', '2', 'BSCS 2B'),
(1122, 'Fajardo, Rod Anthony', '15100668', 'xOdHbvBAbgx2uLa+ZPBQse2D3vDWvDTju+40v7KS7P4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00668', 'M', 'BSCS', '2', 'BSCS 2A'),
(1123, 'Fariol, Carl Hentjie S.', '14200016', 'tBhxO/E4BqUG96uGq9DRsGpw2EWbLHoBMMlytPJTFvk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-2-00016', 'M', 'BSCS', '2', 'BSCS 2B'),
(1124, 'Floro, Mikkey Andrea A.', '15102167', '8x8OYEwFgk6ecUBF0vOx0H4rFl3UiUgQ72Tyj9EqIqw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02167', 'F', 'BSCS', '2', 'BSCS 2B'),
(1125, 'Gado, Christine U.', '15102074', 'g54oJtgpIDypQ8bJqZQsygjgjY6Lw85IProfWm2gi08=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02074', 'F', 'BSCS', '2', 'BSCS 2C'),
(1126, 'Gahera, Mary Jandra Kaagbay', '15101356', 'xAt9bVgrFS4k5GQdaUsHjHUqf9FHxOhGO1LYzrAygG4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01356', 'F', 'BSCS', '2', 'BSCS 2A'),
(1127, 'Ibano, Oliver  Pedrano', '03100295', 'ClRkoQ37a1TLAKaHcEeaQ0BITef6F1f7rwLmHSZGZ7E=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '03-1-00295', 'M', 'BSCS', '2', 'BSCS 2C'),
(1128, 'Jacobe, Michelle Van B.', '15101547', 'tILKDBciQGAmGcCNPrJE4kmhjQLaeKNBYzuCuSHdv6o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01547', 'F', 'BSCS', '2', 'BSCS 2A'),
(1129, 'Jayan, Joevic V.', '15102276', 'AjXs1ER3HA+4KLBwtKiGnM4ouWqXDsZ1P+Hrc1lS+Ac=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02276', 'M', 'BSCS', '2', 'BSCS 2A'),
(1130, 'Joboco, Jefferson Cañete', '14100288', 'xAt9bVgrFS4k5GQdaUsHjHUqf9FHxOhGO1LYzrAygG4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00288', 'M', 'BSCS', '2', 'BSCS 2B'),
(1131, 'Jumetilco, Mark Gelton V.', '15100995', 'd3XkpfZhVOAbC3VnuOEnwA8EeBZCb8K3dpvphTjoLH4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00995', 'M', 'BSCS', '2', 'BSCS 2A'),
(1132, 'Lajera, Jerlynn JoyceT.', '14100498', 'S+sokmBrgPrEVo9bztUY5EDOhwamkLKJv1z7KcRmVA0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00498', 'F', 'BSCS', '2', 'BSCS 2C'),
(1133, 'Layosa, Franco G.', '14101282', 'lJCSvv/ZpLyJDuQWZJ+tMVpNvqz8x5CKNDxl8a/gMYo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01282', 'M', 'BSCS', '2', 'BSCS 2A'),
(1134, 'Maderazo, Richelle S.', '15100176', 'RsqxltYn/nORtUdNxBXc3dqKT7OOaPjKA77xJ5MhNH4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00176', 'F', 'BSCS', '2', 'BSCS 2A'),
(1135, 'Malinao, Jonathan M.', '15100742', 'PDHvcTQpJWe2TZ513HapL0pJ8Uzr/yrlBdP2ypstbbQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00742', 'M', 'BSCS', '2', 'BSCS 2B'),
(1136, 'Manuel, Adrienne H.', '14200015', 'zjtICfBdRAu/J1Apc+KQT4gwk+UZ+gX4lfd+5XuPK6M=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-2-00015', 'F', 'BSCS', '2', 'BSCS 2C'),
(1137, 'Martos Kennu Mier', '14100303', 'n1SmF7SDv4xn/pmdoaCpsrdh1bYxf7xg3xpJPn8pWZk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00303', 'M', 'BSCS', '2', 'BSCS 2A'),
(1138, 'Medillen, Joshua Mico C.', '14102604', 'vRDIZUMuS3+BYBFlQsxFRCBiOhn+blJQINsdZaZFpSU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02604', 'M', 'BSCS', '2', 'BSCS 2B'),
(1139, 'Merino, Flora Mae A.', '14100592', 'a40ki6qyyOpNK6vksrjc8aJrF/kmTNGzWs/sirAIqJk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00592', 'F', 'BSCS', '2', 'BSCS 2C'),
(1140, 'Mondido, Jessa B.', '15100205', 'OiKrEgLd470jNrLdtRo22GCN4mFmX1Nq7qk6t6/63ZM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00205', 'F', 'BSCS', '2', 'BSCS 2A'),
(1141, 'Monteroyo, Irene A.', '15102339', 'jLVIOeVQNfLz09+bQ42hniZJw3kNvnJh0wKzQe8xLco=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02339', 'F', 'BSCS', '2', 'BSCS 2A'),
(1142, 'Morillo, Mike Garin', '14102572', 'RsqxltYn/nORtUdNxBXc3dqKT7OOaPjKA77xJ5MhNH4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02572', 'M', 'BSCS', '2', 'BSCS 2B'),
(1143, 'Mosqueda, Maricar S.', '15100343', 'Rs0ExdPV605L+kHYmjGkU7BC6N+pU43hfMQkiB3lFxs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00343', 'F', 'BSCS', '2', 'BSCS 2B'),
(1144, 'Naagas, Liza S.', '15102236', '+VwzdT96VlaxOj29CRtyTvhxR/Bv53fLqGz7eZq+nfY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02236', 'F', 'BSCS', '2', 'BSCS 2B'),
(1145, 'Naldoza, Leinard R.', '15100258', 'JUxhHp9/pRBfSwE7Of2HJro/9ogmw37ADVB77sFPxmA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00258', 'M', 'BSCS', '2', 'BSCS 2A'),
(1146, 'Napalit, Mande  Giva', '10101374', 'brBOynzWr1Hh+uL9v8piyjjfyDivr8gXSym+clUwNaI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '10-1-01374', 'M', 'BSCS', '2', 'BSCS 2C'),
(1147, 'Ogania, Franklin Noel L.', '15101399', '4Jq2M9OzQms0wwF+MJP7gJURSG8GaXtdvL1X6Aqm5qM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01399', 'M', 'BSCS', '2', 'BSCS 2A'),
(1148, 'Olea, Faith Roxanne Delovino', '11101379', 'HjkKpZ/1F7DsrbGRGtN9c/13/s9uHvalwr5lkiacAs8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-01379', 'F', 'BSCS', '2', 'BSCS 2A'),
(1149, 'Oliveros, Oliver N.', '15101197', 'AjXs1ER3HA+4KLBwtKiGnM4ouWqXDsZ1P+Hrc1lS+Ac=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01197', 'M', 'BSCS', '2', 'BSCS 2A'),
(1150, 'Ongayo, Gynica C.', '15100201', '6JMarpHvP/nw5y9Zjq93O0b9aHtsCEzWUr50TTOABN0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00201', 'F', 'BSCS', '2', 'BSCS 2B'),
(1151, 'Orbeta, Gaylord Jamin', '12100325', 'hA7cgwwMqqIfDgqcsm+Qlj3VUQ3R8CtfqpQox0TWBSU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00325', 'M', 'BSCS', '2', 'BSCS 2B'),
(1152, 'Osmeña, Candle Lee C.', '15100718', 'YY9zxjs2gZU79dk4ef6kJ3OzunxgP2jriRGcTPz3ugE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00718', 'F', 'BSCS', '2', 'BSCS 2B'),
(1153, 'Otic, Cherry Ann M.', '14101406', 'xuucByx3t2tXN/QfQjodLz+jR3PoL4SGcfYpJtBxGIg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01406', 'F', 'BSCS', '2', 'BSCS 2B'),
(1154, 'Palconit, Cheryl C.', '15101085', 'Qik5EoP7etWJB+PHZaWgDYblu7WJ0h3znkYSLDEZ6TE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01085', 'F', 'BSCS', '2', 'BSCS 2C'),
(1155, 'Patagnan, Wednesmar Moreno', '14102471', '6gipd8TjibmZ1EZLSI8p20h/+qxtVUmE7PlaQ9XLeGI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02471', 'M', 'BSCS', '2', 'BSCS 2A'),
(1156, 'Paulo, Zachary Yues Baguna', '13100059', 'WK8mGr+2JcBx9PSLGZokt4o2jgtpgFO8JLE+hCSTCuc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00059', 'M', 'BSCS', '2', 'BSCS 2B'),
(1157, 'Payos, Nerisa S.', '15101868', 'g54oJtgpIDypQ8bJqZQsygjgjY6Lw85IProfWm2gi08=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01868', 'F', 'BSCS', '2', 'BSCS 2A'),
(1158, 'Payos, Sally B.', '15100346', 'l1pg4nbByfiRkoVVDTx4g0Hs0hzqZXWI0wsvoS9RkOc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00346', 'F', 'BSCS', '2', 'BSCS 2B'),
(1159, 'Pecajas, Niko S.', '15100324', '4f8PAhrTZKOtU77tP3Y4nnqf74ce0TIdzRW4S6NyzI0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00324', 'M', 'BSCS', '2', 'BSCS 2B'),
(1160, 'Peñaflor, Joe Mark A.', '15100335', 'DhfE+G44V45Unqy+Zmafah46GBG2Ms1wpR8+EnIZvT8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00335', 'M', 'BSCS', '2', 'BSCS 2A'),
(1161, 'Piamonte, Jhie Ann T.', '15100673', 'JUxhHp9/pRBfSwE7Of2HJro/9ogmw37ADVB77sFPxmA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00673', 'F', 'BSCS', '2', 'BSCS 2A'),
(1162, 'Portes, Eduard Labor', '13200037', 'vRDIZUMuS3+BYBFlQsxFRCBiOhn+blJQINsdZaZFpSU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-2-00037', 'M', 'BSCS', '2', 'BSCS 2A'),
(1163, 'Rañosa, Angelyn I.', '15100130', 'hhUHdLlzmKVfZj3qZcR7LrCOHFZoi4u2LsHy40NeUd0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00130', 'F', 'BSCS', '2', 'BSCS 2B'),
(1164, 'Rosillo, Ramon Jr. A.', '15101622', '/HSA7OoVQHOhQk/2phrofg7aauD9MyWAOo4Z6WV0xKQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01622', 'M', 'BSCS', '2', 'BSCS 2A'),
(1165, 'Salarza, Rechmond P.', '15101613', 'IoBctdth1HzADGrAuQaL+rnRs0k/k4B2QvYydwr7baw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01613', 'M', 'BSCS', '2', 'BSCS 2C'),
(1166, 'Sales, Pablito B. Jr.', '14200072', '/PHi2N6JY+hPI4tqra1hapWBgQQI7mfANRscVu03TvU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-2-00072', 'M', 'BSCS', '2', 'BSCS 2A'),
(1167, 'Saligumba, Irene P.', '15100114', 'Zz05/Vi2+5WdFqdQ4wR53J4rW8XGiwTwAm3EizaVJDQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00114', 'F', 'BSCS', '2', 'BSCS 2B'),
(1168, 'Salinas, Jerick S.', '15100827', 'KzpBI6+XLE/j9AshFw5F3YAKx7quNA67saP8GYAQadk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00827', 'M', 'BSCS', '2', 'BSCS 2A'),
(1169, 'Tadefa, Cheryl Ann V.', '15101074', 'Hsvg1ul6lA+avH2QPRKF2kLPZg9UaMjz4/spJ2Bt8II=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01074', 'F', 'BSCS', '2', 'BSCS 2B'),
(1170, 'Tambis, Jumel L.', '15100763', 'yDOEMzPI+luLQ1aVdIYug4iF0/krpK1kv81jyiwaehQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00763', 'M', 'BSCS', '2', 'BSCS 2B'),
(1171, 'Techo Marnel Rusiana', '15100756', 'NnwGB0lkXw0Jb/mgnOy+cyMtEbPjOAR3gMUOCW3JVbA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00756', 'F', 'BSCS', '2', 'BSCS 2B'),
(1172, 'Temblor, Mary Jean T.', '15101847', '/HSA7OoVQHOhQk/2phrofg7aauD9MyWAOo4Z6WV0xKQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01847', 'F', 'BSCS', '2', 'BSCS 2B'),
(1173, 'Tonedo, Rosalend L.', '15100649', 'EFS/DpujZ2XmsgRs+6eX8oZQnndyQRCpyrXPtNooSl0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00649', 'F', 'BSCS', '2', 'BSCS 2A'),
(1174, 'Tonido, John Lenard P.', '15100226', 'VOPeC0a++ktnk9hkX1cdowS5NI8JZVe0k5L2EH2DEwA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00226', 'M', 'BSCS', '2', 'BSCS 2A'),
(1175, 'Ventulan, Ronald Rey S.', '15100216', 'UHSa926yR8o52Fi8yT0PZ+Zo9ZvBL2g/6F/SlDtcjOs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00216', 'M', 'BSCS', '2', 'BSCS 2A'),
(1176, 'Villaflores, Crissa M.', '14200029', 'OhqOJtTJObj7RhnqiNgQxawErT4qvtipyrYqLf81rVc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-2-00029', 'F', 'BSCS', '2', 'BSCS 2B'),
(1177, 'Aballe, Bernadette', '14101178', 'GHwPTUF5Se8kQcjw1OAbzS72xlbU3/ZNBRspNpsz2V0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01178', 'F', 'BSCS', '3', 'BSCS 3A'),
(1178, 'Abellanosa, Irene C.', '14101582', '+O/f9IJXGwGvbFOJmkCeATPd/F67bzfdYaJpKuU6c+0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01582', 'F', 'BSCS', '3', 'BSCS 3A'),
(1179, 'Aboga, Patrick Mark L.', '14100434', 'raR36yWSID4/CZWMFD2d3neAr/PalJZt3DSqT/EjVQE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00434', 'M', 'BSCS', '3', 'BSCS 3B'),
(1180, 'Adrales, Johnver  De Guzm', '08101405', 'DRCv+d3Bgfn8VBwYWODafqeRViPFyfesEvmfJ7uYEdE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '08-1-01405', 'M', 'BSCS', '3', 'BSCS 3B'),
(1181, 'Agote, Faith Condino', '12100619', 'qAwvSUgcmbFrDOzgrAm2H2zttYd/Q0mGCo+lijxn1S4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00619', 'F', 'BSCS', '3', 'BSCS 3B'),
(1182, 'Ambe, Jomarie Bolante', '14101999', 'vwCKBpEae32rM/+VB5QctkXjlCWLxip0h9VWe4tWyyc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01999', 'M', 'BSCS', '3', 'BSCS 3A'),
(1183, 'Ameril, Rayna A.', '14100899', 'okExqL+TM5YcNCAd8m2rgQh6PFisATYIM6QAF3l165o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00899', 'F', 'BSCS', '3', 'BSCS 3B'),
(1184, 'Arribe, Andrea M.', '14102310', 'K1w5XUT/ffvBQkVBy3ZtngEfhu8f2CvAjDjpm/Q6J6s=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02310', 'F', 'BSCS', '3', 'BSCS 3B'),
(1185, 'Arribe, Mark Anthony G.', '14102266', 'qAwvSUgcmbFrDOzgrAm2H2zttYd/Q0mGCo+lijxn1S4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02266', 'M', 'BSCS', '3', 'BSCS 3A'),
(1186, 'Batiquin, Wilton Gary Bernardes', '14102466', 'KceBpwNo/02YgYeOgSVOUpPCZwhliHGeJRPhzGmwpoE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02466', 'M', 'BSCS', '3', 'BSCS 3A'),
(1187, 'Belen, Christine Joy Medalla', '14102253', '26MIkvyEEmpmQ4bxk1rCbuEs2Tv8pJmIqecf3FMLUqA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02253', 'F', 'BSCS', '3', 'BSCS 3B'),
(1188, 'Berallo, Judy Ann C.', '14101423', 'QSKzOEbXii1okN/z1i1Y/Vz8ezLOOTb//w3QzdhqASA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01423', 'F', 'BSCS', '3', 'BSCS 3A'),
(1189, 'Boncolmo, Joseph Mel B.', '14100212', 'nND7SRbHfvmDINRipXoQfg7XYKBJp2c7WB+VmmFnppo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00212', 'M', 'BSCS', '3', 'BSCS 3A'),
(1190, 'Bornillo, Erskine Roy  Co', '11101907', 'iYUHstdxM87aDfsOhFVDfUNo13PaiTkB4hjHXxWQW9g=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-01907', 'M', 'BSCS', '3', 'BSCS 3B'),
(1191, 'Bornillo, Kennard James Coma', '13102480', 'c/w99v+OA4sfOQjCZm9OviTBsJk0yxP1owRxvyvO+ug=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02480', 'M', 'BSCS', '3', 'BSCS 3A'),
(1192, 'Brane, Kimberly S.', '14100026', 'TBaduEO3ujwvcMhZQ4t1qMT6ujSzRie4YPlZIsj4PBE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00026', 'F', 'BSCS', '3', 'BSCS 3B'),
(1193, 'Bunani, Azreel Seth Keanu Genoguin', '13101002', '/1P7uFY3bXKQGeq4ISGW7wD3XTIna40an/cN6lrQYPo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01002', 'M', 'BSCS', '3', 'BSCS 3A'),
(1194, 'Cabalquinto, Cindy V.', '14100505', 'BTV2YLped7sm3vyozo2KzEDLrKIN4hm5pEaIDe/TitU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00505', 'F', 'BSCS', '3', 'BSCS 3B'),
(1195, 'Cajandoc, Leslie Mae P.', '14100767', 'vYxkXXVRkRYDE7czfALddzGfzUjEs9mDaZhXK7/Wxms=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00767', 'F', 'BSCS', '3', 'BSCS 3A'),
(1196, 'Calderon, Leanna Mae', '14100864', '862ulfsZX+IO+mEHRKBBvW4nhcEowmg4WD9FClWKhx8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00864', 'F', 'BSCS', '3', 'BSCS 3A'),
(1197, 'Cañezo, Rose Ann A.', '14100866', 'c/w99v+OA4sfOQjCZm9OviTBsJk0yxP1owRxvyvO+ug=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00866', 'F', 'BSCS', '3', 'BSCS 3A'),
(1198, 'Canlas, Jeffrey C.', '14101410', '+O/f9IJXGwGvbFOJmkCeATPd/F67bzfdYaJpKuU6c+0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01410', 'M', 'BSCS', '3', 'BSCS 3A'),
(1199, 'Caren, Carlo Mocorro', '14101765', '17xFLqIpf0bDPc+CxKu2jviXemzodJtBGsnjDu/rqAg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01765', 'M', 'BSCS', '3', 'BSCS 3A'),
(1200, 'Casas, Joshua A.', '14100253', 'WlM7F6MNeLY/W3+sFLe8ermDMgCNIhztomwEC0z39KM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00253', 'M', 'BSCS', '3', 'BSCS 3A'),
(1201, 'Catam-isan, Resdel V.', '14100348', 'c/w99v+OA4sfOQjCZm9OviTBsJk0yxP1owRxvyvO+ug=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00348', 'M', 'BSCS', '3', 'BSCS 3B'),
(1202, 'Collamar, Lugine Mae Jayan', '14101906', '9VE0+8jvsrIvaldrWqrTbzBzocq9M7ZwofHFv947bqU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01906', 'F', 'BSCS', '3', 'BSCS 3B'),
(1203, 'Colo, Hya B.', '14100230', 'pJHYGnCl7u8oNXYagLvIAzR+Yobds7xjD0Sc4zFJxC8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00230', 'F', 'BSCS', '3', 'BSCS 3B'),
(1204, 'Comandao, Grace D.', '14100242', 'XspUCHUoPwo3nfxDxlbAFoiOTiQWNCbo5JTqJB4IjJg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00242', 'F', 'BSCS', '3', 'BSCS 3A'),
(1205, 'Completado, Samson Diaz', '14101938', 'btXzzfBIJ2RnGBH3DZ7uz7olX/VW9hwGrqAYuQZpuT8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01938', 'M', 'BSCS', '3', 'BSCS 3A'),
(1206, 'Corpin, Rafaelito S.', '14100097', 'AVlsWA0n/Q81H98iQydpzQEDkKtdq2yBjepxM3sayho=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00097', 'M', 'BSCS', '3', 'BSCS 3B'),
(1207, 'Crisostomo, Renato Jr. C.', '14100686', 'LBTa9ch44o6stS1mjRccIO4h3oo9w6/BVrqLUnfxOQo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00686', 'M', 'BSCS', '3', 'BSCS 3A'),
(1208, 'Dabalos, Romart F.', '14100093', 'vevg3mgu5XQIxQhV+M7VoJH/3fXfrs8Ald6Wvm4lDcg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00093', 'M', 'BSCS', '3', 'BSCS 3A'),
(1209, 'Demate Jr., Arnel Rosos', '13100396', 'nND7SRbHfvmDINRipXoQfg7XYKBJp2c7WB+VmmFnppo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00396', 'M', 'BSCS', '3', 'BSCS 3B'),
(1210, 'Duazo, Angelica B.', '14101317', 'xi45dU7FBYavdyp4dVZgqaU0mdH0rrCT4cobvx7nHmU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01317', 'F', 'BSCS', '3', 'BSCS 3A'),
(1211, 'Ebajan, Maridel Otic', '14102262', '2supJ277AMEJUMo8crGrT7cgIfkd5yChlVxbEQHzv7Y=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02262', 'F', 'BSCS', '3', 'BSCS 3A'),
(1212, 'Gantala, Rowe Jean C.', '14100024', 'mEHM6OETVRZFVrbAqPed/ZypX4jhyAeZP0msalSCPLY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00024', 'F', 'BSCS', '3', 'BSCS 3B'),
(1213, 'Garcia, John Levin Balbuena', '10200137', '7+i4ejdlY7LAmuY5AXIKk24c8qqx7iArRoJc4SRCkNs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '10-2-00137', 'M', 'BSCS', '3', 'BSCS 3B'),
(1214, 'Gascon, Ana Nikki Torion', '14102204', 'u9SLN9c4YSFea50Gd7vkjOZnVjPmCn0dlv2+fb817g4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02204', 'F', 'BSCS', '3', 'BSCS 3B'),
(1215, 'Gavilo, Leoneil Jay L.', '14100481', 'raR36yWSID4/CZWMFD2d3neAr/PalJZt3DSqT/EjVQE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00481', 'M', 'BSCS', '3', 'BSCS 3B'),
(1216, 'Gelizon, Val Eldrich B.', '14100102', 'XspUCHUoPwo3nfxDxlbAFoiOTiQWNCbo5JTqJB4IjJg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00102', 'M', 'BSCS', '3', 'BSCS 3A'),
(1217, 'Godinez, Renezar II M.', '14100888', 'I87hps44VZ1oZc4naMxPg1YXatEkyjnP77xEL0qdcbk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00888', 'M', 'BSCS', '3', 'BSCS 3A'),
(1218, 'Hayahay, Genny N.', '14101441', 'pirVA/yQKGViuduaUMvSzCWzDz9+PR8MUhqy9tQKDbY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01441', 'F', 'BSCS', '3', 'BSCS 3B'),
(1219, 'Ibojo, Jomari Macatumpag', '14102515', '0LRm2YVWelWzsBeoxFIP0TQK8n9cFTC7++6yiFCBqaA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02515', 'M', 'BSCS', '3', 'BSCS 3B'),
(1220, 'Jordan, Frances Dominic S.', '13200075', 'K1w5XUT/ffvBQkVBy3ZtngEfhu8f2CvAjDjpm/Q6J6s=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-2-00075', 'M', 'BSCS', '3', 'BSCS 3A'),
(1221, 'Logro, Ma. Margarita Jane T.', '14100240', 'MF2WMx88UGiuzG7lKZxcrRYRDy99a5LxXQ0L1seCbv8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00240', 'M', 'BSCS', '3', 'BSCS 3B'),
(1222, 'Lozada, Judy Ann G.', '14101458', 'K0q59zyjMRM00mPFkWP2tN6qF8kSgKOEOWlH1oY1lok=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01458', 'F', 'BSCS', '3', 'BSCS 3B'),
(1223, 'Magdalaga, Renelyn M.', '14101448', 'MeiwqbbWOTz7fXzNvKkNVVN2FjBb9qu+PnWYs8QMcSI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01448', 'F', 'BSCS', '3', 'BSCS 3A'),
(1224, 'Mahilum, Nico N.', '14102077', 'usGIVLX729zMTBQl80tCoGbQpO+BNmLVbiS1XfmUWKY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02077', 'M', 'BSCS', '3', 'BSCS 3B'),
(1225, 'Mahusay, Maria Delia', '14101326', 'e6mQ/6+LgT1cL4IhuFxwPdDtsip4zm5eCTG3Q5tdmMA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01326', 'F', 'BSCS', '3', 'BSCS 3B'),
(1226, 'Mangco, Mark Ronnie Mocorro', '13100964', 'pjJDdseRPIYpA8qa9AVkw98V9MnsiKli+I1T7FNMCcs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00964', 'M', 'BSCS', '3', 'BSCS 3A'),
(1227, 'Manzano, John Randolf', '14101340', 'c/oOu63aHBAXZBMFCcAIv1MyTrxDDBJyGc/bX8Hnybc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01340', 'M', 'BSCS', '3', 'BSCS 3B'),
(1228, 'Mapa, Merry Rose M.', '14101195', '0mK/9ytCY4iRbAn3NZfjRpv/JdpKOPV1f9xnGoGT8vM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01195', 'F', 'BSCS', '3', 'BSCS 3B'),
(1229, 'Marcos, Roseanne Mae E.', '14100673', 'lNEiZqfO3guC2COhECTWYTPef72nh4kso/hfeFcy4d8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00673', 'F', 'BSCS', '3', 'BSCS 3A'),
(1230, 'Marino, Jay-ar J.', '14101371', 'pirVA/yQKGViuduaUMvSzCWzDz9+PR8MUhqy9tQKDbY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01371', 'M', 'BSCS', '3', 'BSCS 3A'),
(1231, 'Mejarito, Mara C.', '14101323', '6ZYp1U+9J6zFy+xAXZpJmDroBAOLXwNex+4T7XMkcPg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01323', 'F', 'BSCS', '3', 'BSCS 3A'),
(1232, 'Merino, Amanda Ruth Ambe', '14100597', 'od2s7S3L+PAARA4kvPqxSKg+UTpZNlVec9/mFKIoep8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00597', 'F', 'BSCS', '3', 'BSCS 3B'),
(1233, 'Misagal, Syrah Caceres', '14102229', '2SOu5fozFMO7vy2rTV3oFRAQUIt8Sqx4isboo/thdP4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02229', 'F', 'BSCS', '3', 'BSCS 3B'),
(1234, 'Molejon, Jundie Mahait', '14102547', 'Rob+FVFUyvVHlvjXjdt/CuOCIMyK9O8UJGj6vICsDRY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02547', 'M', 'BSCS', '3', 'BSCS 3B'),
(1235, 'Mondes, Lanie S.', '14101430', '5h5hbR+ntFvDy0MRDhnqPdLZ9bLS+pWwQI8nzXu5aTE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01430', 'F', 'BSCS', '3', 'BSCS 3A'),
(1236, 'Mondoy, Ma. Maribeth E.', '14101162', 'cE5ci8a4Mq3z5gpTLsYd7yqJMRLnF84Bz2+Gur6rsa0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01162', 'F', 'BSCS', '3', 'BSCS 3A'),
(1237, 'Morillo, Nikki Limpin', '12100556', 'IJp7PVuEkNmGQq8d2KKWZzkYIwmvtzeuHah6Kr+thU8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00556', 'F', 'BSCS', '3', 'BSCS 3A'),
(1238, 'Mulig, Lorlie Mae Nuñez', '14100055', 'RsBfcCy+1mhBWMk4efogf+XLTESC1YiQszXMseYIAaI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00055', 'F', 'BSCS', '3', 'BSCS 3A'),
(1239, 'Napoles, Maricris Bardelosa', '14101655', '26MIkvyEEmpmQ4bxk1rCbuEs2Tv8pJmIqecf3FMLUqA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01655', 'F', 'BSCS', '3', 'BSCS 3B'),
(1240, 'Niepes, Roy C.', '14101332', 'TBaduEO3ujwvcMhZQ4t1qMT6ujSzRie4YPlZIsj4PBE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01332', 'M', 'BSCS', '3', 'BSCS 3A'),
(1241, 'Odal, Joan M.', '14100095', 'IJp7PVuEkNmGQq8d2KKWZzkYIwmvtzeuHah6Kr+thU8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00095', 'F', 'BSCS', '3', 'BSCS 3B'),
(1242, 'Oliveros, Juvelyn C.', '14100539', 'DJEPtid20kdrOhrz2jdkbwESmTFnPMbgdM9wwK3E75o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00539', 'F', 'BSCS', '3', 'BSCS 3A'),
(1243, 'Ongue, Angelo C.', '14101616', 'YqoG6o+JS0X+gOsPhovTrO1xGVW8NvLR9NI0XBiRbEA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01616', 'M', 'BSCS', '3', 'BSCS 3B'),
(1244, 'Panis, Rheana M.', '14101461', 'OkDeyoYdSFviwsD80ASyfdhjQZIHntLRoaUIGyi3POk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01461', 'F', 'BSCS', '3', 'BSCS 3B'),
(1245, 'Pardiñas, Antoniette A.', '14100099', 'K/RUT5mjdM6IabmC5nsVC+kWfLT2dptVj4lfk3m7mds=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00099', 'F', 'BSCS', '3', 'BSCS 3B'),
(1246, 'Pepito, Arnold Jansen Mission', '11100744', '2gGh+gu2+D7tS0xtdySDNLxki3OUz8eXQXR5COPD50Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-00744', 'M', 'BSCS', '3', 'BSCS 3B'),
(1247, 'Piamonte, Joralyn A.', '14100085', '46NMuaV3GWClMpBLGc/YRSRP0b5U3fBth3ajwsoJ5iw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00085', 'F', 'BSCS', '3', 'BSCS 3B'),
(1248, 'Pilapil, Ruffa B.', '14100929', '0mK/9ytCY4iRbAn3NZfjRpv/JdpKOPV1f9xnGoGT8vM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00929', 'F', 'BSCS', '3', 'BSCS 3A'),
(1249, 'Plaza, Dave Ive', '14100231', 'mrzDQ22Lw77GHxoFcAJrIRvs0N/e9S5ddxrKNJOmP7I=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00231', 'M', 'BSCS', '3', 'BSCS 3A'),
(1250, 'Ramirez, Lourez S.', '14100032', 'tf45DV8uNkjOhtFHU6TOE73fIArxGtRgi7g9ITZzL7M=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00032', 'F', 'BSCS', '3', 'BSCS 3B'),
(1251, 'Ranes, Marielle D.', '14100167', 'e7nSGOror/qe3iOekBnoqZn8QEKenGXpUngJ97/+9gw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00167', 'F', 'BSCS', '3', 'BSCS 3A'),
(1252, 'Regala, Mylene O.', '14100300', 'Fwj3UdNYFr6c9DLV5cFV51aKRPPx/m2cDAfzgpC5xYY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00300', 'F', 'BSCS', '3', 'BSCS 3B'),
(1253, 'Rosales, Alwen D.', '14100495', 'IIn+E5+MYM7D/FRQ6JFsAsd5G4GlHdoYLlV+2EyslzI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00495', 'M', 'BSCS', '3', 'BSCS 3B'),
(1254, 'Rostata, Nichole Mendoza', '12100759', 'fM40PEZufnMrSYd8Qo3gpin2c7szIPupFjIuAH/S324=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00759', 'F', 'BSCS', '3', 'BSCS 3A'),
(1255, 'Sabordo, Maria Theresa A.', '14102044', 'vdMCAbxoh/5wrrBWT5EW70NFAR4atCa7zpV33mgcOIc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02044', 'F', 'BSCS', '3', 'BSCS 3A'),
(1256, 'Santiañez, Kenneth C.', '14101125', 'Fwj3UdNYFr6c9DLV5cFV51aKRPPx/m2cDAfzgpC5xYY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01125', 'M', 'BSCS', '3', 'BSCS 3A'),
(1257, 'Sarsalejo, Jhunnel Abad', '13101041', 'wRE/LDxB0T+22fXorzuqIHZZ4/ULSIpJDQEdxa0x8v8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01041', 'M', 'BSCS', '3', 'BSCS 3B'),
(1258, 'Sarzuelo, Akio San C.', '14100101', 'dH9QKqBbj7niOytaZNrxyL4yDaQfT0MEsGAApJ72G8s=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00101', 'M', 'BSCS', '3', 'BSCS 3A'),
(1259, 'Seno, Rey Anthony Dapon', '13101545', 'J4HKBkkkp9mvQfyq6oQG6b1LDC1uG85KtHwtnTynVhs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01545', 'M', 'BSCS', '3', 'BSCS 3B'),
(1260, 'Sobrete, John Ray C.', '14101335', 'cE5ci8a4Mq3z5gpTLsYd7yqJMRLnF84Bz2+Gur6rsa0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01335', 'M', 'BSCS', '3', 'BSCS 3A'),
(1261, 'Solamo, Jecill Lipaopao', '14102226', 'AsbYdreZ1XYvtuzWX8nBt/pc7gflz/xy3AzgePlxfME=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02226', 'F', 'BSCS', '3', 'BSCS 3B'),
(1262, 'Sulla, Mark Joseph P.', '14100883', 'pirVA/yQKGViuduaUMvSzCWzDz9+PR8MUhqy9tQKDbY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00883', 'M', 'BSCS', '3', 'BSCS 3B'),
(1263, 'Tadeja, Jessril C.', '14100798', '5h5hbR+ntFvDy0MRDhnqPdLZ9bLS+pWwQI8nzXu5aTE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00798', 'M', 'BSCS', '3', 'BSCS 3A'),
(1264, 'Tagalog, Sherlene Veraque', '14101897', '0mK/9ytCY4iRbAn3NZfjRpv/JdpKOPV1f9xnGoGT8vM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01897', 'F', 'BSCS', '3', 'BSCS 3A'),
(1265, 'Tanguihan, Ma. Lhyza P.', '14102312', 'nTOW67zNZ5xRWH7ZaEa/qwgOaDQPkzghOhNpSpdh4uc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02312', 'F', 'BSCS', '3', 'BSCS 3B'),
(1266, 'Tatoy, Mario Jr.', '14101760', 'ZaKcMMSsbJ3hpmgHXdiUJDagxRBzd1OE6JLB5Q2TZs0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01760', 'M', 'BSCS', '3', 'BSCS 3A'),
(1267, 'Tayong, Rica A.', '14200017', 'AsbYdreZ1XYvtuzWX8nBt/pc7gflz/xy3AzgePlxfME=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-2-00017', 'F', 'BSCS', '3', 'BSCS 3A'),
(1268, 'Tecson, Genrose B.', '14101542', 'yQXZBW7qILAGufUi7gNC16sLHTGA1xsCvdPmXFY9nzQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01542', 'F', 'BSCS', '3', 'BSCS 3A'),
(1269, 'Tol, Ritchelene R.', '14101306', 'gle0xTDvTMA7nOq6ZyUF195i4iTycypxRF0dLNjMroY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01306', 'F', 'BSCS', '3', 'BSCS 3A'),
(1270, 'Tonedo, Reyna G.', '14100094', '2gGh+gu2+D7tS0xtdySDNLxki3OUz8eXQXR5COPD50Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00094', 'F', 'BSCS', '3', 'BSCS 3A'),
(1271, 'Udtohan, Ruzzle Mae A.', '14100846', '17xFLqIpf0bDPc+CxKu2jviXemzodJtBGsnjDu/rqAg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00846', 'F', 'BSCS', '3', 'BSCS 3A'),
(1272, 'Vermug, Dina G.', '14100760', 'uozcBKm7melhGIEZqdQAuMh74w9fIHntPZsQCHmNaXQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00760', 'F', 'BSCS', '3', 'BSCS 3A'),
(1273, 'Verutiao, Khazzel Lubay', '14102718', 'lNEiZqfO3guC2COhECTWYTPef72nh4kso/hfeFcy4d8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02718', 'M', 'BSCS', '3', 'BSCS 3B'),
(1274, 'Victorioso, Cecille Sevillano', '14102348', 'fDbhep4zBhzD19gaiAcM9I9zYRaCN4fDp1IbypT7EK0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02348', 'F', 'BSCS', '3', 'BSCS 3B'),
(1275, 'Villaganes, Gem Mark Quimbo', '10100996', 'vCcLEB5lLksUUcbaxBcpDcvhrePObozHoe9+e9vKY9c=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '10-1-00996', 'M', 'BSCS', '3', 'BSCS 3B'),
(1276, 'Villapaña, Menilva P.', '14101672', 'w/lb10EhTcsIezpoatwOP47inzm/kL6q7huyJK16d24=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01672', 'F', 'BSCS', '3', 'BSCS 3A'),
(1277, 'Visagar, Raymond R.', '14100570', '+YlhUtuvCpMVNDrQnC8tKT8BeOE9SLZV4XjCC3LujCk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00570', 'M', 'BSCS', '3', 'BSCS 3B'),
(1278, 'Abad, Marjory Delda', '13101464', 'uozcBKm7melhGIEZqdQAuMh74w9fIHntPZsQCHmNaXQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01464', 'F', 'BSCS', '4', 'BSCS 4A'),
(1279, 'Agang, Franklin Bern Aclado', '13101042', 'J8ifZzvNadnTYjSHnfRWqLuRjtXL7Uig2WE2XhRml3M=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01042', 'M', 'BSCS', '4', 'BSCS 4A'),
(1280, 'Agujar, Jaofrey Ejorcadas', '13100686', 'od2s7S3L+PAARA4kvPqxSKg+UTpZNlVec9/mFKIoep8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00686', 'M', 'BSCS', '4', 'BSCS 4A'),
(1281, 'Ameril, Hanefah Mamarinta', '13100406', '7+i4ejdlY7LAmuY5AXIKk24c8qqx7iArRoJc4SRCkNs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00406', 'F', 'BSCS', '4', 'BSCS 4A'),
(1282, 'Angni, Hassim Batalo', '13101614', 'pirVA/yQKGViuduaUMvSzCWzDz9+PR8MUhqy9tQKDbY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01614', 'M', 'BSCS', '4', 'BSCS 4A'),
(1283, 'Bacalando, Lloyd Vaporoso', '13101028', 'BYRkpgj3XNR12qv3sJ7cN+oyR3v+CYXiyz82OBAl9wQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01028', 'M', 'BSCS', '4', 'BSCS 4A'),
(1284, 'Barahan, Johanna Marie S', '09200059', 'vjQOP6833Ys73P8UE5aUXIZfzOWiL1ZYCwAuqhrwcA8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '09-2-00059', 'F', 'BSCS', '4', 'BSCS 4A'),
(1285, 'Basagan, Ferry Rose cuna', '13100855', 'e6mQ/6+LgT1cL4IhuFxwPdDtsip4zm5eCTG3Q5tdmMA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00855', 'F', 'BSCS', '4', 'BSCS 4A'),
(1286, 'Borromeo, Vanessa Jee Leonen', '13101966', 'tf45DV8uNkjOhtFHU6TOE73fIArxGtRgi7g9ITZzL7M=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01966', 'F', 'BSCS', '4', 'BSCS 4A'),
(1287, 'Calupaz, Rose Jean Lauzon', '13101632', 'LBTa9ch44o6stS1mjRccIO4h3oo9w6/BVrqLUnfxOQo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01632', 'F', 'BSCS', '4', 'BSCS 4A'),
(1288, 'Cañezo, Oneal Plaza', '13100811', 'w/lb10EhTcsIezpoatwOP47inzm/kL6q7huyJK16d24=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00811', 'M', 'BSCS', '4', 'BSCS 4A'),
(1289, 'Costelo, Marvin Hulguin', '13102442', 'IIn+E5+MYM7D/FRQ6JFsAsd5G4GlHdoYLlV+2EyslzI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02442', 'M', 'BSCS', '4', 'BSCS 4A'),
(1290, 'Cuevas, Maebele Locahin', '13100128', 'grE6pVjgCJ4o4K/mh/4F/2/BRe2HdZIIvP7um88XVrU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00128', 'F', 'BSCS', '4', 'BSCS 4A'),
(1291, 'Dangel, Maria Belen Sales', '13100145', 'okExqL+TM5YcNCAd8m2rgQh6PFisATYIM6QAF3l165o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00145', 'F', 'BSCS', '4', 'BSCS 4A'),
(1292, 'Gascon, Shermin Michelle T.', '09101691', 'vevg3mgu5XQIxQhV+M7VoJH/3fXfrs8Ald6Wvm4lDcg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '09-1-01691', 'F', 'BSCS', '4', 'BSCS 4A'),
(1293, 'Gudmalin, Jezzah Mae Cuevas', '13100022', 'wRE/LDxB0T+22fXorzuqIHZZ4/ULSIpJDQEdxa0x8v8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00022', 'F', 'BSCS', '4', 'BSCS 4A'),
(1294, 'Machete, Johnser Dave Paculan', '13101281', '2gGh+gu2+D7tS0xtdySDNLxki3OUz8eXQXR5COPD50Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01281', 'M', 'BSCS', '4', 'BSCS 4A'),
(1295, 'Merez, Stiffie Marie M.', '13100023', 'pJHYGnCl7u8oNXYagLvIAzR+Yobds7xjD0Sc4zFJxC8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00023', 'F', 'BSCS', '4', 'BSCS 4A'),
(1296, 'Misagal, Ma. Farinah Caceres', '13101600', 'cE5ci8a4Mq3z5gpTLsYd7yqJMRLnF84Bz2+Gur6rsa0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01600', 'F', 'BSCS', '4', 'BSCS 4A'),
(1297, 'Mondejar, Jonalyn Pantas', '13102067', 'SREHwxkThqkSOHaBOn+iKO2CxAEJR4tN7wlJuvJaA2A=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02067', 'F', 'BSCS', '4', 'BSCS 4A'),
(1298, 'Monzales, Christopher Jumetilco', '13101591', 'I3BDYGpZ9AN5WpPa+6x9vx9KFBKNh3a8dTh+o+7VVkY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01591', 'M', 'BSCS', '4', 'BSCS 4A'),
(1299, 'Morillo, Kevin', '13101493', 'khEkC52JmK2ZKZpK5N7yJ0vWrdYM7Iyxr7+zO8/6Acs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01493', 'M', 'BSCS', '4', 'BSCS 4A');
INSERT INTO `tbl_students` (`id`, `name`, `username`, `password`, `email`, `contact_number`, `birthdate`, `type`, `detail`, `active`, `user_id`, `date_added`, `date_modified`, `CODE`, `SEX`, `CRSCD`, `YEAR`, `SECCD`) VALUES
(1300, 'Nallares, Federito Ramento', '11101258', 'QSKzOEbXii1okN/z1i1Y/Vz8ezLOOTb//w3QzdhqASA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-01258', 'M', 'BSCS', '4', 'BSCS 4A'),
(1301, 'Odejerte, Karen Mae Basa', '14102578', 'fcLo+/WMq2lF98ga0LVmhmr03Bs/TFc5/g5eUoIhuxg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02578', 'F', 'BSCS', '4', 'BSCS 4A'),
(1302, 'Parayday, Jingkie Sevillano', '13101083', '7st89tj94kJvFNO6t5HRZcFThnRlBPY6JYqAepdS2eA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01083', 'F', 'BSCS', '4', 'BSCS 4A'),
(1303, 'Paril, Mike Ceasar Borrinaga', '13101489', '5h5hbR+ntFvDy0MRDhnqPdLZ9bLS+pWwQI8nzXu5aTE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01489', 'M', 'BSCS', '4', 'BSCS 4A'),
(1304, 'Petargue, Edgardo Sacay', '13101259', 'mEHM6OETVRZFVrbAqPed/ZypX4jhyAeZP0msalSCPLY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01259', 'M', 'BSCS', '4', 'BSCS 4A'),
(1305, 'Placido, Jerome Mangubat', '13101802', '03DGkM8Qc6Un7rgPg1m1LVsBVGR2ojLR47FdhaMAD2c=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01802', 'M', 'BSCS', '4', 'BSCS 4A'),
(1306, 'Ramirez, Riena Abasyal', '12101706', 'BTV2YLped7sm3vyozo2KzEDLrKIN4hm5pEaIDe/TitU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01706', 'F', 'BSCS', '4', 'BSCS 4A'),
(1307, 'Regodo, Merejoy Ondiano', '13101506', 'v+DyzKaFBZ245lRImEPjLeZ+G1TVwWg5+of457KBnDM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01506', 'F', 'BSCS', '4', 'BSCS 4A'),
(1308, 'Rosell, Joan Pelemer', '13101395', 'aUGA17QKfKeR9aGPUBWxkRjn87qBu0tpNzkFbUYabvg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01395', 'F', 'BSCS', '4', 'BSCS 4A'),
(1309, 'Sabong, Robelyn Aldueso', '13100759', '4pp1fWOPSmzzi2cQ9k/kJqosBU65VB095fNFWNhNBu4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00759', 'F', 'BSCS', '4', 'BSCS 4A'),
(1310, 'Sabonsolin, Aibe Quillopas', '12100223', 'n4Zn3YyUjyJ0gkFf3ykKjdGzB4UnQM17FXWkMJqESXo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00223', 'F', 'BSCS', '4', 'BSCS 4A'),
(1311, 'Sollano, Benedict Madeja', '12200007', '8U7zbFBnP47AMvtdSV/lOikC4hsbJhnIuafNgiNNufg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-2-00007', 'M', 'BSCS', '4', 'BSCS 4A'),
(1312, 'Velarde, Evanre', '13102440', 'o1Cc9AwSFsRPOMsvJs4vARBWV6lfo4QVh4OaaJXB+E0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02440', 'F', 'BSCS', '4', 'BSCS 4A'),
(1313, 'ABRIGO, JONATHAN B.', '16100863', '8U7zbFBnP47AMvtdSV/lOikC4hsbJhnIuafNgiNNufg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00863', 'M', 'BSIS', '1', 'BSIS 1A'),
(1314, 'Agner, Jerry Esqerdo', '15102204', 'OAdwkTtaxpup1F7MZagFJpp2iA8cpW0ON4qGedvrkTw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02204', 'M', 'BSIS', '1', 'BSIS 2C'),
(1315, 'Alegre, Nick Bryan Plaza', '15200026', 'rB01+ZKPumNxsT91NwG6QC/bR36QXhv9BoOEzDJlJOg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00026', 'M', 'BSIS', '1', 'BSIS 1A'),
(1316, 'ALIJID, JASMEN DELA PEÑA', '16100664', 'n4Zn3YyUjyJ0gkFf3ykKjdGzB4UnQM17FXWkMJqESXo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00664', 'F', 'BSIS', '1', 'BSIS 1A'),
(1317, 'ANSALE, MARIJOY P.', '16100692', 'tjRM1+wizoHn0evhx0eRYVU/He7fRICfovP+oxnjIbY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00692', 'F', 'BSIS', '1', 'BSIS 1A'),
(1318, 'Aranguez, Armingel Vender', '16100256', 'HqXpqxm+C2th4zh21NXOgEuXHQZtQuhJAp+U8riSMj4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00256', 'F', 'BSIS', '1', 'BSIS 1A'),
(1319, 'Arpociple, Christian Villaganes', NULL, 'ouGJpjMVSmU3Kobf/THFomnss3Couyhh0JdoicHuFiY=', '', NULL, '2016-11-20', NULL, NULL, 1, 1, NULL, '2016-11-20 13:36:45', '16-1-00292', 'M', 'BSIS', '1', 'BSIS 1A'),
(1320, 'Avila, Tibo Sabugo', '14101780', 'IUpkv5NYi8KgAbXsw+odaDxRKVVLdpZyjLFAcs8G2vs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01780', 'M', 'BSIS', '1', 'BSIS 1A'),
(1321, 'BEROG, MARLOU JOSHUA C.', '16100809', '/1P7uFY3bXKQGeq4ISGW7wD3XTIna40an/cN6lrQYPo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00809', 'M', 'BSIS', '1', 'BSIS 1A'),
(1322, 'Bilbao, Jay', '15200019', '+O/f9IJXGwGvbFOJmkCeATPd/F67bzfdYaJpKuU6c+0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00019', 'M', 'BSIS', '1', 'BSIS 1A'),
(1323, 'Bioco, Rosanna Almen', '16100318', 'AIEX6LI4iJHcbPHFSHZDKZPWa9dBZMOZa8ZVkJwBIWY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00318', 'F', 'BSIS', '1', 'BSIS 1A'),
(1324, 'Brazil, Jonico Fallera', '15200035', 'IUpkv5NYi8KgAbXsw+odaDxRKVVLdpZyjLFAcs8G2vs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00035', 'M', 'BSIS', '1', 'BSIS 1A'),
(1325, 'Brosoto, Cris John Villacrusis', '11100278', 'K/RUT5mjdM6IabmC5nsVC+kWfLT2dptVj4lfk3m7mds=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-00278', 'M', 'BSIS', '1', 'BSIS 1A'),
(1326, 'Dejarlo, Catherine Joy Teñoso', '16100014', 'XwPVKD0QMOH/rogaeD1wZCVQ6HKt0IiOJwcLjCgC7GA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00014', 'F', 'BSIS', '1', 'BSIS 1A'),
(1327, 'Eco, Angelo Martin S.', '16100848', 'rB01+ZKPumNxsT91NwG6QC/bR36QXhv9BoOEzDJlJOg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00848', 'M', 'BSIS', '1', 'BSIS 1A'),
(1328, 'Gahum, Jhon Carlo Maglasang', '12101736', 'n4W/jYDw0usepii3BsiZx4hzZzffMKAuo9ru9uVUbZk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01736', 'M', 'BSIS', '1', 'BSIS 1A'),
(1329, 'Gernaldo, Rosemarie Demate', '16100557', 'IVSSXQUiIxG0X2yrCGlacAaEKZSRrEVbc+5SXFuYMy0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00557', 'F', 'BSIS', '1', 'BSIS 1A'),
(1330, 'Gervacio, Rona Mae Salentes', '16100280', 'grE6pVjgCJ4o4K/mh/4F/2/BRe2HdZIIvP7um88XVrU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00280', 'F', 'BSIS', '1', 'BSIS 1A'),
(1331, 'Gorre, Lorna  Serdoncillo', '09101492', 'Sw4bL93MSjyiM8KQeu8AmRSEoQgeWsFITI0Iu4/gPwo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '09-1-01492', 'F', 'BSIS', '1', 'BSIS 1A'),
(1332, 'Jayubo Jaira Casas', '14101826', 'mEHM6OETVRZFVrbAqPed/ZypX4jhyAeZP0msalSCPLY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01826', 'F', 'BSIS', '1', 'BSIS 1A'),
(1333, 'Layos, Larra Jane', '16100376', 'IIn+E5+MYM7D/FRQ6JFsAsd5G4GlHdoYLlV+2EyslzI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00376', 'F', 'BSIS', '1', 'BSIS 1A'),
(1334, 'Lora, Bonifacio Obado', '14101903', 'n4W/jYDw0usepii3BsiZx4hzZzffMKAuo9ru9uVUbZk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01903', 'M', 'BSIS', '1', 'BSIS 4A'),
(1335, 'LUCERO, SHERYL C.', '16100840', '17xFLqIpf0bDPc+CxKu2jviXemzodJtBGsnjDu/rqAg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00840', 'F', 'BSIS', '1', 'BSIS 1A'),
(1336, 'Madronio, Karla Jane  Sab', '11101523', 'n4W/jYDw0usepii3BsiZx4hzZzffMKAuo9ru9uVUbZk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-01523', 'F', 'BSIS', '1', 'BSIS 1A'),
(1337, 'Mahusay, Glear Marie C.', '14102098', 'IVSSXQUiIxG0X2yrCGlacAaEKZSRrEVbc+5SXFuYMy0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02098', 'F', 'BSIS', '1', 'BSIS 2C'),
(1338, 'Manatad, Jammel Semacio', '16100305', '7soTUmQuha6wPq0DRckF8toxYQGE3fc8m6AGIMczAq4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00305', 'M', 'BSIS', '1', 'BSIS 1A'),
(1339, 'Monfiel, Bebelyn P.', '16100787', 'rB01+ZKPumNxsT91NwG6QC/bR36QXhv9BoOEzDJlJOg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00787', 'F', 'BSIS', '1', 'BSIS 1A'),
(1340, 'Montemor, Myline Micaballo', '16100425', 'hej+Qk0x4/BIeA80vEjNheAWsguYxeFDZzRQNT9B3tU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00425', 'F', 'BSIS', '1', 'BSIS 1A'),
(1341, 'Monzales, Judelyn Gonzal', '16100313', 'YwppNnE4mGTperFy1g5qzmhZkNU8vtG/iit+sCtHigE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00313', 'F', 'BSIS', '1', 'BSIS 1A'),
(1342, 'Nepomuceno, Arvie Casas', '16100431', '2gGh+gu2+D7tS0xtdySDNLxki3OUz8eXQXR5COPD50Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00431', 'M', 'BSIS', '1', 'BSIS 1A'),
(1343, 'Ordinario, Jenifer Gonzal', '16100325', 'mms2CyYCDVU65AgYCZCbAjGOy+AabSG7Uyz13pBUD14=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00325', 'F', 'BSIS', '1', 'BSIS 1A'),
(1344, 'Ortega, Alizarin Nicole Pilapil', '15200030', '2ciWpGfKjkBcE56bZ9KEFU3ShZG87/XS0aUBHwP/cCs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00030', 'F', 'BSIS', '1', 'BSIS 1A'),
(1345, 'PEDROSA, SHEENA', '16100667', 'nTOW67zNZ5xRWH7ZaEa/qwgOaDQPkzghOhNpSpdh4uc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00667', 'F', 'BSIS', '1', 'BSIS 1A'),
(1346, 'Pleños, Charles Louie T.', '15200185', '9o51cIVxghNjiUzcuvi9bdY8fZYTbpQdKZsR4ahqALY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00185', 'M', 'BSIS', '1', 'BSIS 1A'),
(1347, 'Pregua, Joshua V.', '15102353', 'raR36yWSID4/CZWMFD2d3neAr/PalJZt3DSqT/EjVQE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02353', 'M', 'BSIS', '1', 'BSIS 1A'),
(1348, 'Reyes, Bill Kelmer Mañacap', '15102213', 'o3XUKU+ouBooxOypT0Bgxn1qcO07rhzos4RlxsdChSM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02213', 'M', 'BSIS', '1', 'BSIS 1A'),
(1349, 'Roda, Lelita Cuizon', '16100428', '6hJP/IzBvAn1A1KLLhufjYgkeamfPo+sPxe/4Gvjdz4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00428', 'F', 'BSIS', '1', 'BSIS 1A'),
(1350, 'Rosales, Michael John C.', '15101039', 'o5oQAB2zrdH5V9+nnFSWtE2bQ+A0/5BCMP1nNW0ENso=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01039', 'M', 'BSIS', '1', 'BSIS 1A'),
(1351, 'Rosanto, John Paul S.', '15101082', '/KzS6sS3zQhGIrVZOs/aO0pYEjV/gkv7odxpu1MKvnQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01082', 'M', 'BSIS', '1', 'BSIS 1A'),
(1352, 'Rosquites Elvie Datu', '15101768', 'o5oQAB2zrdH5V9+nnFSWtE2bQ+A0/5BCMP1nNW0ENso=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01768', 'F', 'BSIS', '1', 'BSIS 1A'),
(1353, 'Sabagkit, Rico Cabalquinto', '16100556', 'IIn+E5+MYM7D/FRQ6JFsAsd5G4GlHdoYLlV+2EyslzI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00556', 'M', 'BSIS', '1', 'BSIS 1A'),
(1354, 'Sampilo, Elvira', '16100009', 'X+BG+R9t2N2nFc6WqOiGxWyioTpj50qTgdFRN068i5w=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00009', 'F', 'BSIS', '1', 'BSIS 1A'),
(1355, 'SAÑOSA, LECELLE C.', '16100018', 'ZQTTbOXwGbPMV9FxAVArB4tXcZHvPpb2jxcBIqZsBTc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00018', 'F', 'BSIS', '1', 'BSIS 1A'),
(1356, 'Sedurante, Aubry Villaganas', '15100026', 'od2s7S3L+PAARA4kvPqxSKg+UTpZNlVec9/mFKIoep8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00026', 'F', 'BSIS', '1', 'BSIS 1A'),
(1357, 'Soriano, Lovella', '16100008', 'FzDnUHYv1H3VREIcztiwWpJlahPu6yi/+yJyl5VCZZs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00008', 'F', 'BSIS', '1', 'BSIS 1A'),
(1358, 'Suelo, Jeirald Bernabe', '16100322', 'WKq8YQGm+BdbzkPdvkoJg0pzklN3ADmZOsjBpsLzVTQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00322', 'M', 'BSIS', '1', 'BSIS 1A'),
(1359, 'Veril, Arnel Loreto', '16100291', 'vdMCAbxoh/5wrrBWT5EW70NFAR4atCa7zpV33mgcOIc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00291', 'M', 'BSIS', '1', 'BSIS 1A'),
(1360, 'Abad, Jocelyn D.', '15100244', 'TBaduEO3ujwvcMhZQ4t1qMT6ujSzRie4YPlZIsj4PBE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00244', 'F', 'BSIS', '2', 'BSIS 2B'),
(1361, 'Abaya, Hazel Ann R.', '15101312', '7+i4ejdlY7LAmuY5AXIKk24c8qqx7iArRoJc4SRCkNs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01312', 'F', 'BSIS', '2', 'BSIS 2A'),
(1362, 'Aberelia, Juliet', '14200034', 'fM40PEZufnMrSYd8Qo3gpin2c7szIPupFjIuAH/S324=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-2-00034', 'F', 'BSIS', '2', 'BSIS 2C'),
(1363, 'Ado, Fatima F.', '15100302', 'v+DyzKaFBZ245lRImEPjLeZ+G1TVwWg5+of457KBnDM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00302', 'F', 'BSIS', '2', 'BSIS 2A'),
(1364, 'Aljo, Jessa G.', '15100669', 'o3XUKU+ouBooxOypT0Bgxn1qcO07rhzos4RlxsdChSM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00669', 'F', 'BSIS', '2', 'BSIS 2B'),
(1365, 'Alombro, Albert E.', '15100752', 'YwppNnE4mGTperFy1g5qzmhZkNU8vtG/iit+sCtHigE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00752', 'M', 'BSIS', '2', 'BSIS 2A'),
(1366, 'Antipala, Glen D.', '15101479', 'vV3VjkADfhVoy0Poantstlm+G/X0XlckXrtWVnA4GuM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01479', 'F', 'BSIS', '2', 'BSIS 2C'),
(1367, 'Apolan, Marlo Jr. C.', '15101544', '26MIkvyEEmpmQ4bxk1rCbuEs2Tv8pJmIqecf3FMLUqA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01544', 'M', 'BSIS', '2', 'BSIS 2B'),
(1368, 'Arpon, Joaquin D.', '15102225', 'TaC4Eu59WLdA2NDXjgHTWpv5JgNNdLnGbkApm6zrI7Y=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02225', 'M', 'BSIS', '2', 'BSIS 2A'),
(1369, 'Audencial, Joanna Marie O', '15100868', 'ZFyHSm9YRvEYZOnfD32w3vgKe7DdgBoaHKnk+VYNM1Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00868', 'F', 'BSIS', '2', 'BSIS 2B'),
(1370, 'Awa-ao Ma. Theresa', '14102483', 'inX+Cmx+Bv/Dw38RJB38FOUAqFSzzzwBgKSGA4R5+qg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02483', 'F', 'BSIS', '2', 'BSIS 2C'),
(1371, 'Bacalso, Jhan Kenneth L.', '15100622', 'u8V2rxTsiV6ECjJJJ1qGrzNNW0JNfI9p89daVZ79Zag=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00622', 'M', 'BSIS', '2', 'BSIS 2C'),
(1372, 'Barnayha, Marvin S.', '15101711', 'e8eWI5K4LKBQhsUiuviupO3hHlR1UNjLl6oMcbHSsJE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01711', 'M', 'BSIS', '2', 'BSIS 2B'),
(1373, 'Bauyaban, Arlene D.', '14200025', 'Nz+scNXGYGGSq7cC5cDFqNBMLI+2RWPuA08Swh9lW8M=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-2-00025', 'F', 'BSIS', '2', 'BSIS 2A'),
(1374, 'Bermoy, Angelou A.', '15100655', 'IrjvMY4HU21JD3KQOH29TZzTxF6uUEVqrGizXKxq9x0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00655', 'F', 'BSIS', '2', 'BSIS 2A'),
(1375, 'Betiles, Abby Shane L.', '15100998', 'RKRSxFym3mLwNj95ouqzGhSRE/ks6mcazttCSuuZqZk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00998', 'F', 'BSIS', '2', 'BSIS 2B'),
(1376, 'Bonifacio, Jeric Merwa', '14102583', 'F1N/m/5PFd1oifLCFg7Lpd8eCywhJ4Zxbw7Nh/jZIBw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02583', 'M', 'BSIS', '2', 'BSIS 2C'),
(1377, 'Bontes, Kent Lemuel Gozon', '13102023', '8S5dn8L0WiSnP+RAcAxsrIQYt4y9UuCIdMH9toy1LTE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02023', 'M', 'BSIS', '2', 'BSIS 2C'),
(1378, 'Brusas, Cherry Ann C.', '15101833', 'g9NtW7esNlKOA08AxD6AgR3cWO7vbLxmcm35MCa4+EQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01833', 'F', 'BSIS', '2', 'BSIS 2B'),
(1379, 'Cabuquin, Cristina B.', '15100315', 'BmCJR9Jax9q9U4flPVm8fluph3wamS6rNNZOEwIdUFE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00315', 'F', 'BSIS', '2', 'BSIS 2A'),
(1380, 'Cairo, Gretchen B.', '15100654', 'rKOYiCQvGlsVuPeuw0InH+Cw+YxL5XaCnYjJHxqMPzs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00654', 'F', 'BSIS', '2', 'BSIS 2A'),
(1381, 'Camahalan, Danica M.', '15101168', 'uWzdHeIKr1AKvF380hmBHgNfe1LhHPx7SeaR3sO9eAg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01168', 'F', 'BSIS', '2', 'BSIS 2A'),
(1382, 'Caparro, Cristine Mae T.', '15100116', 'ggioZsL+i+ZKKQgXoMEWBJCVAunDoHMVG/sW4xrsT3k=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00116', 'F', 'BSIS', '2', 'BSIS 2B'),
(1383, 'Casas, Jolina V.', '15102022', '3ggKpsCDzSIWOPJ8pgTpAiVE5bETE+00Jn7C4by9Ths=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02022', 'F', 'BSIS', '2', 'BSIS 2B'),
(1384, 'Catre, Romar U.', '15101873', 'DbZSev0+UnqmbDaWDghM+m/SCGifvf3Vg+0Pfbn1CPE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01873', 'M', 'BSIS', '2', 'BSIS 2B'),
(1385, 'Colita, Gian Carlo Nuñez', '15100260', 'Ym4OnWZT+58K3PAnz9q1EYkph/KQ+ftileW42W7Fsxs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00260', 'M', 'BSIS', '2', 'BSIS 2A'),
(1386, 'Corzon, Almera S.', '15100076', 'mko0nJnkjcm0pAGWsdpdS65R7j+ehUbgiHmIZoxI2tA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00076', 'F', 'BSIS', '2', 'BSIS 2B'),
(1387, 'Del Rosario Merazon C.', '15101337', 'oWUr/Cs9LXtSlFEchtCDocV2NtRlWG5l1X4ydds4Hfo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01337', 'F', 'BSIS', '2', 'BSIS 2A'),
(1388, 'Dela Cruz, Nestor T.', '15101522', '+9r9sjtOWYbYoQLV5oYNSLgxCJLjxCJNoVh5JKDTGp4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01522', 'M', 'BSIS', '2', 'BSIS 2A'),
(1389, 'Dela Rosa, Ryan Granados', '14101045', 'g6do4wTcz7Xclc7gN1CGWxAvaMMPVyQ8H1fHW0yWK0w=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01045', 'M', 'BSIS', '2', 'BSIS 2A'),
(1390, 'Demate, Kent L.', '15101324', 'ldfdG/FUwjb0nzVPqNzv81gcx37h6oLL7Lisgl6ItLk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01324', 'M', 'BSIS', '2', 'BSIS 2B'),
(1391, 'Demate, Zosimo R.', '15100198', 'JRH1zyosKhb0FbXG1/+3h2qzSPZJT3Csy6Feedh83+k=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00198', 'M', 'BSIS', '2', 'BSIS 2A'),
(1392, 'Egay, Joyce V.', '15100722', 'ZLS5ERSqdLldAlxEJJT1EAl7X29qXxGUca8fKJfTHTc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00722', 'F', 'BSIS', '2', 'BSIS 2A'),
(1393, 'Eguia, Fred B.', '15100661', 'it+6zBPSyfPxMgrrIY+Osrn40pY/MyVOgsc44ZBt95U=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00661', 'M', 'BSIS', '2', 'BSIS 2B'),
(1394, 'Famor, Jenny Rose V.', '15101286', 'ogaVMCNJYJboBUC/kFnyKsg0gw/zDkwKtNRcAMmvWIE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01286', 'F', 'BSIS', '2', 'BSIS 2B'),
(1395, 'Farma, Gregg Niño Antoinne G.', '15102114', 'w8OHRL0YNxme4LDVJrG13gugWwGyXtDlhJzN8rgJLoM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02114', 'M', 'BSIS', '2', 'BSIS 2A'),
(1396, 'Floro, Kristine Joy A.', '15102166', '+bxlZUQoJc0kMm1XiVt7d4QfMn9IhKbu8IP0ykaOMOc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02166', 'F', 'BSIS', '2', 'BSIS 2B'),
(1397, 'Francisco, Julie Ann A.', '15101856', 'Ts32rpZNUIHwHjoXR3ua2RkQgHqoiSBoZoIGmlBd3Vo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01856', 'F', 'BSIS', '2', 'BSIS 2A'),
(1398, 'Gagui, Ferna  Allegado', '08100486', 'hAP7urmS5gvJphdGSRSD45vz0JYH2RUHVAuRZ+A9PfM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '08-1-00486', 'F', 'BSIS', '2', 'BSIS 2C'),
(1399, 'Gelig, Gerald T.', '15102132', '0I/k0XL9Zy2bVd5/62dM2T4+nM6dcodqZhr12pQTZzY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02132', 'M', 'BSIS', '2', 'BSIS 2A'),
(1400, 'Gonzales, Marvin J.', '15100650', 'wiqhglglQL1IdvdHRCGlcmbVHMGSJwwGS4Vzrb+So1E=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00650', 'M', 'BSIS', '2', 'BSIS 2A'),
(1401, 'Guardian, Annabelle J.', '15100269', 'xOX/tLTqHqms0y1530T7rw6pYoBTscZrE56KXpvDnME=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00269', 'F', 'BSIS', '2', 'BSIS 2B'),
(1402, 'Habab, Jovelyn D.', '15100190', '+sSMPkZ8P5WYmoQZPK5vcRuhtmE9YhS/4ve0lh2Adjw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00190', 'F', 'BSIS', '2', 'BSIS 2A'),
(1403, 'Ignacio, Latifha Lulette Joy S.', '15100969', 'jPcq4orSPHoHku/UL0wDXY1yGoHcQxOCFIpwvUeryes=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00969', 'F', 'BSIS', '2', 'BSIS 2B'),
(1404, 'Jutara, Christian T.', '15100224', 'vwh0jxdeY/nv7HDocqcCKCpK7OdsHYleRd3LoW6Kws0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00224', 'M', 'BSIS', '2', 'BSIS 2A'),
(1405, 'Lequin, Jolina R.', '15101614', 'mzVo1jyc03wknQ2lzXMTl0iKFM97ixsqAoFuOPTnSH0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01614', 'F', 'BSIS', '2', 'BSIS 2A'),
(1406, 'Libores, Vanessa B.', '15101335', 'FsYZRZxu0+fKwCGqjs6ddcJRhZVq6l6cuLMLWP4qphQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01335', 'F', 'BSIS', '2', 'BSIS 2C'),
(1407, 'Macapinig, Jay Marie M.', '15102150', '30gobk80azHPadF504t1nPiVzxX6etJk4HwfWqfVSJE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02150', 'F', 'BSIS', '2', 'BSIS 2A'),
(1408, 'Macascas, Shiela May Jimenez', '09100379', 'FsYZRZxu0+fKwCGqjs6ddcJRhZVq6l6cuLMLWP4qphQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '09-1-00379', 'F', 'BSIS', '2', 'BSIS 2A'),
(1409, 'Malabo, Mary Joe P.', '15101244', 'TrVQVBA5qyHPnjrS5elKG1KwIBmD89c79nxAj10zMXE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01244', 'F', 'BSIS', '2', 'BSIS 2C'),
(1410, 'Maloloy-on, Pery N.', '15101788', 'FsYZRZxu0+fKwCGqjs6ddcJRhZVq6l6cuLMLWP4qphQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01788', 'M', 'BSIS', '2', 'BSIS 2A'),
(1411, 'Marijuan, Lorna M.', '15100645', '6Bcw8iyW+G02NcXbZ/3+5dkv57gdbarINBkDNvjtRkM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00645', 'F', 'BSIS', '2', 'BSIS 2B'),
(1412, 'Matugas, Roda Mae G.', '15100394', 'Q728rYKpN69H2IJ+voKMjZPGrxhJhV9/dO26AN7/WbI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00394', 'F', 'BSIS', '2', 'BSIS 2A'),
(1413, 'Mercado, Krizyl Demate', '15102382', 'Lq+WoEynRG577weG3xlgrHVvYDSh2c/7mA9YRH6gTwI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02382', 'F', 'BSIS', '2', 'BSIS 2C'),
(1414, 'Morillo, Rolando Jr. R.', '15101641', 'ns3ar6gpWfYhjqVN2IUIIYwy2HldZOZ1kr8saiLMawk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01641', 'M', 'BSIS', '2', 'BSIS 2B'),
(1415, 'Navarra, Vincenth Saclolo', '15102488', 'qu+Nqs+Dwiyzzzf80lq9zkLD5H8q0yOxox+XT4AXs+c=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02488', 'M', 'BSIS', '2', 'BSIS 2B'),
(1416, 'Neply, John Alex A.', '15102068', 'GFWfx1nWYTC6scQcfIXSwX88DBvnKHopQPxSDWC0syA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02068', 'M', 'BSIS', '2', 'BSIS 2C'),
(1417, 'Nierra, Leslie A.', '15102075', 'sZag4TbSQFGmUBppi0C4d5b4H2r5hGJeoQucMYecRYg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02075', 'F', 'BSIS', '2', 'BSIS 2B'),
(1418, 'Nillos, Noelyn Laraño', '15101178', 'BObR/rsp9yAjbRc2GZjgvWS5ZXShRm/n16OoU/Fv+kE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01178', 'F', 'BSIS', '2', 'BSIS 2B'),
(1419, 'Oppus, Kristine G.', '15101174', 'QL0Pdhp2AYbuKcCdD6/M3TYRHk9JOOELrA/mVLQojqk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01174', 'F', 'BSIS', '2', 'BSIS 2C'),
(1420, 'Pacabis, Renato Jr. G.', '15100656', 'XPilvQQzHdmoBxmRLze0OVLAMu9l/sDrlcn6WeTxpkY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00656', 'M', 'BSIS', '2', 'BSIS 2B'),
(1421, 'Padilla, Roland T.', '15100886', 'lsGvCH5Pe3/0sO6Gtvt+bY/6fxwgwEBF9CPYjfIZsGk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00886', 'M', 'BSIS', '2', 'BSIS 2B'),
(1422, 'Pagatpat, Marvie M.', '15100155', 'g3OEKmtHXEvtyaMo2bHeojPlS/Uno8kM1xdrwrC9yDs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00155', 'F', 'BSIS', '2', 'BSIS 2A'),
(1423, 'Panglao, Joanna Atok', '13101450', '+mXReqXtk2kO+BeDsL/1AHvW2bI+gQAhjpOfSSCTayA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01450', 'F', 'BSIS', '2', 'BSIS 2C'),
(1424, 'Panglao, Meljare A.', '15100262', 'vwh0jxdeY/nv7HDocqcCKCpK7OdsHYleRd3LoW6Kws0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00262', 'F', 'BSIS', '2', 'BSIS 2A'),
(1425, 'Payos, Richmalyn B.', '15100287', 'KcZgPhw837G/BoH+jrHux0Md/YWjGtBITSMsfAngIrE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00287', 'F', 'BSIS', '2', 'BSIS 2B'),
(1426, 'Pepito, Marjorie A.', '15100118', 'Y9XwyGMPYsK4sUG63lR03FauVMo6wJYGmFTdqycfNhU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00118', 'F', 'BSIS', '2', 'BSIS 2B'),
(1427, 'Provido, Ma. Thefanny Kathleen R.', '15102155', 'MVzp+An0Ol4FOtPcFHwKrXzB/f1le2vQfA1ZEvE0bB0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02155', 'F', 'BSIS', '2', 'BSIS 2A'),
(1428, 'Regir, Jeifrey S.', '15101362', 'jGwYYRG/nZv7UbovBPQh7oRxD026yaFzwB++X9C8iG4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01362', 'M', 'BSIS', '2', 'BSIS 2B'),
(1429, 'Reyes, Billy Joe Mañacap', '13102052', 'bZnZ4ONDmlDNSqZ7rZErB1CG1U/SluQJfWx/FNvz3Rc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02052', 'M', 'BSIS', '2', 'BSIS 2A'),
(1430, 'Ribot, Rizza P.', '15100174', 'xk/0spzv1ndgXAvvB8Xz6Pfpo3Wc8tvRIrwr5PqMGcQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00174', 'F', 'BSIS', '2', 'BSIS 2A'),
(1431, 'Roble, Grace Baco', '08101103', 'kOoW67sNO9LuAJVEv5/mYE6z4Hsk+w2omf66OIQv+NM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '08-1-01103', 'F', 'BSIS', '2', 'BSIS 2A'),
(1432, 'Rodil, Merry Queen Ruel M.', '14102715', '84iJ8CJZaDzM8NWd1tjKAl3qHnuXCl5TLVdDAXLjevE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02715', 'F', 'BSIS', '2', 'BSIS 2A'),
(1433, 'Romero, Jenny T.', '15101435', 'HMt5kGBvYUfqDP70oDTnf7YQ+VXLYi3mPijBaMFDwo8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01435', 'F', 'BSIS', '2', 'BSIS 2C'),
(1434, 'Rondina, Willford E.', '15100648', 'mrs2dxloCyMsJ8cuOHr6Y9JpIAceGoqp9IPnxxxVsr8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00648', 'M', 'BSIS', '2', 'BSIS 2A'),
(1435, 'Rosario, John Eric Tarrayo', '15100820', 'QL0Pdhp2AYbuKcCdD6/M3TYRHk9JOOELrA/mVLQojqk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00820', 'M', 'BSIS', '2', 'BSIS 2A'),
(1436, 'Rosialda, Raquel J.', '15101347', 'Q728rYKpN69H2IJ+voKMjZPGrxhJhV9/dO26AN7/WbI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01347', 'F', 'BSIS', '2', 'BSIS 2A'),
(1437, 'Sabanal, Aiza S.', '15102234', 'tSEsXoZZ9dagYk3xXarwMaWLqF2DqY1X2efT2gGIamE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02234', 'F', 'BSIS', '2', 'BSIS 2A'),
(1438, 'Sabong, Salvy C.', '15100987', '1iB3LNgkLBmjNMsbasc9cDMyGk95ZybWof1wKRhAU3A=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00987', 'F', 'BSIS', '2', 'BSIS 2B'),
(1439, 'Sabornido, Marvin M.', '15100240', 'hjVcG3+3VrKP1dcssC6GYbCHZfBwhUVgU3kEC4Zrunc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00240', 'M', 'BSIS', '2', 'BSIS 2A'),
(1440, 'Sabornido, Sharniel D.', '15101751', 'SkW1503Uae+npBevUN9sbXXx1YljLcQ0BXSsPlPG17M=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01751', 'F', 'BSIS', '2', 'BSIS 2A'),
(1441, 'Sacare, Jean Clare P.', '15100112', '6KgWVCLzgcyb/oGM7cirI3IyXjbC/eYmPBC1qLoKPko=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00112', 'F', 'BSIS', '2', 'BSIS 2A'),
(1442, 'Salele, Floramae B.', '15102016', 'Y9XwyGMPYsK4sUG63lR03FauVMo6wJYGmFTdqycfNhU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02016', 'F', 'BSIS', '2', 'BSIS 2A'),
(1443, 'Salvo, Marilyn A.', '15100307', 'MrqiMEbFkmy1pGZbewBbb3JJzf6pY+sQC481mVWkWRc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00307', 'F', 'BSIS', '2', 'BSIS 2A'),
(1444, 'Samante, Reymart C.', '15101314', '08KpLG9VYiTsQooARigwseiyQhFV2s6vTQTAGYuH3eI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01314', 'M', 'BSIS', '2', 'BSIS 2B'),
(1445, 'Sanchez, Inday Nelly Abogado', '15100803', 'BVDE/RdTeHokwqySVA3pqEAA/bNdL7rFZpz9xad8Nug=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00803', 'F', 'BSIS', '2', 'BSIS 2A'),
(1446, 'Santuele, Mary Rose T.', '15101456', 'mrs2dxloCyMsJ8cuOHr6Y9JpIAceGoqp9IPnxxxVsr8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01456', 'F', 'BSIS', '2', 'BSIS 2A'),
(1447, 'Seno, Mark Christian C.', '15100791', 'oJIvKZUFCtHYLsRpo2QUxo3YcC9glpZgLbUF0xEIeT4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00791', 'M', 'BSIS', '2', 'BSIS 2B'),
(1448, 'Soria, Joselito M.', '15100764', 'g1VMr4zmcscCcN93OzFkCaiuvYTZephTjixmIH/dqxc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00764', 'M', 'BSIS', '2', 'BSIS 2B'),
(1449, 'Soria, Justsier A.', '15100817', 'Zq13T5ceyb3Q91qVEYxroVx+v8KKdntypkr2fsBEp08=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00817', 'M', 'BSIS', '2', 'BSIS 2A'),
(1450, 'Sotto, Jeth Ford C.', '15102078', '1iB3LNgkLBmjNMsbasc9cDMyGk95ZybWof1wKRhAU3A=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02078', 'M', 'BSIS', '2', 'BSIS 2A'),
(1451, 'Tizon, Jonnalyn A.', '15100148', 'hjVcG3+3VrKP1dcssC6GYbCHZfBwhUVgU3kEC4Zrunc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00148', 'F', 'BSIS', '2', 'BSIS 2A'),
(1452, 'Udtohan, Rodel R.', '15102251', '4qdgC4w2d0/KBTKGvwUSVoVPNcsECLlNOk54rBF4tL8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02251', 'M', 'BSIS', '2', 'BSIS 2A'),
(1453, 'Verian, Arjie N.', '15100193', 'lsGvCH5Pe3/0sO6Gtvt+bY/6fxwgwEBF9CPYjfIZsGk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00193', 'M', 'BSIS', '2', 'BSIS 2B'),
(1454, 'Villotes, Crisalyn B.', '15100653', 'Nu5fFaDTUZPYZDCuqLY0LVjc4OSWktNbdvhrW9qVLt4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00653', 'F', 'BSIS', '2', 'BSIS 2A'),
(1455, 'Villotes, Dolly Mae B.', '15100651', 'QjnGQblI31PROzPcuFjiLMtkUHulYp9qETjcu0BNQ6Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00651', 'F', 'BSIS', '2', 'BSIS 2A'),
(1456, 'Acuin, Girly Nazareno', '14102456', 'GFWfx1nWYTC6scQcfIXSwX88DBvnKHopQPxSDWC0syA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02456', 'F', 'BSIS', '3', 'BSIS 3B'),
(1457, 'Alemanza, Rustom Mahinay', '14102350', 'n0FJByWQ7XZDD0BpVzmgtYruKrjMXEOy0Lupfc2mhKA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02350', 'M', 'BSIS', '3', 'BSIS 3A'),
(1458, 'Arellano, Victor III Tan', '08100135', 'VfGQBs6qpCsfa+LyzbuzK+VbcMVQoB1v+/G+sFlGhSg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '08-1-00135', 'M', 'BSIS', '3', 'BSIS 3A'),
(1459, 'Arevalo, Roselyne O.', '14100665', 'zv4oDbKVLXFJcg9dTaC1h93wR3u2lIB4Wrt203COZaY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00665', 'F', 'BSIS', '3', 'BSIS 3A'),
(1460, 'Avelino, Shaira Lazarte', '14101958', 'ADbzpGdvAtbNBVEnCs4NOStYRzmUuQNFDRxYdKphtao=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01958', 'F', 'BSIS', '3', 'BSIS 3A'),
(1461, 'Badon, Robert Roy C.', '14101338', '5DLaNa7OG5Fe3pgjhVtUBXl2W7r6kwLqH+S1eMt0qCg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01338', 'F', 'BSIS', '3', 'BSIS 3A'),
(1462, 'Bañas, Grecel Tambis', '14101858', 'xk/0spzv1ndgXAvvB8Xz6Pfpo3Wc8tvRIrwr5PqMGcQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01858', 'F', 'BSIS', '3', 'BSIS 3A'),
(1463, 'Bargan, Bebelyn Toñacao', '14102512', '30gobk80azHPadF504t1nPiVzxX6etJk4HwfWqfVSJE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02512', 'F', 'BSIS', '3', 'BSIS 3A'),
(1464, 'Barriga, Rafael John Maranga', '14102535', 'vBQBE8C3gUjQPQbotKT2yXdvgcmRilHSPkSun9fqO50=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02535', 'M', 'BSIS', '3', 'BSIS 3A'),
(1465, 'Bartolo, Jean Erika Gonzales', '13100581', 'HMt5kGBvYUfqDP70oDTnf7YQ+VXLYi3mPijBaMFDwo8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00581', 'F', 'BSIS', '3', 'BSIS 3A'),
(1466, 'Beronio, Diana L.', '14101221', 'ADbzpGdvAtbNBVEnCs4NOStYRzmUuQNFDRxYdKphtao=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01221', 'F', 'BSIS', '3', 'BSIS 3A'),
(1467, 'Bontes, Lee Leander Hotricano', '13101544', 'Qp3elo9qYHYdFVrDyD4JfQeRreaSz/kwn52Aai8/eyQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01544', 'M', 'BSIS', '3', 'BSIS 3A'),
(1468, 'Caliao, Airra T.', '14101202', 'VMxMxO9cHE+IRTe7PxWVpTa5Tgh+d/PsWewqToZFRAM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01202', 'F', 'BSIS', '3', 'BSIS 3A'),
(1469, 'Caliao, Jamaica  Lopez', '11101399', 'DjOoKetk5hi+9NdUs17815kj44LBVVogoxMNy/1HsVE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-01399', 'F', 'BSIS', '3', 'BSIS 3A'),
(1470, 'Cano, Mark Anthony Robaro', '15101911', '/7t45BKVg3N7dFbKDITzm2uQibZILvWqiGu+0bgjThA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01911', 'M', 'BSIS', '3', 'BSIS 3A'),
(1471, 'Caparro, Leslie V.', '14101159', 'dgi3BnQY2bGoqhSZYq+fUv5rukARIeT6qc6p7xcOn1c=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01159', 'F', 'BSIS', '3', 'BSIS 3A'),
(1472, 'Casalta, MAry Jean L.', '14101369', 'slE6CODkbmFtPOOI1LgJfVbrRuw1lp3NYnNz65TdZEo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01369', 'F', 'BSIS', '3', 'BSIS 3A'),
(1473, 'Co, Maila Jessanine Lagunay', '14102649', 'xOX/tLTqHqms0y1530T7rw6pYoBTscZrE56KXpvDnME=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02649', 'F', 'BSIS', '3', 'BSIS 3A'),
(1474, 'Corpin, Darwen Rosal', '14101435', '+sSMPkZ8P5WYmoQZPK5vcRuhtmE9YhS/4ve0lh2Adjw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01435', 'M', 'BSIS', '3', 'BSIS 3A'),
(1475, 'Daan, Dario Retanal', '14102339', '30gobk80azHPadF504t1nPiVzxX6etJk4HwfWqfVSJE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02339', 'M', 'BSIS', '3', 'BSIS 3A'),
(1476, 'dela Peña, Maria Azucena M.', '14101108', 'PxMZfqlGpmerGxVCbK2PWGBlnqz4VFYfZJNNVSvUONk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01108', 'F', 'BSIS', '3', 'BSIS 3A'),
(1477, 'Delda, Genie May G.', '14101660', 'ZVQ7JpGoMO170s68Pj+r8S2Jfl57VDvTx+UGYDA+En8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01660', 'F', 'BSIS', '3', 'BSIS 3A'),
(1478, 'Demasilan, Angielie N.', '14101193', 'Nu5fFaDTUZPYZDCuqLY0LVjc4OSWktNbdvhrW9qVLt4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01193', 'F', 'BSIS', '3', 'BSIS 3A'),
(1479, 'Domingo, Cyril Rosales', '14102523', '3emb3jikH8yC16fQTLNhRBVOUO7fs1CA8cKOTvLdrNo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02523', 'F', 'BSIS', '3', 'BSIS 3A'),
(1480, 'Egot III, Esperedion L.', '14101622', 'bf4tinJfrkmpiV4NUSGtKd7ExU1Q8hj9Xv5RmIuugfg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01622', 'F', 'BSIS', '3', 'BSIS 3A'),
(1481, 'Fajardo, Abegael Gepega', '14100600', 'sZag4TbSQFGmUBppi0C4d5b4H2r5hGJeoQucMYecRYg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00600', 'F', 'BSIS', '3', 'BSIS 3A'),
(1482, 'Florida, Macr Venzon A.', '14100281', '+iy+8Y1nEjvzHFeMlk7RIPAalCMUtAzljp9WjqHq4vo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00281', 'M', 'BSIS', '3', 'BSIS 3A'),
(1483, 'Gamos, Beltran Jr. P.', '14101175', 'DUf7xG9jpqKSNJ+prR2Rv6hyCcV5nHWQLsHzYFYMH1Y=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01175', 'M', 'BSIS', '3', 'BSIS 3A'),
(1484, 'Honrejas, Marie Rose M.', '14101365', '65ry8wRT6WOXV9WNIvjwZuRxQLjK8/47VTwZqu6iELE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01365', 'F', 'BSIS', '3', 'BSIS 3A'),
(1485, 'Jaguines, Ranaliza H.', '14100688', 'HYLLFrt1Kwb9K/R+WjYKIYSetQwTZ4k6a1P2jrkEfkg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00688', 'F', 'BSIS', '3', 'BSIS 3A'),
(1486, 'Lasaca, Rhose Dianne Solayao', '14102659', 'M5BzSHandpsDZnITE5ErmKLt79JJakrAmkiqZ8aC0ZQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02659', 'F', 'BSIS', '3', 'BSIS 3A'),
(1487, 'Lood, Rochelle E.', '14101102', 'D3Hi5a4pNYCkFVHItXQEp25Uh3dIFswKEqwOqMoMYbY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01102', 'F', 'BSIS', '3', 'BSIS 3A'),
(1488, 'Luriaga, Crisabel E.', '14100692', 'BD3Gho4d9gYB0/fO0Yw0MqTT7beCPEV3w81O53GczHg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00692', 'F', 'BSIS', '3', 'BSIS 3A'),
(1489, 'Mahilom, Nae Karen N.', '14102084', 'Is/hS+y643mU26+6BuF7d0eTQfP52PFoX3TaV1g5LCM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02084', 'F', 'BSIS', '3', 'BSIS 3A'),
(1490, 'Malate, Maridel Molito', '14102557', 'NnZUg9xnTuJCp0XZbEx3+2/9+Tj1zLouZqSj5Tnztlo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02557', 'F', 'BSIS', '3', 'BSIS 3A'),
(1491, 'Malinao, Lovely Jane G.', '14101116', '69YVOih4Vv+dQwJsUQNljE7XArcmR0XdXIhjxARpri8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01116', 'F', 'BSIS', '3', 'BSIS 3A'),
(1492, 'Manatad, Janine D.', '14100921', 'mkD4QxAFH2OyWAXsmtX1UWDVhHcfhcIy//uuKLC3Qt8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00921', 'F', 'BSIS', '3', 'BSIS 3A'),
(1493, 'Mapue, Mike Jefferson Dadizon', '13101026', 'ohiEGY6dOwv5nUcd9aHsvSNFq8nOHyVc2JEWzpo5ius=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01026', 'M', 'BSIS', '3', 'BSIS 3A'),
(1494, 'Maurillo, Novelyn Muldera', '13100379', '1+vo9NB1IUWSAjYdUp63o+E3xPQ/Rda0/IxApi7BqZw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00379', 'F', 'BSIS', '3', 'BSIS 3A'),
(1495, 'Monzales, Edielhyn P.', '14101570', 'kaF9SaooRvwO/a5xmTt0uWaz0vKWTBTFb+50P54aD2Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01570', 'F', 'BSIS', '3', 'BSIS 3A'),
(1496, 'Orongan, Ruby V.', '14100839', '7FORsGasa2Si5PLINYpYO//WvwRgIHhqR2Y+YtVMQ00=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00839', 'F', 'BSIS', '3', 'BSIS 3A'),
(1497, 'Paghid, Stephanie S.', '14101215', 'mrDh3W454wOO1eNV1J+ybaon+noOsfJAB2gt07LiMXI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01215', 'F', 'BSIS', '3', 'BSIS 3A'),
(1498, 'Quinto, Niño Consas', '13100321', 'ICouHPCSs5WNEEv8O2ZCrtaopgJQdJ6HIjUt0w/tgP8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00321', 'M', 'BSIS', '3', 'BSIS 4A'),
(1499, 'Requiso, Marielle Udtohan', '14101974', 'hbTs1SKIAAVL8wcHbVO/mxNo70hoEvZxR6EbHypvFiQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01974', 'F', 'BSIS', '3', 'BSIS 3A'),
(1500, 'Rosario, Leo Mark Baguna', '14102338', 'SZ2X8KdJktYl1VAuo1eWaed9BGAaF13uiivajar6gqM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02338', 'M', 'BSIS', '3', 'BSIS 3A'),
(1501, 'Rosel, Manilyn U.', '15102159', 'T1tCDYUnPBoWpjd7usSjkQlB1ZMwhkRNJyVpDE6oK3w=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02159', 'F', 'BSIS', '3', 'BSIS 3A'),
(1502, 'Ruizol, Laarni S.', '14102156', 'gvR02j5STlJaU4z0ThW+U/fOtxAriqZnc1IM3etUNPQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02156', 'F', 'BSIS', '3', 'BSIS 3A'),
(1503, 'Sabonsolin, Eljun Sepra', '10001702', 'xtDlVbun8Xw5MMxp2ywjF8LN7Bnd9SZdNh0mt6bqkH4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '10-0-01702', 'M', 'BSIS', '3', 'BSIS 3A'),
(1504, 'Saludar ,Jeah A.', '14100100', 'NEPk/1yn9E6W14o7SmRhixlo++pG6MCLO/x67QeQAUk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00100', 'F', 'BSIS', '3', 'BSIS 3A'),
(1505, 'Veliganio, Rechie Mendiola', '11100760', '9OAmgL3oCsltvp5+U//QIccE4532mQw6vGP7yRXFgK0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-00760', 'M', 'BSIS', '3', 'BSIS 3A'),
(1506, 'Yase, Marybeth T.', '14101572', 'frRx1XVQVu3+CwG+Fc5LxG5L5TidxGxPZueCOmttoUA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01572', 'F', 'BSIS', '3', 'BSIS 3A'),
(1507, 'Ysulan, Gerson Silvano', '14102354', 'D7zsKtwvjOLB09f1eq7ALSe+mx9eyr+p0JoKdS8WX6Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02354', 'M', 'BSIS', '3', 'BSIS 3A'),
(1508, 'Bayani, Jhonray Costaño', '12101567', 'THFW8wSqZKIZbFXagxX+w5ld6Rbx6pwvzRzAzVyB/6M=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01567', 'M', 'BSIS', '4', 'BSIS 4A'),
(1509, 'Corpin, Jemark  Gervacio', '09101728', 'HYLLFrt1Kwb9K/R+WjYKIYSetQwTZ4k6a1P2jrkEfkg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '09-1-01728', 'M', 'BSIS', '4', 'BSIS 4A'),
(1510, 'Delantar, Jephany Ibatan', '12101943', 'Zn/DECbjDlP4njNKOayTX01NHX/kbjWkDeRNSG3RFqc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01943', 'F', 'BSIS', '4', 'BSIS 4A'),
(1511, 'Delis, Leonard Abanilla', '13101989', '1tW/2Lm+i84B4Tx3lSEc4uulpeTHD5q3ltdW7OVTVVM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01989', 'M', 'BSIS', '4', 'BSIS 4A'),
(1512, 'Demate, Annold Fer Patente', '12200035', 'ztLKlEFAMkhhUMYIT7pZKj6uyIOvEwKez4WHV+SZyno=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-2-00035', 'M', 'BSIS', '4', 'BSIS 4A'),
(1513, 'Diana, Teresita Velosa', '13102317', '4qJ1y0mP166G9mXZ6hBRMAMk52JytxToYMCkTBJQl+k=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02317', 'F', 'BSIS', '4', 'BSIS 4A'),
(1514, 'Fabon, Mari Grace Avelino', '13101899', 'yJazKezSal/hiamsAYAQITXXL5zUAfYHa8DDgVw45Zw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01899', 'F', 'BSIS', '4', 'BSIS 4A'),
(1515, 'Faustino, Ernie Letran', '11102082', '1tW/2Lm+i84B4Tx3lSEc4uulpeTHD5q3ltdW7OVTVVM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-02082', 'M', 'BSIS', '4', 'BSIS 4A'),
(1516, 'Juntilla, Victorio Jr. Saclolo', '13101020', 'Pq5PJ5J0cKh4N0il2LAhC8StaoOcEWCgcIwblrq1W0I=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01020', 'M', 'BSIS', '4', 'BSIS 4A'),
(1517, 'Lagrosas, Joenissa Malabat', '13101809', 'ScUhZ2HRLfWloXK+GjQF08AJo18w1TqUDGamlUc1BNQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01809', 'F', 'BSIS', '4', 'BSIS 4A'),
(1518, 'Ligmon, Mierasul Tagocon', '13100339', 'vlZwSKZXhpwtNbXRWDrvCe+Y9famsXCceY1gjnTDtEM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00339', 'F', 'BSIS', '4', 'BSIS 4A'),
(1519, 'Loriaga, Lorainne Honrejas', '13101948', 'MYBE6JdoKBQeLCjy2TukQ1oRnuemO5fh+Aq1ruLhx3o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01948', 'F', 'BSIS', '4', 'BSIS 4A'),
(1520, 'Mejarito, Aprilene Lamban', '13100105', 'vzwkgqXBcU/rxytRJdO0ngWQVxtF85CFnUYUdPoun3k=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00105', 'F', 'BSIS', '4', 'BSIS 4A'),
(1521, 'Misa, Sam Remo Redoloza', '12101261', 'F7i61SzLztV+ZOm27ylnJXB+6bPIy8aPHLdBWmOxyNg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01261', 'M', 'BSIS', '4', 'BSIS 4A'),
(1522, 'Monfiel, Ailyn Subito', '12101828', 'BP2dnEBXTMH1pP9Fo8pxj6Ef6sVRUNfAoqQEANWx26A=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01828', 'F', 'BSIS', '4', 'BSIS 4A'),
(1523, 'Odeña, Van Francis Reyes', '13100270', 'gMhalzXJxlGgeyW3ZdoSyEz9w3nlhL+JgrxLbv5lSwg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00270', 'M', 'BSIS', '4', 'BSIS 4A'),
(1524, 'Pacabez, Rizalyn Garin', '13101661', 'kBhlVPdgZQ0cRdhJmr9umV3hI/HnTJOgS51cxNwIv0A=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01661', 'F', 'BSIS', '4', 'BSIS 4A'),
(1525, 'Reñola, Honey Grace Camacho', '13101849', 'ScUhZ2HRLfWloXK+GjQF08AJo18w1TqUDGamlUc1BNQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01849', 'F', 'BSIS', '4', 'BSIS 4A'),
(1526, 'Reposar, Eunice Alburo', '13101204', '4X29KGTH5cwaqYi0VSrkM3DWFE05aSnKzevuSl2jkWA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01204', 'F', 'BSIS', '4', 'BSIS 4A'),
(1527, 'Rosales, Jeah Marie Majadas', '13101602', 'D3Hi5a4pNYCkFVHItXQEp25Uh3dIFswKEqwOqMoMYbY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01602', 'F', 'BSIS', '4', 'BSIS 4A'),
(1528, 'Sabado, Mike Sadoguio', '13101688', 'IKkB15V/RQP6fQoOQ7icVINzjazSKi3Su0qc//0N3Sg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01688', 'M', 'BSIS', '4', 'BSIS 4A'),
(1529, 'Salacayan, Myra C.', '13100113', 'vfcQh7XpDHurpaz/B10qR3QS2Bw+ZG97uf9WkhBw/F0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00113', 'F', 'BSIS', '4', 'BSIS 4A'),
(1530, 'Salele, Sheila Marie Bayang', '13101862', 'cyVRKcAKNeTiLMm8Zo+/X0TColRPrH9H8mGbUnZHIxU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01862', 'F', 'BSIS', '4', 'BSIS 4A'),
(1531, 'Sales, Cherry Ann Elias', '13101640', 'Pnu9Hnr4N4aeaBCa3C5ikaea7anQ/gdpmwr6NBT6IUw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01640', 'F', 'BSIS', '4', 'BSIS 4A'),
(1532, 'Sañosa, Jade Juaniza', '12101190', 'PT2sz9y/7h3nYoCS+wDmTRqZLbdYeUx8k6J4S+0bZxM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01190', 'M', 'BSIS', '4', 'BSIS 4A'),
(1533, 'Sañosa, Jojane  Ebina', '09101732', 'ScUhZ2HRLfWloXK+GjQF08AJo18w1TqUDGamlUc1BNQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '09-1-01732', 'F', 'BSIS', '4', 'BSIS 4A'),
(1534, 'Villamor, Estiffany Anne R', '12200036', 'QdOU2vpnxJPHQSfoT/GM6EdzjZ59wmFzn4HlkWVcBO0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-2-00036', 'F', 'BSIS', '4', 'BSIS 4A'),
(1535, 'Cadion, Edbert', '16100938', '5H1Gc0f/hcM5L6QuU20QRtziQ0UrotpAlMui4XKL0JI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00938', 'M', 'BSIT', '1', 'BSIT 1C'),
(1536, 'Cadion, Reynaldo', '16100940', 'DZjl1qfh0KhIP+WMCiYe2W2coGMlt7U/gbpMeRzuYpA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00940', 'M', 'BSIT', '1', 'BSIT 1C'),
(1537, 'Cerera, Denis Bacalla', '11100551', '2OpOlbfXnM8Vp7pOq0Nk2k8dp8YEYXFDnfoy3CexIuI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-00551', 'M', 'BSIT', '1', 'BSIT 1A'),
(1538, 'Kim, Yong Hyun', '16100931', 'gvR02j5STlJaU4z0ThW+U/fOtxAriqZnc1IM3etUNPQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00931', 'M', 'BSIT', '1', 'BSIT 1C'),
(1539, 'Maburao, Anjo', '16100941', 'yklHA/jK5w3e0fzRr/w1gRU63rrcenkpvg4WMa4SL4s=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00941', '', 'BSIT', '1', 'BSIT 1C'),
(1540, 'Matuguinas, Mark Jaylo Carbo', '15102055', 'zul1mKgWWo5p6nIGRMWHk3Iss5jbPFJNv5nZeMiLrS0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02055', 'M', 'BSIT', '1', 'BSIT 1C'),
(1541, 'Paanod, Wawie Ambi', '11101272', 'Rf1OjFAoajdpdN1ORILh8nN2BH2oO0dFjZDl/ZxfXz8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-01272', 'M', 'BSIT', '1', 'BSIT 2-AUTO'),
(1542, 'Perdugas, John Paul  Casi', '11101842', 'cgPRKNqzzx3n6e0vxkYS1fO2BsHUvBfn6BB5LMM9NCc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-01842', 'M', 'BSIT', '1', 'BSIT 1C'),
(1543, 'Perido, Rodolfo Jr. Rasonabe', '16100935', 'D3Hi5a4pNYCkFVHItXQEp25Uh3dIFswKEqwOqMoMYbY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00935', '', 'BSIT', '1', 'BSIT 1C'),
(1544, 'Almen, Danilo, Jr  Vere', '11101101', 'x9YmFixz6HcQPvoTzzARMXC2g5n/4HLf65m1pgfSvVo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-01101', 'M', 'BSIT', '3', 'BSIT 3-AUTO'),
(1545, 'Lacson, Israel Garin', '08100494', 'mrDh3W454wOO1eNV1J+ybaon+noOsfJAB2gt07LiMXI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '08-1-00494', 'M', 'BSIT', '3', 'BSIT 3-ELECTRO'),
(1546, 'Aban, Jame Carlo', '14100025', 'AjD8PznIetOQ4XxdWFbQ8cSMS8C8+eJ/YZ7LPHvpe+A=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00025', 'M', 'BSIT-AUTO', '1', 'BSIT 3-AUTO B'),
(1547, 'Acdog, Kirk Marilao', '07100730', 'xv6ZOwLMNIdr0VeOh/+k4f1JB3Ps5SOIGjfkEqDyCAU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '07-1-00730', 'M', 'BSIT-AUTO', '1', 'BSIT 3-AUTO B'),
(1548, 'Alfanoso, Willian Cabello', '15102144', 'zul1mKgWWo5p6nIGRMWHk3Iss5jbPFJNv5nZeMiLrS0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02144', 'M', 'BSIT-AUTO', '1', 'BSIT 2-AUTOMOTI'),
(1549, 'Amoroto, Jepe Morga', '16100260', '4gLRDL7QJcCNF5Teq2mw22V9JZ+kfWFeS7ceMb6raEg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00260', 'M', 'BSIT-AUTO', '1', 'BSIT 1A'),
(1550, 'Apolinar, Albert F.', '15101608', '1+vo9NB1IUWSAjYdUp63o+E3xPQ/Rda0/IxApi7BqZw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01608', 'M', 'BSIT-AUTO', '1', 'BSIT 2-AUTOMOTI'),
(1551, 'Apolinar, Raphy Auza', '10102319', 'kaF9SaooRvwO/a5xmTt0uWaz0vKWTBTFb+50P54aD2Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '10-1-02319', 'M', 'BSIT-AUTO', '1', 'BSIT 1A AUTO'),
(1552, 'Arrio, James Bong S.', '14101229', '32vIraX4tB7uAbxUACKtMINclJDa2UA7zqMCX9yhFAg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01229', 'M', 'BSIT-AUTO', '1', 'BSIT 3-AUTO B'),
(1553, 'Astilla, Jay Ralph Clifford Batac', '16100537', 'DJTJY7iAl9uBfkcZ60IVT+xND/ZUmPIu8CU7Iqx6FWg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00537', 'M', 'BSIT-AUTO', '1', 'BSIT 1B'),
(1554, 'Balbuena, Rodel', '16100436', 'dmOUqipuM91HAI0FpfhbU5sTSpSYS54A0/uXQSrJRL8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00436', 'M', 'BSIT-AUTO', '1', 'BSIT 1A'),
(1555, 'Baleyos, Robert S', '14101759', 'RmAZgKv09dfmY20DlMdhfMjhDjzPOUmqF+7HINgXAqA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01759', 'M', 'BSIT-AUTO', '1', 'BSIT 3-AUTO B'),
(1556, 'BAYO, RYAN MACASCAS', '16100889', 'DZjl1qfh0KhIP+WMCiYe2W2coGMlt7U/gbpMeRzuYpA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00889', 'M', 'BSIT-AUTO', '1', 'BSIT 1C'),
(1557, 'Bermoy, Erwin Yap', '16100360', 'N+YA1OQxsEjgrA4uBv/rnIqoYPiFAtvNHnICiqfQ76g=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00360', 'M', 'BSIT-AUTO', '1', 'BSIT 1A'),
(1558, 'Bernil, Gerbie Dela Cruz', '16100346', 'ZXj09JHIdHBmGCYhuTPhKvyd3XCdK34lpMvP3Nd/WPU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00346', 'M', 'BSIT-AUTO', '1', 'BSIT 1A'),
(1559, 'Caliao, John Mark Lopez', '09101725', 'GxT0dpoxhVOBJCV6GBGrRHUdlPo1ro6+6VEq9CnVhbU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '09-1-01725', 'M', 'BSIT-AUTO', '1', 'BSIT 3-AUTO B'),
(1560, 'Catam-isan, Joseph Mackenzie Icain', '16100355', 'vzwkgqXBcU/rxytRJdO0ngWQVxtF85CFnUYUdPoun3k=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00355', 'M', 'BSIT-AUTO', '1', 'BSIT 1A'),
(1561, 'Cobre, Ryan Omega', '16100289', 'GWIFEse1GHGhAglsWWfJFLcQsXirw+X3Rq88D3dmpyc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00289', 'M', 'BSIT-AUTO', '1', 'BSIT 1A'),
(1562, 'Cortez, Anthony Macayan', '15200057', 'Yl2D9vWq7bSkAM2yFtFtzpj5kg9TW7nBKxYCLKratoc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00057', 'M', 'BSIT-AUTO', '1', 'BSIT 1B'),
(1563, 'Corzon, Jovanni Eligue', '12102220', 'z3sPfOwxTfDpRMmMqYrb+OjdjqvmAD+AxPBGcLImHco=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-02220', 'M', 'BSIT-AUTO', '1', 'BSIT 1A AUTO'),
(1564, 'Dangel, Johnray G.', '14101508', 'v0uUSyLP6eyQEC69zviFXeprgv76tmn9ftVJW5sI4IM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01508', 'M', 'BSIT-AUTO', '1', 'BSIT 3-AUTO B'),
(1565, 'Delantar, Erwin Cabillan', '16100295', 'HYLLFrt1Kwb9K/R+WjYKIYSetQwTZ4k6a1P2jrkEfkg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00295', 'M', 'BSIT-AUTO', '1', 'BSIT 1A'),
(1566, 'Delgado, Ramil R.', '14101248', 'zPcAhvKtd2gz+2jM9Peevpb2BJPP0DRcO6QxOERjA2w=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01248', 'M', 'BSIT-AUTO', '1', 'BSIT 3-AUTO B');
INSERT INTO `tbl_students` (`id`, `name`, `username`, `password`, `email`, `contact_number`, `birthdate`, `type`, `detail`, `active`, `user_id`, `date_added`, `date_modified`, `CODE`, `SEX`, `CRSCD`, `YEAR`, `SECCD`) VALUES
(1567, 'DEMATE, CRISTIAN E.', '16100795', 'kaF9SaooRvwO/a5xmTt0uWaz0vKWTBTFb+50P54aD2Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00795', 'M', 'BSIT-AUTO', '1', 'BSIT 1C'),
(1568, 'DONZAL, SPENCER PIAD', '16100921', 'x9YmFixz6HcQPvoTzzARMXC2g5n/4HLf65m1pgfSvVo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00921', 'M', 'BSIT-AUTO', '1', 'BSIT 1C'),
(1569, 'Gonzaga, Orlando Majait', '16100384', 'f83CNK0odLftY9qW7Afpg7KKFVhBGmmOHJZ/GEkXs9Y=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00384', 'M', 'BSIT-AUTO', '1', 'BSIT 1A'),
(1570, 'Labine, Nathaniel Arnejo', '15102210', 'noPeuAdbGYE0GiGeCoZwKGVj0VFiAYKbYPhFe7j2Q54=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02210', 'M', 'BSIT-AUTO', '1', 'BSIT 1A AUTO'),
(1571, 'Lacano, Jay-ar B.', '15200084', 'Nhqi9/BrtRcrT7Nal6O2VOpf7dfTqbdB1c4ApU7Ayy4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00084', 'M', 'BSIT-AUTO', '1', 'BSIT 1B'),
(1572, 'Llorag, Jairo Betellones', '16100320', 'VjmAo/60hUMW0b+Hgi6iWqmTbzfPHfruri7BTt0ZV8g=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00320', 'M', 'BSIT-AUTO', '1', 'BSIT 1A'),
(1573, 'Lolo, Nelson Orais', '15200016', 'N+YA1OQxsEjgrA4uBv/rnIqoYPiFAtvNHnICiqfQ76g=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00016', 'M', 'BSIT-AUTO', '1', 'BSIT 1B'),
(1574, 'MADERAZO, MARK JOEY DAANG', '16100693', 'gvR02j5STlJaU4z0ThW+U/fOtxAriqZnc1IM3etUNPQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00693', 'M', 'BSIT-AUTO', '1', 'BSIT 1C'),
(1575, 'Malabo, Marvin D.', '14102101', 'siOMUU9XigDxreRVe+J/hV5AeIJ+RslEY8+KD/PdlyE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02101', 'M', 'BSIT-AUTO', '1', 'BSIT 3-AUTO B'),
(1576, 'MENDOZA, OGIE VILLARIN', '16100594', 'ofL2bFuR8ghIFxmpmkbJMjqL8rH+1Mn3jfma0VaDkFU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00594', 'M', 'BSIT-AUTO', '1', 'BSIT 1B'),
(1577, 'MONDEJAR, DEXTER PANTAS', '16100598', 'XQMQTnWaVb9BF+PfBV1kRssIr0DNnG297sG4YUuyGLE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00598', 'M', 'BSIT-AUTO', '1', 'BSIT 1B'),
(1578, 'MONTES, PATRICK PANTAS', '16100592', '49arMMpHIAxlEXF3zV8kppAS8Psv3nUEYwwa4hptsDs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00592', 'M', 'BSIT-AUTO', '1', 'BSIT 1B'),
(1579, 'Morillo, Meljohn  Garin', '10102289', 'siOMUU9XigDxreRVe+J/hV5AeIJ+RslEY8+KD/PdlyE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '10-1-02289', 'M', 'BSIT-AUTO', '1', 'BSIT 3-AUTO'),
(1580, 'NARIDO, DARYL SALUDAR', '16100631', 'yHXKS2PZiatZnvgbTwC2wot9pCISg6pIJekMhPrm7S4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00631', 'M', 'BSIT-AUTO', '1', 'BSIT 1B'),
(1581, 'Narrido Jeahane Areglo', '14101359', 'DzItqR5lfjoZzDL2ietDUyQgnE6d17SAD85gYqJUMdk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01359', 'M', 'BSIT-AUTO', '1', 'BSIT 2-AUTOMOTI'),
(1582, 'Necesario Aron Bornillo', '14200009', 'amjaCtrkuk6jd756jzcB+OgQs96mdiUvVtzuA1LISRM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-2-00009', 'M', 'BSIT-AUTO', '1', 'BSIT 2-AUTOMOTI'),
(1583, 'Onting, Dale Clarence G.', '14100596', '5H1Gc0f/hcM5L6QuU20QRtziQ0UrotpAlMui4XKL0JI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00596', 'M', 'BSIT-AUTO', '1', 'BSIT 1B'),
(1584, 'PADOGA, KENETH JOHN S.', '16100703', 'M/eoHWdczp4ynLGXfFBS7SV0BGT0QQsTIAgTC5IDtt0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00703', 'M', 'BSIT-AUTO', '1', 'BSIT 1B'),
(1585, 'PAUTAN, DONIEL S.', '16100796', 'mrDh3W454wOO1eNV1J+ybaon+noOsfJAB2gt07LiMXI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00796', 'M', 'BSIT-AUTO', '1', 'BSIT 1C'),
(1586, 'Rosende, Melvin Lacaba', '14101976', 'amjaCtrkuk6jd756jzcB+OgQs96mdiUvVtzuA1LISRM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01976', 'M', 'BSIT-AUTO', '1', 'BSIT 2-AUTOMOTI'),
(1587, 'Sadoguio, Aj Lloyd Tambis', '15200010', 'u/Mu57qCiab2fR+Sr/F1bUy4azdFME2Gmuw3BgScPKE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00010', 'M', 'BSIT-AUTO', '1', 'BSIT 1B'),
(1588, 'Salino, Bryan B.', '14101488', '7FORsGasa2Si5PLINYpYO//WvwRgIHhqR2Y+YtVMQ00=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01488', 'M', 'BSIT-AUTO', '1', 'BSIT 3-AUTO B'),
(1589, 'Salvame, Jovany Rico', '16100386', 'kNGFtNuFMp/5lH6OEBNeL4jZfpkjh9sJ7bBnCF+dli8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00386', 'M', 'BSIT-AUTO', '1', 'BSIT 1A'),
(1590, 'Solite, Jovan Malingo', '15200043', '+Y+J8jVLB5/joZO13e/vIgGMV6wBYLL1Wpix/MkAXMk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00043', 'M', 'BSIT-AUTO', '1', 'BSIT 1C'),
(1591, 'TALAÑA, EMERALD REALISTA', '16100668', 'Ld3qRNNMXo5Ruh/APw9ZV+disgAFPLnGMqN2r54IWdM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00668', 'M', 'BSIT-AUTO', '1', 'BSIT 1B'),
(1592, 'Tumulak, Paulie Sadoguio', '15200015', 'HhCTxxsuvRxh/mRsFZL0+4P1OrPYL0X4vkJnFBI98go=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00015', 'M', 'BSIT-AUTO', '1', 'BSIT 1B'),
(1593, 'Valdez, Mateo Ave Llurag', '16100340', 'ScUhZ2HRLfWloXK+GjQF08AJo18w1TqUDGamlUc1BNQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00340', 'M', 'BSIT-AUTO', '1', 'BSIT 1A'),
(1594, 'Abud, Emmanuel Nadores', '14101690', 'BP2dnEBXTMH1pP9Fo8pxj6Ef6sVRUNfAoqQEANWx26A=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01690', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1595, 'Agang, Rodel A.', '15100806', 'sU3hfkzsQvMtQe40aViGX4zGgJAmQga67PFTkbGpjfM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00806', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1596, 'Agusto, Edsel S.', '15101606', 'osydAahLdOJq6XOowS4KhIoEWMRiLtgxK5qfsR2wgdA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01606', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1597, 'Alarcon, Bieb A.', '15100670', 'XQMQTnWaVb9BF+PfBV1kRssIr0DNnG297sG4YUuyGLE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00670', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO B'),
(1598, 'Arnosa, Richard B.', '15102473', 'OBSOWmhULSL66aqbGxpleYx3X3JV4Q6H/DJ3BinEcF8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02473', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO B'),
(1599, 'Arpon, Jonnel B.', '15101968', 'MYBE6JdoKBQeLCjy2TukQ1oRnuemO5fh+Aq1ruLhx3o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01968', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1600, 'Ayuste, Juvel A.', '15100166', 'Pq5PJ5J0cKh4N0il2LAhC8StaoOcEWCgcIwblrq1W0I=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00166', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO B'),
(1601, 'Baco, Reymond Dalumpines', '13102534', 'TAxEz3njJQSN5QnETgdCVZDMmT4IbG8Yw4vtVLIDVY0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02534', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO B'),
(1602, 'Barile, Cygie D.', '15101541', 'BZ56SV1y71uMUcLOo5wYD0AbpcehNsm9CwZn8k5D2tk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01541', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1603, 'Batac, Joenard G.', '15102083', 'dqdDC+tcZNz8UOIVB1dGgAIZa6FTSWKkaabVoeCSBsQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02083', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO B'),
(1604, 'Bonifacio, Vincent', '15100614', 'oY7LFLXm09TBTzI8hW5Og/pjyOxxHtjPlyFAxoiNZP8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00614', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO B'),
(1605, 'Borabo, Erwin Sedurante', '15101919', 'TAxEz3njJQSN5QnETgdCVZDMmT4IbG8Yw4vtVLIDVY0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01919', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1606, 'Botastas, Marlon Z.', '15100161', 'LA7NW1WfPeLDhZPCTEAKxlijfMRfdbfjPLCm0WGeuOo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00161', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1607, 'Camacho, Wilque A.', '15102121', 'TAxEz3njJQSN5QnETgdCVZDMmT4IbG8Yw4vtVLIDVY0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02121', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1608, 'Caotivo, Christian M.', '15102184', 'KDLgMfhsMHb6oIHdKXEErnBEqqeNxV6ipmG9tmSiaVc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02184', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1609, 'Castil, Von Maler', '15100657', 'mrDh3W454wOO1eNV1J+ybaon+noOsfJAB2gt07LiMXI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00657', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO'),
(1610, 'Corpin, Kent Roey R', '15101034', 'H6g4khr0BAnvD71FvhP/ylVVVEfSRCBdMWU6WWaj83k=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01034', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1611, 'Cosep, Roy D.', '15100952', 'Nhqi9/BrtRcrT7Nal6O2VOpf7dfTqbdB1c4ApU7Ayy4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00952', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO'),
(1612, 'Ebina, Armon Mondejar', '15102293', 'osydAahLdOJq6XOowS4KhIoEWMRiLtgxK5qfsR2wgdA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02293', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO B'),
(1613, 'Eguia, Aljhon B.', '15100731', 'MplQH8bTvSWr833ub/XOCvTkg0xEFglJEQMtWYXm/zk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00731', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO B'),
(1614, 'Felecia, Jayco Sumaya', '15101432', 'LA7NW1WfPeLDhZPCTEAKxlijfMRfdbfjPLCm0WGeuOo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01432', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1615, 'Francisco, Chester Owen C.', '15100683', '1kDqn808HFYpqB7kmwmIKD8WEeAxrLMEqcFu6iM+wdk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00683', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1616, 'Geraldo, Charles Nathaniel P.', '15100066', 'h6UbXnSCfHrop/vjfO7J0p2JU/KZgkFYpjpdoWbYjm0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00066', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1617, 'Gollena, Leonard S.', '15100277', 'MplQH8bTvSWr833ub/XOCvTkg0xEFglJEQMtWYXm/zk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00277', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO B'),
(1618, 'Gonzal, Emmanuel O.', '15101282', 'jBytqbj4yYzeq6yxq8+Wx5wLVR7z/eWEYDbsHCh7WhQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01282', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1619, 'Lanante, Ronnie Alibio', '12101748', 'Nhqi9/BrtRcrT7Nal6O2VOpf7dfTqbdB1c4ApU7Ayy4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01748', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1620, 'Macorol, Wilfred R.', '15100703', 'ofL2bFuR8ghIFxmpmkbJMjqL8rH+1Mn3jfma0VaDkFU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00703', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO'),
(1621, 'Mahait, Michael P.', '14100838', 'NvWiyqJXBSzHr86iAKyjcO8FeO46Ja0aQSU5qUmCEhI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00838', 'M', 'BSIT-AUTO', '2', 'BSIT 3-AUTO B'),
(1622, 'Mariano, Florante C.', '15101048', 'RhwxHjXB2vttKMQTBAjKLiQyh+Ipx4w9gD/fJrzefS0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01048', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO B'),
(1623, 'Mendoza, Jhon Lloyd G.', '15101542', 't8poZBlVanQhbysje2beTFRdhn9ZrGlnZevTlsY+ZTg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01542', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1624, 'Neduelan, Jaybee M.', '15101785', 'tvUFy1U0PCyJ4bFNiUUDcDv76edQlovIHAFWgNEH10w=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01785', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1625, 'Pedida, Gilbert G.', '15101043', 'E4t89z2GakP9semuGNmIV/l1jmu0KsQMBRMtDE7Urx4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01043', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO B'),
(1626, 'Piamonte, Arnold P.', '15102180', '1eu4sS3+wbBB+n4zHvdQ3xAjgAk72NG216ZO0Jm9Y5g=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02180', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO'),
(1627, 'Ricote, Clide Ian M.', '15100234', 't8poZBlVanQhbysje2beTFRdhn9ZrGlnZevTlsY+ZTg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00234', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1628, 'Rojas, Jaymar L.', '15102390', 'VjmAo/60hUMW0b+Hgi6iWqmTbzfPHfruri7BTt0ZV8g=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02390', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO B'),
(1629, 'Rosario, James Patrick S.', '15100167', 'ScUhZ2HRLfWloXK+GjQF08AJo18w1TqUDGamlUc1BNQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00167', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1630, 'Rosellosa, Dandenver O.', '15100340', 'j7ECR0oTECpu+PgMAF7Jg1ACxUTAVV9863w+JFbHkec=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00340', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1631, 'Rotia, Aristotle S.', '14100644', 'D3Hi5a4pNYCkFVHItXQEp25Uh3dIFswKEqwOqMoMYbY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00644', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO'),
(1632, 'Sacedor, Jaypee S.', '15101070', 'ofL2bFuR8ghIFxmpmkbJMjqL8rH+1Mn3jfma0VaDkFU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01070', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO'),
(1633, 'Sambitan, Jan Paul E.', '15101500', 'uT58q8ebd+hqXO/47mlenofCPDYTxfdqdTPWrisctZE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01500', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1634, 'Sandigan, Joemar A.', '15101639', 'dAc4UmhwsULACVE5G3dl25E1yfJPcDkeUBGXoZV4XdI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01639', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO'),
(1635, 'Tadeja, Kenjie C.', '14100859', 'UygPrJ5QuzRYBbMk+KNmgCZEalltt/2PkLCkTBfQxpc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00859', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTOMOTI'),
(1636, 'Vaporoso, Ronie P.', '15100203', 'X1Fpp9uFM7coDvULu3C3q1TcEUo0XGTeX7JSIz0KFf4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00203', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO B'),
(1637, 'Veliganio, Jester Ladisla', '15100297', 'Is/hS+y643mU26+6BuF7d0eTQfP52PFoX3TaV1g5LCM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00297', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO B'),
(1638, 'Zamora, Lino M.', '14200032', 'RhwxHjXB2vttKMQTBAjKLiQyh+Ipx4w9gD/fJrzefS0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-2-00032', 'M', 'BSIT-AUTO', '2', 'BSIT 2-AUTO B'),
(1639, 'Abellanosa, Melchor C.', '14101643', 'jP/zhGRmWiBzzBlCNpokZlLZUvxfkWHkd5AKp34NTXY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01643', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1640, 'Arnejo, Jasper L.', '14100040', '32vIraX4tB7uAbxUACKtMINclJDa2UA7zqMCX9yhFAg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00040', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1641, 'Balse, Franklin V.', '14100028', 'N+YA1OQxsEjgrA4uBv/rnIqoYPiFAtvNHnICiqfQ76g=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00028', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1642, 'Bohol, Bimbo C.', '14101490', 'amjaCtrkuk6jd756jzcB+OgQs96mdiUvVtzuA1LISRM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01490', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1643, 'Cabuquin, John Michael C.', '14100925', 'iysrodspzoWoEkVslr9WumtHWsF6TOrS7Zq44E0SemY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00925', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1644, 'Caingcoy, Jayboy  Castro', '10102127', 'bXQ+U7ENHYRf2bVoBD5EItVqZn9ug/gBiSGemYXwx3E=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '10-1-02127', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO'),
(1645, 'Cajes, Elmar Jabuen', '12101697', 'IOTY7GejXsIq42demzwam0GgAhH91PkQXbtGhwfz6wE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01697', 'M', 'BSIT-AUTO', '3', 'BSIT 1A'),
(1646, 'Casia, Michael Joshua Inot', '15100243', 'cSxroFbWVBTCLqhV88d4OXspeT8Wgx/hKVFI37QXtIc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00243', 'M', 'BSIT-AUTO', '3', 'BSIT 2-AUTO B'),
(1647, 'Contratista, Suni Villaganas', '14101726', 'VvaSD679HgB1QUqStBWSu/lCSXXiepVLNx1vk2u3D0I=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01726', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1648, 'De Lara, Jayrald Dave D.', '14100211', 'jmGtLDyJiVPKK1qqH6qLBvB8XQxSAE18ESbTJ53B/F4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00211', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO'),
(1649, 'Del Mundo, Ariel', '14101185', 'VvaSD679HgB1QUqStBWSu/lCSXXiepVLNx1vk2u3D0I=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01185', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1650, 'Dogelio, Roger R.', '14102057', 'Md6MtH7hzmCgXCUF4mLqUWq4t2YWvCgnqnIaR6Ywhwc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02057', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1651, 'Felecia, Johnmark M.', '14100616', 'ZIr7Pl562ME/Nd3rBA67OMGe7uz7JmzV1NVEx0CyDdM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00616', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1652, 'Javines, Bryan Jay D.', '14101151', 'W3sUlYHpl9T5kzxf9SUf15cog+blVyRwe4bqLHoyTLk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01151', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO'),
(1653, 'Lequiron, Eve Janmar Culibra', '14102743', 'iHdwMfaayBfXYy0qYrLXa1Hf5mg+5h5kjb+n9WZjrqI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02743', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1654, 'Lubo, Jason H.', '14100874', 'WX13crbJU1sW17vTlN9xrX2/X4VKeVtekKS4rwdwzU8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00874', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO'),
(1655, 'Nacional, Paul Chan R.', '14100649', 'Fh+EXWGcrXoOv/+9pd+YlE0LSlO3jgz6ikfWqJdEpGg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00649', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1656, 'Paghubasan, Melvin E.', '14100216', 'aMF844pZUBIwDuSpSIGkg7e5xDr+ncJ1EF/MRga/AoM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00216', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1657, 'Pante, Jermyn Sulteras', '13102078', 'o5m2tzIdIENOBnqpWamsC2jD41V8Hmf7/Zj8HaBnCJo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02078', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO'),
(1658, 'Paragatos, Orlando Jr. R.', '14101541', '1jcwSHEZpDXF5jLFVV38GiT4zFVCenORofo+67bgsoU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01541', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO'),
(1659, 'Parilla, Gilbert D.', '14101525', 'gWfgPsvwoEJjFuB3tgaMgepPS4BIkzc2GOMIjsRvczg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01525', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO'),
(1660, 'Pedida, Dexter Mendez', '14101863', 'KIccdGE6yvc/n7OFaVfJcDc3MTrIgaeydDmZ6+3/wJc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01863', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1661, 'Peñeda, Bryan Leman Abilar', '08100310', 'Y8rZEXtY7RBmNUJjxv445ldL7V1ABGTmU96CVPJ7oaU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '08-1-00310', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1662, 'Requiso, Nilnil O.', '14101560', '/uAaEa0D6z5k6rmLVCefVmyNwQTQ7kq1JiPJkH2h/yg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01560', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1663, 'Saberon, Jeffersol B.', '14101110', 'z4uLR0pigQwABSKHG1Ojqs3EF5D47W0R0aJV6toFtCk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01110', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1664, 'Tabocolde, James Carl Docallos', '14101675', 'BFioKV0bJrOR7Jbr2honW2wkmtEtPA7CSfAcOjoadD4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01675', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1665, 'Tangog, Adonis H.', '14101558', 'iHdwMfaayBfXYy0qYrLXa1Hf5mg+5h5kjb+n9WZjrqI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01558', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO B'),
(1666, 'Tobiano, Gerald Marañan', '12101187', 'WetNl/uHD7MtKEDuWs74v3MWm5tk+/6OlWx9v9ogtL4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01187', 'M', 'BSIT-AUTO', '3', 'BSIT 3-AUTO'),
(1667, 'Amistoso, Rey Peañar', '13100556', 'oNSfpgkXciLik9N5mzmhE778x9UJhP3Soq0Aa3vcGO4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00556', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1668, 'Batucan, Virgilio P', '12101698', 'dSQPFksbUkHUOH01s95txLvR+LwbSpaD4xmCsWdy5oI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01698', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1669, 'Bello, Jamel Belza', '13100763', 'yb8aTqqDG/SMXou06gUpxrsdfLWsFA5sHb6e6xQ42ak=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00763', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1670, 'Bernal, Judesser Servano', '13102475', '0cbmhxM7FqVk6R3/udlTIfacapWucbVe9U1MBZyWAIQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02475', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1671, 'Caliao, Jeric Demate', '13101588', 'aHiqcbAD/nn8U49VpXBBsfpdxNvu3oDxB6Gc6ahroOE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01588', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1672, 'Canencia, Danold Elatico', '13100375', '6x1cYHDNxgS4DYSksCdLMYFS8Y004kmV9TsVKpYP2rI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00375', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1673, 'Decinilla, Mharc Joseph Boca', '12100467', 'o5m2tzIdIENOBnqpWamsC2jD41V8Hmf7/Zj8HaBnCJo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00467', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1674, 'Docallos, Romel M', '12100430', 'a6VV8v/m7u01iiARAkq5CwJLxVndy2Sn3lJgPOq06m4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00430', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1675, 'Lachica, Marlo Avisado', '13100708', 'ssVp9p+GKAnJF8PTdvSzaN342v8c3QjJuy7M1WE0w5Y=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00708', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1676, 'Ladica, Victor Laure', '13100751', 'Gw8+QRJoLmqPRcXZafae0GKX/BdO9pKS54cog/bFlow=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00751', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1677, 'Lamoste, Arvin Basilad', '13101043', '7WPPBmKdNrelMm9w7d7rjxuBJkYDYqMp0kQakszer68=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01043', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1678, 'Liloan, Nicleto, Jr  Catoltol', '12101394', 'RigEFu+l4XM+1BidCc263aQ5w+1u37rCVD8beSgJWv0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01394', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1679, 'Makabenta, Gino Cuesta', '13101366', 'C6JulDcbW30sfIQMkTLwmJtEEGkCTwT4O7Gg30Cq5lg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01366', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1680, 'Malinao Joy Marito', '11101997', 'XZWw+cjgChHAyKF3tv20s0QyMm1d0Ffbo0A3VKWuUsE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-01997', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1681, 'Matuguinas, Randy Sambitan', '13101988', 'BwaFu490hyhcpDIwEuLtHiAjIYdf+c92fggSIK78zYs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01988', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1682, 'Parena, Lorence Jurilla', '13100034', 'Gb9rnAVC7h1IPsg7uTIE27VmffB5/mq2GsVX8KT5lqY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00034', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1683, 'Payos, Nisan Sampilo', '13102319', 'Do7Jc5lIgjEfckTFgoyXgba9gdHytdLOnYbJkCHV/l4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02319', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1684, 'Rostata, Jayson Mendoza', '13101040', 'XZWw+cjgChHAyKF3tv20s0QyMm1d0Ffbo0A3VKWuUsE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01040', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1685, 'Sambitan, Benjie Largo', '13100944', 'xmjvGRL0dOqfRnvwSnLWevsOHlWj/iEo0VkD9VfjEG0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00944', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1686, 'Sarsalejo, Anthony Saligumba', '12101212', 'ixYSnDRvDl362cpzaevPdZMUOYMPCPs1MqeEsQm2t0s=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01212', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1687, 'Socuano, Melvin Jones  Ab', '09101321', '8j/dFxg47FTp1hUgKkuKsp1bZ4orxjAU0NeFoisJ3U8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '09-1-01321', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1688, 'Solis, Dexter Arintoc', '13100951', 'fkoqw+3vnSbVIM1cq4J/jew7Fatj7bw4fJiUWcaMdx0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00951', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1689, 'Suyo, Giovanni', '11100903', '5QNCKYK51qlybQMSn19GyaVsUAQ7FNOtBwOxMKFRNiw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-00903', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1690, 'Tacmoy, Junie Vic P.', '14200124', 'Yxscn/kqzLK7fwP9/0JJ+MF4lbT9AVjkH85kMKRwoS4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-2-00124', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1691, 'Ybañez, Roger Rosal', '13102423', 'pZDfxslfkN9bWKsGMlT2cdL0SmkJBFL9FrbbHEZs3Dk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02423', 'M', 'BSIT-AUTO', '4', 'BSIT 4-AUTO'),
(1692, 'Cotejar, Christine Joy C.', '15102100', 'toauHsIycpkcm7eG0OW8g9b/b7/P7Wowr4cNdcQhblw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02100', 'F', 'BSIT-DRAFTIN', '1', 'BSIT 1C'),
(1693, 'Quilisadio, John Ryan M.', '15102182', 'ZVD/ezVaHo2GEFj5cwzbNrNOdw29wlQdskeP7g5zI0k=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02182', 'M', 'BSIT-DRAFTIN', '1', 'BSIT 2-AUTOMOTI'),
(1694, 'Sacarre, Lesly Sarsusa', '16100419', '/7iFSu2TNLBCQT02Hd7Q2HExjAqNC5yCQmK59DxbPzg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00419', 'F', 'BSIT-DRAFTIN', '1', 'BSIT 1A'),
(1695, 'Alimorong, Denmark Siat', '12200017', 'FEAvNmF6ZSSqcol8H8losoo1gIP16DPTan8wZz+G+O4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-2-00017', 'M', 'BSIT-DRAFTIN', '2', 'BSIT 2-DRAFTNG'),
(1696, 'Caayon, Riza R.', '15101610', 'F3QGM1iK1qxBRnMWJvP4uhQDo8l6yI44RJqtxONkp3A=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01610', 'F', 'BSIT-DRAFTIN', '2', 'BSIT 2-DRAFTING'),
(1697, 'Caingcoy, Joenyl Fernand Castro', '13100289', 'dCHXg7IOTR+xY8kMEgirDcqIat66ya8tqe8Xp2Orax8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00289', 'M', 'BSIT-DRAFTIN', '2', 'BSIT 2-DRAFTNG'),
(1698, 'Lovitos, Gregorio Jr. B.', '15100219', '1UmCWeF0kLVwfJkf/tpv2jsTY7o5GvO5P9qoRKn5zuM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00219', 'M', 'BSIT-DRAFTIN', '2', 'BSIT 2-DRAFTNG'),
(1699, 'Villanueva, Arjay  E.', '03100347', '8j/dFxg47FTp1hUgKkuKsp1bZ4orxjAU0NeFoisJ3U8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '03-1-00347', 'M', 'BSIT-DRAFTIN', '2', 'BSIT 2-AUTO B'),
(1700, 'Berian, Bernardo', '14101659', 'QIAjSyzRpKetjd7ezx9YE2cAjJpCkpVXJpUdaxAz8eI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01659', 'M', 'BSIT-DRAFTIN', '3', 'BSIT 3-DRAFTING'),
(1701, 'Capellan, Joshua G.', '14101545', 'HLXag/12r4r22qVasVCRY+2LD9lwTBZGd3/8D23xFoU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01545', 'M', 'BSIT-DRAFTIN', '3', 'BSIT 3-AUTO'),
(1702, 'Cordeta, Erwin Jr. E.', '14100220', 'IwHFvHTi9L1skGk8o6Evzp//Fh4xUha5CG0JFIgkzgE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00220', 'M', 'BSIT-DRAFTIN', '3', 'BSIT 3-DRAFTING'),
(1703, 'Gonzaga, Dandy Arcilla', '14101930', '+YrOOOAY8XCVzlgc9vtntZwVS83r9CfwmcLJabEqUdY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01930', 'M', 'BSIT-DRAFTIN', '3', 'BSIT 3-DRAFTING'),
(1704, 'Sumayang, Jove M.', '14100881', '46eJSLHnUjmoaj2ZrJfA0PWFynMVQR8crU3150UNvzc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00881', 'M', 'BSIT-DRAFTIN', '3', 'BSIT 3-DRAFTING'),
(1705, 'Balondo, Christina Esguerra', '13100863', 'zAyUw+c2BTwmiwsQiaLURJ2vfWsgfHD02FoYB2T66JQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00863', 'F', 'BSIT-DRAFTIN', '4', 'BSIT 4-DRAFTING'),
(1706, 'Ejorcadas, Rosemarie Lambit', '08101024', '7u8Lp7ydNeptatLpvXvBmSqMO5wERNEZxX6nJXigOzw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '08-1-01024', 'F', 'BSIT-DRAFTIN', '4', 'BSIT 4-DRAFTING'),
(1707, 'Garing, Randolph Entrolizo', '09100572', 'n9DZph2Ip51y5dbE9XWxN8U1WVCnzx80quXfczo/UvE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '09-1-00572', 'M', 'BSIT-DRAFTIN', '4', 'BSIT 4-DRAFTING'),
(1708, 'Marcos, Rea Akina Ochavo', '13100437', 'DPuvW4+L7XZIc7fVNDPqezi1crg5g7GtqRQpwX6hEQA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00437', 'F', 'BSIT-DRAFTIN', '4', 'BSIT 4-DRAFTING'),
(1709, 'Angcahan, John B.', '14101479', 'xmjvGRL0dOqfRnvwSnLWevsOHlWj/iEo0VkD9VfjEG0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01479', 'M', 'BSIT-ELECTRI', '1', 'BSIT 3-AUTO B'),
(1710, 'Anima, Hadjie', '16100007', '0H+26uthKSDuPxgkUqGd+/GTJz/4hnmewBaOCq0YTx4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00007', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1A'),
(1711, 'ANSALE, JHUNE ALBERT MANGLE', '16100612', 'm8p5SFSD44bhTj5Ju0McNIrs+DpuR7yYQInHOlPRFEw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00612', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1B'),
(1712, 'Balidbid, Jordan Baja', '16100412', 'LHMwhIIqeNkvlS2IQboHRA6Gf6z7bzp63GBT95rrY7c=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00412', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1A'),
(1713, 'Balina, Jesus P.', '15100626', 'Uw2SX8WW2MlteTwY3QZ/421K7IZiVyTEcMCKpptXJ8o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00626', 'M', 'BSIT-ELECTRI', '1', 'BSIT 2-AUTOMOTI'),
(1714, 'BELACSI, BENSON JOHN DELA TORRE', '16100614', 'ixYSnDRvDl362cpzaevPdZMUOYMPCPs1MqeEsQm2t0s=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00614', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1B'),
(1715, 'Bergamo, Archie Dionisio', '14101624', 'gWfgPsvwoEJjFuB3tgaMgepPS4BIkzc2GOMIjsRvczg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01624', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1C'),
(1716, 'Besoyo, Jhon Waynne  Sale', '12101920', 'atD7Vx5KDH/7QN5gHYxDYz9UGU0ueff+cbfsGybcQdk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01920', 'M', 'BSIT-ELECTRI', '1', 'BSIT 3-AUTO'),
(1717, 'CABALQUINTO, RICHARD P.', '16100383', 'bwdIPmgSuiFclsfC4ZE+p5NjMi+82ivg9m3ataKDyos=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00383', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1C'),
(1718, 'Cabañas, Karl Ponsoy', '12100580', 'ixYSnDRvDl362cpzaevPdZMUOYMPCPs1MqeEsQm2t0s=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00580', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1B'),
(1719, 'Cabantoc, Mhel Jake De Paz', '16100408', 'Fh+EXWGcrXoOv/+9pd+YlE0LSlO3jgz6ikfWqJdEpGg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00408', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1A'),
(1720, 'Castañeda, Julius  Atok', '08101667', 'W3sUlYHpl9T5kzxf9SUf15cog+blVyRwe4bqLHoyTLk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '08-1-01667', 'M', 'BSIT-ELECTRI', '1', 'BSIT 3-ELECTRIC'),
(1721, 'Dela Peña Junry Mahinay', '14100477', '5QNCKYK51qlybQMSn19GyaVsUAQ7FNOtBwOxMKFRNiw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00477', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1A ELECTRO'),
(1722, 'Fulgencio, Darel Jamot', '16100555', 'y7+4LGfCOOpImiNgWQaaQZTtKNPvdieT4qXR5zDaMlA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00555', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1B'),
(1723, 'Gayo, Archie Del Sol', '14100002', 'C6JulDcbW30sfIQMkTLwmJtEEGkCTwT4O7Gg30Cq5lg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00002', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1B'),
(1724, 'Gayrama, Ervin Remitera', '11101279', 'fkoqw+3vnSbVIM1cq4J/jew7Fatj7bw4fJiUWcaMdx0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-01279', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1C'),
(1725, 'Lambit, Wilson Jr. Dagohoy', '16100361', 'pyWF2Cgthz0qRjl0rIo/JaBlHIL2qmE0whwOVfQ7xHc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00361', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1A'),
(1726, 'Lamoste, Elmour Sabas', '16100411', 'KwoQUxoCcBSjhdWbfIP6JYiZXmxUnjiuTKU/4WfxbPE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00411', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1A'),
(1727, 'Machete, Ariel M.', '15100272', 'IGWoD9Qm9KCz/44dUK1L9NbvxCZvMovgCs2sqx3SNf4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00272', 'M', 'BSIT-ELECTRI', '1', 'BSIT 2-ELECTRIC'),
(1728, 'MANUEL, JOHN MICHAEL M.', '16100642', 'xirRJbYQfhf/GvTSDDT8RD2JLEmX2rKZLWJHoTHDT7Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00642', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1B'),
(1729, 'Mapanao, Leomark Bermoy', '16100540', 'fC2mvJvsG9zrQt1zREozu3fXxZ2+RLL6fjEDO/3Bh34=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00540', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1B'),
(1730, 'Maulit, Jhoban Pag-ong', '12100582', 'ytPTaypao8M1pfovU6y2jicUHuZtLhKGxtD7t3HqD98=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00582', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1B'),
(1731, 'Mercolita Raymund Juntilla', '13100496', 'ZIr7Pl562ME/Nd3rBA67OMGe7uz7JmzV1NVEx0CyDdM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00496', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1B'),
(1732, 'MIROY, NIÑO JEFF BAXIMEN', '16100688', 'IZ6ucTo0vvh0g+tzrXZV62hVnEDXdSV62+o7xKpZ/+U=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00688', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1B'),
(1733, 'Nudalo, Jeferson Garcia', '15200042', '5QNCKYK51qlybQMSn19GyaVsUAQ7FNOtBwOxMKFRNiw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00042', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1B'),
(1734, 'ORAL, JOSHUA E.', '16100807', '60Ga8892E1/2DV27spFZQ3Cv8IMTp/54I+eC21YMJSk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00807', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1C'),
(1735, 'Palconit, Zenaido, Jr  Juliar', '12100904', 'O7H0OZfqLtSUHMr+kY10scyh2RGWBW44ahIAv9XRgEU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00904', 'M', 'BSIT-ELECTRI', '1', 'BSIT 3-ELECTRIC'),
(1736, 'Perez, Leonardo III', '16100382', 'wQYV3XEhRfFYMWFUbWIPm3rnCY791TuxeHFBE8pc7V4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00382', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1A'),
(1737, 'Regala, Lim Noel Baco', '13102363', 'o5m2tzIdIENOBnqpWamsC2jD41V8Hmf7/Zj8HaBnCJo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02363', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1C'),
(1738, 'ROBARO, JAYSON J.', '16100643', '5ciSkA1iUbjphXcDjrfGxXNib6oTmZ5dLGeMognA28c=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00643', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1A ELECTRI'),
(1739, 'Rosel, Lester Ian C.', '13102414', '9R+hafNNjRVcgGdzw57ZitASECZ0cODyA/mAW946ITQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02414', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1C'),
(1740, 'Saba, Arson Rey S.', '07100169', 'j38TR0+cQDDha/r5VG1AF9oede+bPehR8kz6E8TpWMk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '07-1-00169', 'M', 'BSIT-ELECTRI', '1', 'BSIT 3-AUTO'),
(1741, 'Sangcap, Jonelle Palomar', '16100259', 'kaxEhWdGIwa6k5FvvOa44aPoyKOGq3eZ0T53da0q3eA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00259', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1A'),
(1742, 'Sangcap, Windel Omega', '16100344', 'nIp75JFDTMwoV+vQQPUWheNMCB2TrOGVAK47WHiiOzE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00344', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1A'),
(1743, 'Sanico, Jessie Estrada', '16100290', 'jEOScadEn0tVLapAkAXqNuuk6J/3MCPERcmXuHryPZA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00290', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1A'),
(1744, 'Sarsalejo Jr., Noel S.', '15101056', '4O1Gh2UvJmLDxIEUkRvyZBlzFmz7Px1P7NyreXFdBLI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01056', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1B'),
(1745, 'Ulan Ulan, Aimer Clabis', '15200020', '7WPPBmKdNrelMm9w7d7rjxuBJkYDYqMp0kQakszer68=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00020', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1B'),
(1746, 'Verba, Ronnie S.', '15100862', 'l0Q6E8AZaVp9oiROZn7JOQ8bg5eaMC53UZ3+gRkgLkc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00862', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1C'),
(1747, 'Villaflores, Jairo Casas', '16100321', 'tT3j3rXXRiIwxHmmNN5msUJCoMglpsm/GGGZbGzXqrU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00321', 'M', 'BSIT-ELECTRI', '1', 'BSIT 1A'),
(1748, 'Acero, Michael C.', '15100318', 'F3QGM1iK1qxBRnMWJvP4uhQDo8l6yI44RJqtxONkp3A=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00318', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1749, 'Aljo, Marvin', '15102235', 'dCHXg7IOTR+xY8kMEgirDcqIat66ya8tqe8Xp2Orax8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02235', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1750, 'Arquisola, Julito M.', '15101065', '1X/7TqnO5HU1fUK7LdQlRsPMKNRVnZ0q0MlrWkFscT0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01065', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1751, 'Ayles, Solomon Jr., R.', '15100630', 'gWfgPsvwoEJjFuB3tgaMgepPS4BIkzc2GOMIjsRvczg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00630', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1752, 'Baring, Elmer C.', '15100732', '46eJSLHnUjmoaj2ZrJfA0PWFynMVQR8crU3150UNvzc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00732', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1753, 'Basagan, John Phillip C.', '15102001', 'JJz6dPCNPjg6rqd2I539by0+FjrAh7F8LEsRKWq62Mo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02001', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1754, 'Bernil, Hernsthon Cadlawn', '13102054', 'DbxW7mVt77pVFvW1a9nKFJ+o2tz78QivUSanRtC2RKI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02054', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1755, 'Besbal, Jereme A.', '15101233', '1MAE0H5mRA+veHpd9tsHDZIWX9sca74p09wbPlAMYw8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01233', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1756, 'Borromeo, Edson J.', '15100720', 'fkoqw+3vnSbVIM1cq4J/jew7Fatj7bw4fJiUWcaMdx0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00720', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1757, 'Bulasa, John S.', '15100194', 'zAyUw+c2BTwmiwsQiaLURJ2vfWsgfHD02FoYB2T66JQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00194', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1758, 'Cabuquin, Ryvan G.', '15100255', '/rhh0IWTL3cIqLpPYHd0gAdxfCl5W5R1TkTQD6ItloI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00255', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1759, 'Catandihan, Dave B.', '15101264', 'oNSfpgkXciLik9N5mzmhE778x9UJhP3Soq0Aa3vcGO4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01264', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1760, 'Corto, Aljun Batuigas', '12100074', 'a821UrPqGdgLMwjDHAqloZwZ9uvzDfW4t9UvMCCXe9s=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00074', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1761, 'Corto, Mark Lerie G.', '15100120', 'X4deyOF3AsB2i+uZqBLVSwAXpxBa/Xb/zOeoM0Cvjkg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00120', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1762, 'Corto, Mark Niel Collarga', '12101150', 'ixYSnDRvDl362cpzaevPdZMUOYMPCPs1MqeEsQm2t0s=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01150', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1763, 'Dagalea, Jorge P.', '14200033', 'KUoJpUXzrq1ozBp4IslYEoNQfVBTPGanBPI5NcEBDhU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-2-00033', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1764, 'Delfin, Joramil E.', '15100212', 'DymWd9yEl8TeS5Q0dvcQneT+rLrEQYUfoMw300eJuvU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00212', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1765, 'Ebina, John Lerry M.', '15100809', 'rc6ryL4ugXIvQzOM1ujZ3JUxlTc89LUu0Plyk967iJU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00809', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1766, 'Ilogon, Lloyd S.', '14101097', 'f58HQ6vXD9UDPkAkhl2hAym54SAeY5pJnsys3ZEIgl0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01097', 'M', 'BSIT-ELECTRI', '2', 'BSIT 3-AUTO B'),
(1767, 'Impas, Meljohn A.', '15100170', 'oNSfpgkXciLik9N5mzmhE778x9UJhP3Soq0Aa3vcGO4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00170', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1768, 'Macanas, Edgar Villaganas', '12100550', 'knXPcp8G7mqJ+B0Wz+Jl3VpEWQ0wQ1VHWKLIk9JbF6E=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00550', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1769, 'Machete, Daniel B.', '15100273', 'Lv6knAKowebWjnNfP33bF264g+RyPyf4VsnGAXMU2Q8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00273', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1770, 'Matalcero, Joven H.', '15100659', 'JoLbXuOtSFKMGOgTTUeMro+iHDESHiiByDK9fpgXh18=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00659', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1771, 'Montemor, John Paul B.', '15100434', 'ssVp9p+GKAnJF8PTdvSzaN342v8c3QjJuy7M1WE0w5Y=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00434', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1772, 'Penedilla, Jude A.', '15100747', '/3MDvyoGhNBgpwx2BNI8G8XULXcNJ346l5SF0dpqMW0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00747', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1773, 'Piamonte, Ariel D.', '15100168', 'X4deyOF3AsB2i+uZqBLVSwAXpxBa/Xb/zOeoM0Cvjkg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00168', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1774, 'Ramos, Karl Edison L.', '15102148', '1jcwSHEZpDXF5jLFVV38GiT4zFVCenORofo+67bgsoU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02148', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1775, 'Sabenorio, Ruel S.', '15100125', 'CU7rZoV9LDQ0fpxOPpeEuuuTFbVUivaIYtMrYYI0+Q8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00125', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1776, 'Sabuag, Raymon S.', '15100280', 'a821UrPqGdgLMwjDHAqloZwZ9uvzDfW4t9UvMCCXe9s=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00280', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1777, 'Sediro, Eugene V.', '15100189', 'zAyUw+c2BTwmiwsQiaLURJ2vfWsgfHD02FoYB2T66JQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00189', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1778, 'Songalia, Philson G.', '15101047', 'qdEnrPmU+F7qs42uz5Ofmlsp5Urzc/pnaGeeFw4wDO4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01047', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1779, 'Torres, Jefferson A.', '15100830', 'KrAp0Hqx0f5Pq6siFiju5JkXTNAaXpeQpl3j/p5cr0w=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00830', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1780, 'Tumangan, Nimrey P.', '15100715', 'r4rYzERF6rrF+qPTdiQ/exNLgCrXEeC4VxJjGhr7EEE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00715', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1781, 'Verian, Rigy Boy N.', '15102129', '08gtIeduTEelkokiAKalfSvLnvllfe+cBu3lYYCA0CU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02129', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-AUTOMOTI'),
(1782, 'Verunque, Jhon Paul C.', '15102327', 'f58HQ6vXD9UDPkAkhl2hAym54SAeY5pJnsys3ZEIgl0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02327', 'M', 'BSIT-ELECTRI', '2', 'BSIT 2-ELECTRIC'),
(1783, 'Acuin, Mel John A.', '14100414', 'n9DZph2Ip51y5dbE9XWxN8U1WVCnzx80quXfczo/UvE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00414', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1784, 'Badanoy, Jimmel E.', '14100422', 'RcWcT/cZPmwQ42lwwvR/CWvWZIQilfQa5CLHe1n8Zhw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00422', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1785, 'Cabug, Jayson Salazar', '13101992', 'fC2mvJvsG9zrQt1zREozu3fXxZ2+RLL6fjEDO/3Bh34=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01992', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-AUTO'),
(1786, 'Cañete, Francis  E.', '12102370', 'CS3s/iFnLAMqajQrrPWv+hJbTt8vfLka+xjA/vfF+8U=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-02370', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1787, 'Cervantes, Von Roger  Fie', '06100772', 'FSfxJq9NrHC15nMncxs6rm4O8G/hlMvnPxEYmwwBgy0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '06-1-00772', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1788, 'Corpin, Ace Donald Pedrigal', '12100436', 'QIAjSyzRpKetjd7ezx9YE2cAjJpCkpVXJpUdaxAz8eI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00436', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1789, 'Datuin, Rogelio Jr. Bacalla', '14102600', 'fC2mvJvsG9zrQt1zREozu3fXxZ2+RLL6fjEDO/3Bh34=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02600', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1790, 'Decinilla, Japh Christian Boca', '14101966', 'APjqg97srqzfk6oa32cnmYTRYVlIkLu0UWSMi/pl6Qg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01966', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1791, 'Ebajo, Mark Joseph Ramento', '14101956', 'Vn4gzXHFOjeQ0xq3tO/3f4M5T0HLgME65HKgy8iqobc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01956', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1792, 'Estrada, Ricardo, Jr  Turado', '11101090', 'CS3s/iFnLAMqajQrrPWv+hJbTt8vfLka+xjA/vfF+8U=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-01090', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-AUTO'),
(1793, 'Gadugdug, Jhun B.', '14100443', 'W/DaqPYIxlEA3dkm7q73yV6ZZI3+0gKDCQJFA0LflVM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00443', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1794, 'Jamot. Kenjie P.', '12100533', 'ytPTaypao8M1pfovU6y2jicUHuZtLhKGxtD7t3HqD98=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00533', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1795, 'Lim, Eric, Jr.  Abrico', '08100389', 'K+R37XmrU6bDEiqKDnyyyS2RtC0uachKVcys55yj73A=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '08-1-00389', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-AUTO B'),
(1796, 'Monton, Riyan Batiquin', '13200010', 'W3sUlYHpl9T5kzxf9SUf15cog+blVyRwe4bqLHoyTLk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-2-00010', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-AUTO B'),
(1797, 'Ortiz, John Richter P.', '14101129', 'v41aOv0fi6/CgPKQJ6GhGElrmr6Rr00w8gjGpz5STto=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01129', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1798, 'Quibete, Joe Nash Banua', '13102033', 'Yr5CcJKA3jm0JOotVXPxgeeeLuA3ciuG/P8dIY5yuqA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02033', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-AUTO B'),
(1799, 'Ramilla, Christian Sari', '13100689', '05KU5rKOjpnJL7ASxn4+t0snOZmx1fM7GmKY49aMYEM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00689', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1800, 'Sabenorio, Librando Chua', '12100359', 'N3GKobgvfxkVvbjXX+MZy2+jh4ctfTmT1c+fiFT1Umo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00359', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1801, 'Sale, Geimar M.', '14100849', 'LQXSCZIgJaUpDWkuoldSKNCkjidN6tfA7gC2lO5/q8o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00849', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1802, 'Sumaya, Rey P.', '14100852', 'u+T9k++iLL/8eySCixh+zx3DjmARxYj1yfeTUm9xBWw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00852', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1803, 'Tobiano, Jayson M', '12200106', 'G/yKBmBwjPxrPW5CLwb8aFPQaCpOQsDeEwYHRimy4ns=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-2-00106', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1804, 'Vaporoso, Jake S.', '14101182', 't2O3E9AFCWyY+Cuk2nA5tuvDoV5LvoQi9/J6jQPxkeo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01182', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1805, 'Veruen, Mark', '12102375', 'Vm8gydkXfR90D/HQRaOcHq0EHMwbrMrFmIY4yTGH1Ec=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-02375', 'M', 'BSIT-ELECTRI', '3', 'BSIT 3-ELECTRIC'),
(1806, 'Agner, Raul Esquerdo', '13101726', 'XB2RJfo+rfK5s3TFKJPK4FZvx9M83CmIZMgCrtS4+0Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01726', 'M', 'BSIT-ELECTRI', '4', 'BSIT 4-ELECTRIC'),
(1807, 'Almaden, Vincent Raganas', '12101409', '8hssIvMhsRVwZR+yhhPpYYVdCdGZLjmRSEepUYZLTDY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01409', 'M', 'BSIT-ELECTRI', '4', 'BSIT 4-ELECTRIC'),
(1808, 'Balan, Rosalito Gemaol', '13100870', 'erjUiDvgPEYu+1Gvo6WWC/vtJVTIkS5VoDSOCmgJXnA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00870', 'M', 'BSIT-ELECTRI', '4', 'BSIT 4-ELECTRIC'),
(1809, 'Ceno, Argei Aquino', '11102060', 'cZaLfgWrsNpaGnk5nK9VDBdXeVVBMeK1Q10ATDpC2Xs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-02060', 'M', 'BSIT-ELECTRI', '4', 'BSIT 4-ELECTRIC'),
(1810, 'Cordeta, Angelito  Espreg', '11101922', 'gB4Va36NHMeYaHJa6Lub1dqPe6+XA0DQ/V/0RGeVOQ4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-01922', 'M', 'BSIT-ELECTRI', '4', 'BSIT 4-ELECTRIC'),
(1811, 'Cuyos, Diecher Villaren', '10100754', 'j3/Co+8peyi44vfD3cZGVHbl4XCWIJuYUs8QoHXv/t0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '10-1-00754', 'M', 'BSIT-ELECTRI', '4', 'BSIT 4-ELECTRIC'),
(1812, 'Emnas, Mark Cantre', '13101436', 'groDY0qe3sqoMR3H+8kufWRqmB1iYAJewuyhZ+TpFhk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01436', 'M', 'BSIT-ELECTRI', '4', 'BSIT 4-ELECTRIC'),
(1813, 'Esperanza, antony Briol', '13101439', '52Y5hlPyzRDkvLxO0z/P/jg7Nmqpdt4HvgS8REGDXgQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01439', 'M', 'BSIT-ELECTRI', '4', 'BSIT 4-ELECTRIC'),
(1814, 'Gelig, Marlon Pujeda', '13101780', 'W3kqQ7Z+dRXfx3M4GkO7Za5aqiemBR84B3JSc3heH38=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01780', 'M', 'BSIT-ELECTRI', '4', 'BSIT 4-ELECTRIC'),
(1815, 'Lubiano, Horrace Dane Demeterio', '13101598', '8hssIvMhsRVwZR+yhhPpYYVdCdGZLjmRSEepUYZLTDY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01598', 'M', 'BSIT-ELECTRI', '4', 'BSIT 4-ELECTRIC'),
(1816, 'Musa, Junmar S.', '11102089', 'HMfffBCZ4jESRaKMWrehGebirhZUQWBwbC4ovBwomLE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-02089', 'M', 'BSIT-ELECTRI', '4', 'BSIT 4-ELECTRIC'),
(1817, 'Osmeña, Carlos Jr. Corpin', '13102418', 'cZaLfgWrsNpaGnk5nK9VDBdXeVVBMeK1Q10ATDpC2Xs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02418', 'M', 'BSIT-ELECTRI', '4', 'BSIT 4-ELECTRIC'),
(1818, 'Sangcap, Marlon Sandigan', '13101812', 'PFRyNtMqBfKByNdGVRn5o1kByAdYedExX09Td5rXfds=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01812', 'M', 'BSIT-ELECTRI', '4', 'BSIT 4-ELECTRIC'),
(1819, 'Abad, Jay-an', '16100010', 'oreHP0dqETjHO5ssGPo/FA0u3b6gNc12F2iZdZJNtw8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00010', 'F', 'BSIT-ELECTRO', '1', 'BSIT 1A');
INSERT INTO `tbl_students` (`id`, `name`, `username`, `password`, `email`, `contact_number`, `birthdate`, `type`, `detail`, `active`, `user_id`, `date_added`, `date_modified`, `CODE`, `SEX`, `CRSCD`, `YEAR`, `SECCD`) VALUES
(1820, 'Atok, Noreen Joy G', '16100413', 'GM3dRqyb5pd9A5rMgicuP68POj+/2KSpOqpeptLWk+4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00413', 'F', 'BSIT-ELECTRO', '1', 'BSIT 1A'),
(1821, 'Bergamo, Mark Dionesio', '09101523', '9ysLopW2PLfEaerTitbMv5zi2z6aBrGSpSyhzbmVC6o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '09-1-01523', 'M', 'BSIT-ELECTRO', '1', 'BSIT 3-AUTO'),
(1822, 'Casas, Garry Alterado', '16100269', 'pUJu2ecD4OJmRfZNBEmZLN9ycod6lg2Ek4hYQhi/fO4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00269', 'M', 'BSIT-ELECTRO', '1', 'BSIT 1A'),
(1823, 'De Lara, Justine Jay Saludar', '16100552', 'O675mbag7W8GDNt6b+VAaNe3A4ABtYd7m/k6U4/PUjE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00552', 'M', 'BSIT-ELECTRO', '1', 'BSIT 1B'),
(1824, 'Ebron, Brian Baco', '14102048', 'Qo8EAY4eOGHxl7v/CYJ5vMvGUYo1/h26o2CCMhGS0v0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02048', 'M', 'BSIT-ELECTRO', '1', 'BSIT 1B'),
(1825, 'Eraño, Jony Demate', '16100438', 'OM2NdAdf1097FAxmEnDCdHpZ+Jy/Ixjf0k4mekR8Ad4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00438', 'M', 'BSIT-ELECTRO', '1', 'BSIT 1A'),
(1826, 'Lumaad, Kim G.', '16100653', 'zGikEeop/AeioiSWv6EFuPevBYhiUqFoEA/7mXqDI0Y=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00653', 'M', 'BSIT-ELECTRO', '1', 'BSIT 1C'),
(1827, 'Mamacos, Christian R.', '15101007', 'gwLKeoetWrGra/G0x++eOdU/CCmPEOnka4QPHwhV+/Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01007', 'M', 'BSIT-ELECTRO', '1', 'BSIT 2-AUTOMOTI'),
(1828, 'Noval, Jengkie', '16100011', '8Ey4ebJAzCet+IvNoA83vSukzHWKEd3BvY4brTwcSHk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00011', 'F', 'BSIT-ELECTRO', '1', 'BSIT 1A'),
(1829, 'OMAS, ORVILLE M.', '16100676', 'w1QnuEDv5G40kxai2gXlisTWVKXO/lHq9UHFCy1ZTMg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00676', 'M', 'BSIT-ELECTRO', '1', 'BSIT 1B'),
(1830, 'Rosillon, Noel M.', '15100065', '2eDfItsR9S+gBlxwy3kRJHHHEe7PkUJD7LtPJNSWY8Y=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00065', 'M', 'BSIT-ELECTRO', '1', 'BSIT 1A'),
(1831, 'Samante, David Verana', '16100549', 'gwLKeoetWrGra/G0x++eOdU/CCmPEOnka4QPHwhV+/Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00549', 'M', 'BSIT-ELECTRO', '1', 'BSIT 1B'),
(1832, 'Sarabia, Dexter Sanchez', '13101696', 'FBLG8jjPzDbxUdIJLXUXNv9XLSJ/br5Jcwzb8UBhb0c=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01696', 'M', 'BSIT-ELECTRO', '1', 'BSIT 3-AUTO B'),
(1833, 'Sevillano, Marlon Cañete', '10100565', 'T4lr5GLzHngUNcOsd8QBUR3IATfco9EL65UaZ1ytSok=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '10-1-00565', 'M', 'BSIT-ELECTRO', '1', 'BSIT 3-AUTO B'),
(1834, 'Temblor, Christine', '16100364', '5RPyo9DGnCSza5JnaVKvvcv7ZzdlANGr8y05PKryv1k=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00364', 'F', 'BSIT-ELECTRO', '1', 'BSIT 1A'),
(1835, 'Azores, Allan  Jr. L.', '13100073', 'o98jgGvolVzvFy+cKREojZ/NoUr4cq5+Jj4oTcw+ShA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00073', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRO'),
(1836, 'Balse, Panfilo Jr. V.', '15101774', 'LCnaCTfHJ+HuSgyQAYysX9TqU44ErmTAZLQ1F2N9Ok8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01774', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRON'),
(1837, 'Bauno, Mark Anthony M.', '15100726', 'T4lr5GLzHngUNcOsd8QBUR3IATfco9EL65UaZ1ytSok=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00726', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRON'),
(1838, 'Bergamo, Lark Dionesio', '10101100', '2eDfItsR9S+gBlxwy3kRJHHHEe7PkUJD7LtPJNSWY8Y=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '10-1-01100', 'M', 'BSIT-ELECTRO', '2', 'BSIT 3-AUTO'),
(1839, 'Braga, Jorlan C.', '15100236', 'ec+1SLkAllXtx1PwafdeUVPhSwpOWbHBlcoka7GhHkc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00236', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRIC'),
(1840, 'Braga, Renz C.', '15101136', 'LKO+JakyOZ9bVcZTvz9yaRo9lEv59cVPfHPHCLPMHJA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01136', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRIC'),
(1841, 'Cañete, Kennon Rey L.', '15100748', 'OM2NdAdf1097FAxmEnDCdHpZ+Jy/Ixjf0k4mekR8Ad4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00748', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRON'),
(1842, 'Casas, Jhon Philip M.', '15100456', 'k4rcW7XceZ11/KM1Epn26xKhnwuE+rEYibQgyKYGdvE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00456', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRO'),
(1843, 'Castin, Michael Rubia', '15100694', 'H3o9zWve6fX+Twv5nABLikxzykgcX9eI9310pTnmlM4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00694', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRO'),
(1844, 'Collanto, William Jr. T.', '15102040', 't2O3E9AFCWyY+Cuk2nA5tuvDoV5LvoQi9/J6jQPxkeo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02040', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRO'),
(1845, 'Coma, Aljon M.', '15101369', 'Ef+yBKktZNzWbdllE2w1zpd2b28+SdjL89HfQaL7U+o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01369', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRON'),
(1846, 'Dela Cerna, Dino June P.', '15101875', 'w6Lkm8c9HrXvr8kpzW4yK/Hx/CHKiB2NtoF6VGCKt9c=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01875', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRON'),
(1847, 'Gilboy, Gil Ryan Salvador A.', '14102235', 'Pv3LAjShhfRJZioi73gEQ5c4J+QfWJSrWPMMgkiUtTE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02235', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRIC'),
(1848, 'Juntilla, Niel John C.', '15100469', 't2O3E9AFCWyY+Cuk2nA5tuvDoV5LvoQi9/J6jQPxkeo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00469', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRO'),
(1849, 'Napalit, Antonio M.', '15100944', '/ZLUvL7CZC10W1G25OgoHQhyxWtLmmK8WrJxeH/+kzM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00944', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRO'),
(1850, 'Ponce, Robert Monares', '14102542', 'PiBOifTyYiscLEKXUdzsfw1sy6CHNQs8I3lhANsleck=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02542', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRON'),
(1851, 'Roldan, Lenardo Jr. Dalanon', '14102534', 'H3o9zWve6fX+Twv5nABLikxzykgcX9eI9310pTnmlM4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02534', 'M', 'BSIT-ELECTRO', '2', 'BSIT 3-AUTO'),
(1852, 'Sabondo, Arnold Asistol', '09101414', 'LKO+JakyOZ9bVcZTvz9yaRo9lEv59cVPfHPHCLPMHJA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '09-1-01414', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRON'),
(1853, 'Sales, Mark Anthony E.', '15100461', 'U9met//E6saUjBD2aYinbiiXFB/BtrVSr69Mzhc8D/M=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00461', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRON'),
(1854, 'Tabuldan, Edwin C.', '15100107', 'd2eCkKkR7hjzjMCYAvIWb2jr4Aw7SgVCG7E3fEugaJM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00107', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRO'),
(1855, 'Tinoc, Jeremy B.', '15100992', 'a1Uun3rAU9BU8dZqLC99c+HzOEPiXEEmlandmADzVcs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00992', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRO'),
(1856, 'Velez, Jheson Chris S.', '15101371', 'KTqK8Jy+tujAnLs2BWzYEr5XtJk/Bek3lMGdscQbo98=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01371', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRO'),
(1857, 'Villaflores, Daryl G.', '15100895', 'cZaLfgWrsNpaGnk5nK9VDBdXeVVBMeK1Q10ATDpC2Xs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00895', 'M', 'BSIT-ELECTRO', '2', 'BSIT 2-ELECTRO'),
(1858, 'Atok, Edilberto Jr. Catigbe', '13101022', 'QYB+n432WcnoHxf64dzgTgZepSvKkxi6zSThHKO6+j0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01022', 'M', 'BSIT-ELECTRO', '3', 'BSIT 3-ELECTRO'),
(1859, 'Busa, Mark James Rosendal', '14102263', 'UeRAmJSH8CrW9FctrkYm4yrZoxBfdEQJ7niOP2Z07tk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02263', 'M', 'BSIT-ELECTRO', '3', 'BSIT 3-ELECTRO'),
(1860, 'Caparro, Jeeboy D.', '14100352', 'OhsLUYQ9hA64Snkn413Sd8XtPNaelp6tNI81DfIygxY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00352', 'M', 'BSIT-ELECTRO', '3', 'BSIT 3-ELECTRO'),
(1861, 'Espina, Marvin Jay Ollave', '14102508', '/7JtB5l5Nxnygp/TkyBv2zD7/qacX12EHzBkdhaxhaE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02508', 'M', 'BSIT-ELECTRO', '3', 'BSIT 3-AUTO B'),
(1862, 'Estrella, Wilfred Ariza', '12100208', 'zGikEeop/AeioiSWv6EFuPevBYhiUqFoEA/7mXqDI0Y=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00208', 'M', 'BSIT-ELECTRO', '3', 'BSIT 3-AUTO'),
(1863, 'Halichic, Sanyo', '14100031', '/avoQNMy5C1xqaJDUFXNxxrkuIg09XTRNo7qwgK8dNc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00031', 'M', 'BSIT-ELECTRO', '3', 'BSIT 3-ELECTRO'),
(1864, 'Lacson, Demar M.', '14100476', 'puiALYmtZUl9rldbN2YLH6PVrWwBitVzfL5bfShYMxU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00476', 'M', 'BSIT-ELECTRO', '3', 'BSIT 3-ELECTRO'),
(1865, 'Lausin, Mark Dimson', '14100177', 'LQXSCZIgJaUpDWkuoldSKNCkjidN6tfA7gC2lO5/q8o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00177', 'M', 'BSIT-ELECTRO', '3', 'BSIT 3-ELECTRO'),
(1866, 'Lomot, Ric Wilson Centino', '14101895', 'W3kqQ7Z+dRXfx3M4GkO7Za5aqiemBR84B3JSc3heH38=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01895', 'M', 'BSIT-ELECTRO', '3', 'BSIT 3-ELECTRO'),
(1867, 'Macanas, Wilbert M.', '14102018', 'vqmuglJ935VWFQtKBgiKOyE+L83TzH8zZDyP1n8wCfY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02018', 'M', 'BSIT-ELECTRO', '3', 'BSIT 3-ELECTRO'),
(1868, 'Nuñez, Junrey E.', '14101494', 'ukQ6KGBlnQHJil8Q4WUoXBNwHiLhk36qZt2IBPvATlo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01494', 'M', 'BSIT-ELECTRO', '3', 'BSIT 3-ELECTRO'),
(1869, 'Orsal, Ruel Estacion', '14102327', '52Y5hlPyzRDkvLxO0z/P/jg7Nmqpdt4HvgS8REGDXgQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02327', 'M', 'BSIT-ELECTRO', '3', 'BSIT 3-ELECTRO'),
(1870, 'Arante, Agosto Jonales', '13101738', 'RmiTPbHUazZ641KuMmHvEiu30jvXOOpG/Z+sx/Ds66o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01738', 'M', 'BSIT-ELECTRO', '4', 'BSIT 4-ELECTRO'),
(1871, 'Badanoy, Elsed Ebajan', '13100659', '/avoQNMy5C1xqaJDUFXNxxrkuIg09XTRNo7qwgK8dNc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00659', 'M', 'BSIT-ELECTRO', '4', 'BSIT 4-ELECTRO'),
(1872, 'Bustillo, Camilo Boren', '13101326', 'groDY0qe3sqoMR3H+8kufWRqmB1iYAJewuyhZ+TpFhk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01326', 'M', 'BSIT-ELECTRO', '4', 'BSIT 4-ELECTRO'),
(1873, 'Cabalhin, John Carlo German', '13101562', 'ZpH8iMv32gh3A7ph4eMV4Uu1dBUMH8g7FzwhVtHUk2E=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01562', 'M', 'BSIT-ELECTRO', '4', 'BSIT 4-ELECTRO'),
(1874, 'Dangel, Jemark Baco', '13101970', 'PiBOifTyYiscLEKXUdzsfw1sy6CHNQs8I3lhANsleck=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01970', 'M', 'BSIT-ELECTRO', '4', 'BSIT 4-ELECTRO'),
(1875, 'Gelig, Marvin Pujeda', '13101778', 'H3o9zWve6fX+Twv5nABLikxzykgcX9eI9310pTnmlM4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01778', 'M', 'BSIT-ELECTRO', '4', 'BSIT 4-ELECTRO'),
(1876, 'Marquez, Antonio, Jr  Palompon', '12200025', 'OhsLUYQ9hA64Snkn413Sd8XtPNaelp6tNI81DfIygxY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-2-00025', 'M', 'BSIT-ELECTRO', '4', 'BSIT 4-ELECTRO'),
(1877, 'Panugaling, Mark Anthony Peroso', '12101279', 'yFMTDaxIElOLmV2/hgssA1E2cLPrgUAX+qrBk2igIcc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01279', 'M', 'BSIT-ELECTRO', '4', 'BSIT 4-ELECTRO'),
(1878, 'Rojas, Lyn Don Dondriano', '10101976', '+hbtItzU7dnK+ZP2yMxn7TeU3HnNUJ4XH4LamKAXMhc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '10-1-01976', 'M', 'BSIT-ELECTRO', '4', 'BSIT 4-ELECTRO'),
(1879, 'Torres, Cris Angelo Ramos', '13101751', 'NxuQR2RJaQatfFZKpzecRZxvQh8Lk4z27R+Ynf1KmIk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01751', 'M', 'BSIT-ELECTRO', '4', 'BSIT 4-ELECTRO'),
(1880, 'Abancina, Richard S.', '15100782', 'L9Fu+HtlYrs8cFoG1A+mqVqrw/TxB9q484HY9PGcdDk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00782', 'M', 'BSIT-FOODS', '1', 'BSIT 1C'),
(1881, 'Agoylo, Loriefe Mondiego', '16100370', '/3QYC7ZqWXV2D9/ECQ+Z0Hva4Ce98PsK2DTmQgIZ8io=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00370', 'F', 'BSIT-FOODS', '1', 'BSIT 1A'),
(1882, 'Ambe, Alicel Entrolezo', '16100870', 'txu7fpwo1XsBzcUwNSBOZbYrCFOG2zc2a0Zf2LFDrQY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00870', 'F', 'BSIT-FOODS', '1', 'BSIT 1C'),
(1883, 'Bentor, Lian Ran Rosales', '11100195', '52lGPwDdl7UtaRGvqm+YvRq3LJvf6Wh3nqRhrYMnejw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-00195', 'M', 'BSIT-FOODS', '1', 'BSIT 1C'),
(1884, 'Catigbe, Ronie Arrisgado', '12101945', '12pUcVhSHwm24zHYQw2ywP9dPPVd6Fe8iAlgCmk8Wcw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01945', 'M', 'BSIT-FOODS', '1', 'BSIT 3-AUTO'),
(1885, 'Dacer, Jonavelle Matugas', '16100368', 'Ef+yBKktZNzWbdllE2w1zpd2b28+SdjL89HfQaL7U+o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00368', 'F', 'BSIT-FOODS', '1', 'BSIT 1A'),
(1886, 'Denzo, Emely Gebaña', '13101477', 'YKJtpdBcJcA6jSzQM1mE+hDimJEaa0YsaYHBCQNoqs8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01477', 'F', 'BSIT-FOODS', '1', 'BSIT 1B'),
(1887, 'FUENTES, JAY ANN ENOT', '16100659', 'SW8G578B3p4d5KwiR/wnU/Ntb8Ex4Bf/4WPpFV5kdQI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00659', 'F', 'BSIT-FOODS', '1', 'BSIT 1B'),
(1888, 'Gonzales, Joel M.', '15200137', '7uHMoI8sdX7YF5udXv9M5qHrs66UKL1Xn63YtHuaP1c=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00137', 'M', 'BSIT-FOODS', '1', 'BSIT 1C'),
(1889, 'Gumontag, Mary Jane Halius', '16100558', 'T4lr5GLzHngUNcOsd8QBUR3IATfco9EL65UaZ1ytSok=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00558', 'F', 'BSIT-FOODS', '1', 'BSIT 1B'),
(1890, 'Hanna Mae R. Macorol', '16100004', 't/HBq7ayyfRyKRVKog0TC4XGjKUGWbbh7q1WXspeIMM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00004', 'F', 'BSIT-FOODS', '1', 'BSIT 1A'),
(1891, 'Ibañez, Richel Solayao', '14101845', 'w2Pf+ijNwB3/ftmRsLpHH/YcVL9mdb1AhIZgudNLRF0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01845', 'F', 'BSIT-FOODS', '1', 'BSIT 1C'),
(1892, 'LANUZO, MAE ANN CASAS', '00102804', 't2O3E9AFCWyY+Cuk2nA5tuvDoV5LvoQi9/J6jQPxkeo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '00-1-02804', 'F', 'BSIT-FOODS', '1', 'BSIT 1C'),
(1893, 'LEVIDICA, CHARMAINE MONSALES', '16100600', '1A+iIltca2VQoOdLc6qY2Ewl4OLEfzIphlcnUsE2VmI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00600', 'F', 'BSIT-FOODS', '1', 'BSIT 1B'),
(1894, 'Luab, Jevon C.', '15101960', 'qMl/gypSE6PMatOXJij0n4NL0aKJY3Jxax20If4yIZw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01960', 'M', 'BSIT-FOODS', '1', 'BSIT 1A'),
(1895, 'MARIBAO, EDGARDO JR. S.', '16100570', 'vqmuglJ935VWFQtKBgiKOyE+L83TzH8zZDyP1n8wCfY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00570', 'M', 'BSIT-FOODS', '1', 'BSIT 1B'),
(1896, 'Matugas, Federico Luteria', '14101977', '7uHMoI8sdX7YF5udXv9M5qHrs66UKL1Xn63YtHuaP1c=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01977', 'M', 'BSIT-FOODS', '1', 'BSIT 3-AUTO B'),
(1897, 'OBEJERA, JULINE LEDESMA', '16100871', 'zUfI5yhsk6DAIlihXuwv2wxFrnx2bI0zbC4Pjgz7LWY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00871', 'F', 'BSIT-FOODS', '1', 'BSIT 1C'),
(1898, 'Palmaira, Ferdi Marc  M.', '12102208', 'IL3BylrHZuVy4VH2HnhgCt6xYKgg5NHqyFSSIrkMDxk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-02208', 'M', 'BSIT-FOODS', '1', 'BSIT 3-FOODS B'),
(1899, 'Rodrigo, Cherrylane P.', '15200074', 'EflMNzu2uJXW8sUzo5PX27JbctSZX5/ANmtvig902PA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00074', 'F', 'BSIT-FOODS', '1', 'BSIT 1B'),
(1900, 'Sinining, Gella Hanna Mae Botaya', '15200017', 'NUYnESqBdLdm+HXxZeItZDsuHw8445ZBLfLrhc2wzSI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00017', 'F', 'BSIT-FOODS', '1', 'BSIT 1A FOODS'),
(1901, 'Sumapig, Rogilberth N.', '14100485', 'eJXnzfkYHLyRntrzUWhJKa6HtjuWa9bTs0/gn6ooBEE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00485', 'M', 'BSIT-FOODS', '1', 'BSIT 2-FOODS'),
(1902, 'Abad, Marivic O.', '15100238', 't/HBq7ayyfRyKRVKog0TC4XGjKUGWbbh7q1WXspeIMM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00238', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1903, 'Abad, Ronelyn O.', '15102019', 'vuxby32AlQDVxP4hf/gUb+/ewOWP4iG5anAl3MqroXA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02019', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1904, 'Abia, Gladys', '15102237', 'erjUiDvgPEYu+1Gvo6WWC/vtJVTIkS5VoDSOCmgJXnA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02237', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1905, 'Abogado, Joy Ann D.', '15101591', 'yFMTDaxIElOLmV2/hgssA1E2cLPrgUAX+qrBk2igIcc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01591', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOOD C'),
(1906, 'Agosto, Ana Marie G.', '15100725', 'PiBOifTyYiscLEKXUdzsfw1sy6CHNQs8I3lhANsleck=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00725', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1907, 'Bandolon, Annie I.', '15102018', 'O675mbag7W8GDNt6b+VAaNe3A4ABtYd7m/k6U4/PUjE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02018', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1908, 'Binseg, Cindy Rose R.', '15100729', 'erjUiDvgPEYu+1Gvo6WWC/vtJVTIkS5VoDSOCmgJXnA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00729', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1909, 'Cabaltierra, Domingo M.', '15102328', '9ysLopW2PLfEaerTitbMv5zi2z6aBrGSpSyhzbmVC6o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02328', 'M', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1910, 'Calderon, Gean J.', '15100140', '5RPyo9DGnCSza5JnaVKvvcv7ZzdlANGr8y05PKryv1k=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00140', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1911, 'Cañete, Jean Pareño', '12101685', 'Hnc6qCiJeyi6KeVHup5afufADPgmph8jZiftEACdmxk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01685', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1912, 'Cochesa, Chary Shane E.', '15100123', '7uHMoI8sdX7YF5udXv9M5qHrs66UKL1Xn63YtHuaP1c=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00123', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1913, 'Dagami, Rheaffy Angel C.', '15100681', 'SHzIP3QHIsJcZFi7QAdwnO+1AO8xwgFpBO4zNFcTZN8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00681', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOOD C'),
(1914, 'Dayuha, Melanie V.', '15100910', 'QYB+n432WcnoHxf64dzgTgZepSvKkxi6zSThHKO6+j0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00910', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOOD C'),
(1915, 'De Paz, Vrix Orzal', '12100928', 'o98jgGvolVzvFy+cKREojZ/NoUr4cq5+Jj4oTcw+ShA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-00928', 'M', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1916, 'Denoy, Israel', '15100147', 'KTqK8Jy+tujAnLs2BWzYEr5XtJk/Bek3lMGdscQbo98=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00147', 'M', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1917, 'Ebrada, Criselle L.', '15101959', 'K5RsgC8MBujwzF0d02/0XfmusgucnILQIda/dkYXCT4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01959', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1918, 'Elorde, Marivel D.', '15101706', 'XB2RJfo+rfK5s3TFKJPK4FZvx9M83CmIZMgCrtS4+0Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01706', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOOD C'),
(1919, 'Endrina, John Mariel S.', '15102049', '9+bYx2J8tUqjthEiqRZLa51S+z66nXfHZn3E6s67bQE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02049', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOOD C'),
(1920, 'Esperanza, Cheza S.', '15100617', 'NUYnESqBdLdm+HXxZeItZDsuHw8445ZBLfLrhc2wzSI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00617', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1921, 'Garin, Lauren Cuizon', '09100525', 'zAviews2NEhOuWcJbZn1Wjz+1o/Xe2OQGvD6HRFwqdg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '09-1-00525', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1922, 'Gonzales, Danilo Jr. J.', '15100242', 'QYB+n432WcnoHxf64dzgTgZepSvKkxi6zSThHKO6+j0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00242', 'M', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1923, 'Josol, Muriel R.', '15100705', 'Mxte0eJeoculrJx9c8imSOydEmT6i8BB0ZOREJw5Aj4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00705', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOOD C'),
(1924, 'Lanante, Judy Ann P.', '15100396', 'XypBQfFqWI6kIH+OgXyCm2YO7YZmK7filnn3WuCF0XI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00396', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1925, 'Laoreno, Joan Mae B.', '15100423', '1A+iIltca2VQoOdLc6qY2Ewl4OLEfzIphlcnUsE2VmI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00423', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1926, 'Laurente, Rechel D.', '15101937', '0Lxgm7RsoL6q2cs09se+wIUEb9TvPb3U2sjBSF3Y6E4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01937', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1927, 'Lechido, Jesiel Prias', '15102048', '1FsvUSdYBBIKRGLuh9B06B4hjbSQai5yxcrSur4NdP4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02048', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOOD C'),
(1928, 'Legaspi, Antonena P.', '15101689', 'UY8yRZibqVLTIQgTIWFc8cBk4NP3auHxVcgDJjhj92U=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01689', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1929, 'Lepasana, Rhema P.', '15101537', 'klVvsCgxqljmNZJ5N5R/qeSej2AmzNChM+K92CvYqDo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01537', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOOD C'),
(1930, 'Lonesto, Melona D.', '15100195', 'JGCHnsXTKNgfaHzMMivWABcoLioN7OalIwaX9tcw1CA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00195', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1931, 'Macabacyao, Mae R.', '15100191', 'tFCSWWhfXWYMMr+fmL7pIe8CvhZkVhm0NYD+KRmEPTc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00191', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1932, 'Malinao, Noime M.', '15100249', 'sQU+h0zQLLRsiMRISP8NVqj/3sC/RD4pV+6/QCwpn9g=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00249', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1933, 'Masbang, Rey-ann A.', '15100410', 'LQXSCZIgJaUpDWkuoldSKNCkjidN6tfA7gC2lO5/q8o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00410', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1934, 'Masibag, Veah Rose L.', '15101634', 'PFRyNtMqBfKByNdGVRn5o1kByAdYedExX09Td5rXfds=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01634', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1935, 'Narrido, Shaira Jane Petargue', '11101899', 'qXjtKX4vGMVcvgDqqgUN05rF35NOUXccnh4c3JHSPFQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-01899', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1936, 'Nolasco, Andy Cyrell  D.', '15102140', 'XypBQfFqWI6kIH+OgXyCm2YO7YZmK7filnn3WuCF0XI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02140', 'M', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1937, 'Obenza, Klezziel Maye M.', '15100687', '17s9Y2qmeF0b1xB0i3Oazl/EYeWGZxlSV7NuROg0YWQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00687', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1938, 'Pancito, Charles Stephen D.', '15102250', 'sh9/OTktsoWkGEUEoNIfHW6VePlTPobxWzsxdYfJgOU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02250', 'M', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1939, 'Peliño, George S.', '15100728', 'awo7dCcl1+JtccjtaOP4srNn40WxRx8NmC2ypZVtPg8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00728', 'M', 'BSIT-FOODS', '2', 'BSIT 2-FOOD C'),
(1940, 'Rabacio, May Joy S.', '15100104', 'bj4mBBiKfaHwb0jsC3BnX7jArUVOg34LJLMTfFIv0s8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00104', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1941, 'Romano, John Michael A.', '15101372', 'bUOOLLr9geIrVLK2+xD/BnLeGAOYysHr/B52jfVWKDY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01372', 'M', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1942, 'Rosales, Dawn Suzane B.', '15102134', 'IklYVh9eJ3YhOmRQEPl8WpA/j3eUxijWDNdx2O6FVOI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02134', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1943, 'Sabornido, Mary Queen T.', '15100192', '17s9Y2qmeF0b1xB0i3Oazl/EYeWGZxlSV7NuROg0YWQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00192', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1944, 'Sacedor, Rodhel', '15101724', 'p7rgN2ScehXxU6ioKEC0BFY6tNQGL53Cr32dOwZzGQc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01724', 'M', 'BSIT-FOODS', '2', 'BSIT 2-FOOD C'),
(1945, 'Salvatierra, Mariel A.', '15100102', 'EfK/ijNF7c3uB9YLv+/kNHtIZ8/SPNDJIeawRFgco6A=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00102', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1946, 'Sinangote, Miarose V.', '15101624', 'qmfgedohCjmrGFS1SZXiyvNH5uLrANprnvL70x+AKRQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01624', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS'),
(1947, 'Suliva, Romel O.', '15102170', 'ehMumZh/O5nDOAVqi7BZBWkINv3+t5njiLRa9UhP0dI=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02170', 'M', 'BSIT-FOODS', '2', 'BSIT 2-FOOD C'),
(1948, 'Tanzo, Marvie N.', '15102262', 'YxwD2Y0m3RP5A95gZtI10BdA0TMzmHz7xgMvMyg6nOw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02262', 'M', 'BSIT-FOODS', '2', 'BSIT 2-FOOD C'),
(1949, 'Vaporoso, Richelle J.', '15100675', 'DZcXTxoNuqf+q3sSza6A8m3qYRMW0ZZ8Em/qVXFtK1s=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00675', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1950, 'Ventaja, Jessel C.', '15102109', 'cE+WYZP2QN9yG/CmIKOqw9LYUx1FD46ZFI2XLms1up8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02109', 'F', 'BSIT-FOODS', '2', 'BSIT 2-FOODS B'),
(1951, 'Allanic, Reynan T.', '14101580', 'ajjMt29NxnXyPuUKXvOfli+iUz158gMeOnr6bv9XpBE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01580', 'M', 'BSIT-FOODS', '3', 'BSIT 3-FOODS B'),
(1952, 'Amarilla, Rose Ann E.', '14102457', 'niz1g7g66YOH0UV9ST75hWGHUIOcCQBg6QAaBdv5Ue4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02457', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1953, 'Añasco, Jericho M.', '14101200', '6KTuNxNZkrmpwIZ5Z0xFirRsCVP7+8Q15v0D0cOVWck=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01200', 'M', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1954, 'Atok, Jovelyn A.', '14101255', 'NuIgyyWIOrmEND0/SZueXUjvRTJ6JJEJr34fIhYRREA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01255', 'F', 'BSIT-FOODS', '3', 'BSIT 3-RAC'),
(1955, 'Baguna, Jayson Ambe', '13100555', 'ZTFtwEI3aPpuQqA9XeOVfz8MpFWZUI/x8QVYoShmEBo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00555', 'M', 'BSIT-FOODS', '3', 'BSIT 3-AUTO'),
(1956, 'Baltar, Ronalyn M.', '14101483', 'BHiSJNq1vM19HCloNaCH7BdWG7lpBPf4MnfrLmC58Oo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01483', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS B'),
(1957, 'Berallo, Jelyn C.', '14101481', '12qjVh44NtNL+uRB2pgrRzeeq1XPfKjXm3/JCFNquIE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01481', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1958, 'Boren, Evelyn M.', '14101188', 'NuIgyyWIOrmEND0/SZueXUjvRTJ6JJEJr34fIhYRREA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01188', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1959, 'Brezo, Joselyn O.', '14100467', 'NG5mywX+4aL5ZNHQVDI59HIm6PvcEwjoSHncOl70AEk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00467', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1960, 'Castigon, Renalyn J.', '14101269', 'VZS7FHhxt63YuzX0Xm/HgFj8P3E8T7BmPqnrLok7ljE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01269', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1961, 'Catandijan, Jonel', '14101552', 'cMbGA5baLdHwV9aLFHiLUZdB9P6YP6K468J6BYT6zj0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01552', 'M', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1962, 'Cometa, Jea A.', '14100455', 'hf+nlup9Lp1C4qthF2S20BggHU0QX6HrWEjVzdbd0oM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00455', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS B'),
(1963, 'Cumayas, Joylen Onio', '13101508', '94WJqdW9/wdjz6pDEjIX+UJItStzFsi7T3u/muvmpkQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01508', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1964, 'Demate, Frelyn R.', '14100763', 'SGDZk82C9E1J3EF4grQhfY1izxEhCT9oLJucnLh+4XE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00763', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1965, 'Docallos, Kristel Cassandra', '14101669', '8YsZqk+WT3aA8fVn9JJmZexdt/6cZ1QGjxwt9CcFNO0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01669', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1966, 'Jornales, Estela M.', '14100365', 'dWN5gS87+HP/4wpYmMQxz6n5VFj/Rein/7Xz0iSU3OQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00365', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1967, 'Lastima, Christian Paul Tarrayo', '14102619', 'f6OaYEOmA6RT+jdaPoHPZjVOBBKEW3YuWcQkb38rC34=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02619', 'M', 'BSIT-FOODS', '3', 'BSIT 3-FOODS B'),
(1968, 'Machete, Harrish Reingie', '14100261', 'L25QP4S2tzoTKHmBZI/J4NeGaOUfLF33/Ve7n7w6ngU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00261', 'M', 'BSIT-FOODS', '3', 'BSIT 3-FOODS B'),
(1969, 'Marentes, Anna Fe Q.', '14100338', 'OK1LKH+UXOF+yLmt8TkKXD3zIInMwncHfqJHiCtcJTM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00338', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1970, 'Merez, Mary Ann B.', '14101099', 'Xed32CwFcPosP+Rk+2MbIY3zPGaYg5MSrI1+d2K+LOg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01099', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1971, 'Musa, Marjorie Sillar', '14102314', 'm7Zp8L4qg+Yc4kmzhcHukdjrzO2j+7jg+RT91yv1LQg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02314', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS B'),
(1972, 'Poyos, Jahara Mae G.', '14102549', 'yrbGD2ViikJkaVmPcJOeUxEhMI3AEtaco6qk1gQ3FrY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02549', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1973, 'Ramirez, Theresa C.', '14101513', 'cMbGA5baLdHwV9aLFHiLUZdB9P6YP6K468J6BYT6zj0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01513', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1974, 'Saboy, Apple Dawn Moncada', '12102172', 'AOeATAAVLyX3jIHJ8ANlIwaEEDAt1s5DoQVczlTYOzE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-02172', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1975, 'Salut, Iceel Pabilada', '04101171', 'tOnT/IFliXk/k3Nsp71byPrnXXXtOXkDt6J0RFXzGIA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '04-1-01171', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1976, 'Santua, Crisbell C.', '14101839', 'YxwD2Y0m3RP5A95gZtI10BdA0TMzmHz7xgMvMyg6nOw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01839', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS'),
(1977, 'Sevilla, Roelyn Disto', '14102353', 'EfK/ijNF7c3uB9YLv+/kNHtIZ8/SPNDJIeawRFgco6A=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02353', 'F', 'BSIT-FOODS', '3', 'BSIT 3-FOODS B'),
(1978, 'Vijuan, Reyner Mesias', '14102429', '0bnL5BpesJwXqOybcsfbuMCVQEn9PRZPS/2AAD+dwUs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02429', 'M', 'BSIT-FOODS', '3', 'BSIT 3-FOODS B'),
(1979, 'Gervacio, Emma Cielo Cañete', '13102241', '31crnh1HAQ0e6TMeD3Xyp6jTYHze7+d6ID3zVxmkL/k=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02241', 'F', 'BSIT-FOODS', '4', 'BSIT 4-FOODS'),
(1980, 'Inosante, Erwin Agunos', '13102208', 'VZS7FHhxt63YuzX0Xm/HgFj8P3E8T7BmPqnrLok7ljE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02208', 'M', 'BSIT-FOODS', '4', 'BSIT 4-FOODS'),
(1981, 'Narido, Almira Mae Arante', '13101097', 'uUDnKQHx10kVEio1IJbuyoYZUhezeYvDmkDbBnLjdqw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01097', 'F', 'BSIT-FOODS', '4', 'BSIT 4-FOODS'),
(1982, 'Palconit, Joy Diaz', '13101725', 'jzXJ5gtQ5C32PrwmhrLqs4k8z2+r97OCBaPLg2UUhWU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01725', 'F', 'BSIT-FOODS', '4', 'BSIT 4-FOODS'),
(1983, 'Ramirez, Kristine Samong', '13101617', 'Y5kcGbFESWwToTFZQIB4TUHl4TIA4c2Wuav4KWhB9lM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01617', 'F', 'BSIT-FOODS', '4', 'BSIT 4-FOODS'),
(1984, 'Raya, Roselyn German', '13101586', 'bj4mBBiKfaHwb0jsC3BnX7jArUVOg34LJLMTfFIv0s8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01586', 'F', 'BSIT-FOODS', '4', 'BSIT 4-FOODS'),
(1985, 'Sadoguio, Andrew Salut', '05200101', '0scHGLPm0/pEtpbRP7ziDWJ10Eqxh9GwURWYe4gXi4E=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '05-2-00101', 'M', 'BSIT-FOODS', '4', 'BSIT 2-FOODS'),
(1986, 'LAMBIT, AYEN MONARES', '16100603', 'abshwVP0dk/rVRYF3v79J9/1D4EsW5qVAdb7PwkgHIA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00603', 'F', 'BSIT-GARMENT', '1', 'BSIT 1B'),
(1987, 'Narrido, Ma. Cristiana Obligado', '16100395', 'avsVea+InUyWXNxmAGQYmOQI0wCOYFrG4pm4nuaNd5o=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00395', 'F', 'BSIT-GARMENT', '1', 'BSIT 1A'),
(1988, 'PAJE, REZIEL VERADOR', '16100665', 'mQgKwbQH78UFLlx0TwXyNV/gM3Z8Pmc3V1o05wIP/Ik=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00665', 'F', 'BSIT-GARMENT', '1', 'BSIT 1B'),
(1989, 'Sierra, Quenee Managaytay', '16100349', 'bUOOLLr9geIrVLK2+xD/BnLeGAOYysHr/B52jfVWKDY=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00349', 'F', 'BSIT-GARMENT', '1', 'BSIT 1A'),
(1990, 'Adorador, Riza Mae C.', '15101790', 'Wc/Xl+0JPRJXNpxg7j7t/6xIY4iPLS1ksoy02hGsKLs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01790', 'F', 'BSIT-GARMENT', '2', 'BSIT 2-GARMENTS'),
(1991, 'Baculando, Cathrina S.', '15100206', 'xhpe8oNUPThOW3s32e3zvVVKOOpRZ1HmwwL32//VD/c=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00206', 'F', 'BSIT-GARMENT', '2', 'BSIT 2-GARMENTS'),
(1992, 'Carmen, Janny Vee M.', '14100879', 'Pwttp8Amck8TEpywHXd3aC19uRo1nbG0guzdSayU5so=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00879', 'F', 'BSIT-GARMENT', '2', 'BSIT 3-GARMENTS'),
(1993, 'Garcia, Analie L.', '15101425', '4bkbuwod2J8Jsk3g7j3TCLh+MOtBapQMrawaVaVd5OQ=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01425', 'F', 'BSIT-GARMENT', '2', 'BSIT 2-GARMENTS'),
(1994, 'Herra, Sharmaine M.', '15101596', 'RzpXN6DTkkcKZEQCm9Y0o5qo6TqsJOlXaN2f6D6WJYo=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01596', 'F', 'BSIT-GARMENT', '2', 'BSIT 2-GARMENTS'),
(1995, 'Limpiado, Mary Jane C.', '15100173', 'vSEW6uwprQvAJYnpqEt9STjVUDw/1NhA0KmeCD7hUG4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00173', 'F', 'BSIT-GARMENT', '2', 'BSIT 2-GARMENTS'),
(1996, 'Moreno, Jemaila Yvonne R.', '15100250', '+kWsQY+PLrkWEWssc7K9WAg55NGY9NxSZ4qocy9HNNs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00250', 'F', 'BSIT-GARMENT', '2', 'BSIT 2-GARMENTS'),
(1997, 'Pontillas, Dionirose U.', '15101001', 'ev7VSHPLHuQ6X7+JJpnt/h7cxrNBS+4S+tyS62PL07g=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01001', 'F', 'BSIT-GARMENT', '2', 'BSIT 2-GARMENTS'),
(1998, 'Pontillas, Rica N.', '15101687', 'ngmpYD3aRRYTd8DDil33/DXuNGlCUX0h6rTdn/qiwEU=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01687', 'F', 'BSIT-GARMENT', '2', 'BSIT 2-GARMENTS'),
(1999, 'Limpiado, Jhurna B.', '14100043', 'a2pi3UV+PyHcj+coRLD2jTmZ/Ot6ZXjWKvhjxcx99S8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00043', 'F', 'BSIT-GARMENT', '3', 'BSIT 3-GARMENTS'),
(2000, 'Montilla, Carmina Juablar', '14102479', 'jTdhikiryb5f0sEXEPSlMnKfEMDNpTqhccA7Mnqsy/Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02479', 'F', 'BSIT-GARMENT', '3', 'BSIT 3-GARMENTS'),
(2001, 'Panuncio, Mayville P.', '14100855', 'yiMpBhpBkru2XOI/6NhzCrpNWoq68oNOVThPhkSO7Dk=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00855', 'F', 'BSIT-GARMENT', '3', 'BSIT 3-GARMENTS'),
(2002, 'Ramos, Roxanne Padoga', '14102550', '5D8wGkgPNqb6wzPgk/ObS4pwPiAQ8AZ5Uw0jHGFcmiA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02550', 'F', 'BSIT-GARMENT', '3', 'BSIT 3-GARMENTS'),
(2003, 'Amador, Jaene G.', '07100764', 'Y5kcGbFESWwToTFZQIB4TUHl4TIA4c2Wuav4KWhB9lM=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '07-1-00764', 'F', 'BSIT-GARMENT', '4', 'BSIT 4G'),
(2004, 'Howard, Glory Belle  Cali', '05100309', 'I6XLxWxEWfW50Qg5g1fkwH5Uf+G0bzqpeKs/TXVzPUc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '05-1-00309', 'F', 'BSIT-GARMENT', '4', 'BSIT 4G'),
(2005, 'Calvez, Anthony Grapeza', '16100414', 'y0pjYn8z1Ond6q3+vNYIsnQO+uYFvZMCahqWezY5Nws=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00414', 'M', 'BSIT-RAC', '1', 'BSIT 1A'),
(2006, 'Calvez, Mark Gil Sangon', '16100381', 'jTdhikiryb5f0sEXEPSlMnKfEMDNpTqhccA7Mnqsy/Q=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00381', 'M', 'BSIT-RAC', '1', 'BSIT 1A'),
(2007, 'Dagotdot, Juowel Cabulang', '16100005', 'UcgaQsFJCehsSr9879Xe1kakaEE+JzilPT+LfuRP0x0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00005', 'M', 'BSIT-RAC', '1', 'BSIT 1A'),
(2008, 'Delima, Niño P.', '15200077', 'torHbx8zGy2GLo3pzyOyQKtcaBw9npsoDIk3nlVhM1s=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-2-00077', 'M', 'BSIT-RAC', '1', 'BSIT 1B'),
(2009, 'Edos, Rodel C.', '15101588', '12qjVh44NtNL+uRB2pgrRzeeq1XPfKjXm3/JCFNquIE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01588', 'M', 'BSIT-RAC', '1', 'BSIT 2-AUTOMOTI'),
(2010, 'Jumadas, Rodulph Anthony Robaro', '13102257', 'LzQVR11+R1qjwfIrJdeDfkkpk30hXkjBpsOIOIayoos=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02257', 'M', 'BSIT-RAC', '1', 'BSCS 1A'),
(2011, 'Lasaca, James Rhey Solayao', '16100312', 'Re+ePDMl1j8MbdQJctK+P/Y/cTGMcfRr/gOMKfnUPlA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00312', 'M', 'BSIT-RAC', '1', 'BSIT 1A'),
(2012, 'Mondoy, Joven', '16100003', 'VZS7FHhxt63YuzX0Xm/HgFj8P3E8T7BmPqnrLok7ljE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00003', 'M', 'BSIT-RAC', '1', 'BSIT 1A'),
(2013, 'PAYOS, ALVIN G.', '16100865', 'axX7HNZWDKNT5OHvydStiGUnGHjrbUqxoFC/WwB660Y=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00865', 'M', 'BSIT-RAC', '1', 'BSIT 1C'),
(2014, 'Robaro, Mark O''neal Luaya', '16100545', 'AJu6Fth2AzCZaTDYFLXf2/sfwxNp3UJvBRM4v0z3LLc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00545', 'M', 'BSIT-RAC', '1', 'BSIT 1B'),
(2015, 'Romero, Michael B.', '15101525', 'xhpe8oNUPThOW3s32e3zvVVKOOpRZ1HmwwL32//VD/c=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01525', 'M', 'BSIT-RAC', '1', 'BSIT 1A ELECTRI'),
(2016, 'SABALSA, RYAN ALWIN A.', '16100439', 'R3lAkyiOGtmhGZ7cDqdbiDsS6nMqRXZb0gmIJKd+F94=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '16-1-00439', 'M', 'BSIT-RAC', '1', 'BSIT 1C'),
(2017, 'Abad, Dominique S.', '15100421', 'w+8IX1o0llekWqywT7EugrpATPLNlsyNdv1Dnbg7Oec=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00421', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC B'),
(2018, 'Balloso, Matt Jerone Paul N.', '15100776', 'EtUfkvSFMJDoXY738NlFaP9yqb+Hz09HV3ydizbj8Qc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00776', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC C'),
(2019, 'Cabalquinto, Jerick C.', '15100625', 'kU9DpvcMJ/hL8aq2ddGKXHgJX9ogsLYguTWaLP2Ce80=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00625', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC C'),
(2020, 'Cabug, Jicky S.', '15101406', 'VdSjCDAG0FlSA4N7TW8iUbtiumKmZgrrpLU3ZrUhd9Y=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01406', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC B'),
(2021, 'Calvez, Glen G.', '15100709', 'eMAbxltG6nrOL3yxkrnm91Kn5Wzg1Hs/1SlLNt7eZsE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00709', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC C'),
(2022, 'Calvez, Ramoncito M.', '15101501', 'Xed32CwFcPosP+Rk+2MbIY3zPGaYg5MSrI1+d2K+LOg=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01501', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC C'),
(2023, 'Catalan, Christian Kent A.', '15101421', 'SZWzv8AoVTPIwZnijqlGp0/XGQJIH2SR7G/eSFcB6Dw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01421', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC B'),
(2024, 'Cezar, Jerald A.', '15101257', 'bj4mBBiKfaHwb0jsC3BnX7jArUVOg34LJLMTfFIv0s8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01257', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC C'),
(2025, 'Clamonte, Jethol C.', '15102037', 'L/MqI/AtAXw6fcV7tqvuweTjJDt1vtsWDlB6yzFpXWc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02037', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC B'),
(2026, 'Ejorcadas, Osias Jr L.', '15102079', 'UOPSy4azn2b/qAjEj3QwiQ9g5aR4a7E0VfFfFFqMcD0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02079', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC'),
(2027, 'Estrado, Aldren C.', '15101903', 'TEJasR7zY6K475MFLS12vNO2YSVjOM99NOfdIC4OXZ8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01903', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC'),
(2028, 'Fernandez, Mark Japhet R.', '15101529', 'SGDZk82C9E1J3EF4grQhfY1izxEhCT9oLJucnLh+4XE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01529', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC C'),
(2029, 'Gocela Rey John Madeja', '14101911', 'EbpMaVX8EfzRJJRqkGt/vzkt4QarxYawJ4JNfVTHf+g=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01911', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC C'),
(2030, 'Gubantes, Allan Peter N.', '15101579', 'CSVgIkqnBD0feSb5/Xza1u8+1QDgzfn+gPbOFrBjWto=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01579', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC C'),
(2031, 'Hilongo, Ben Unabia', '14200077', 'C2DbwfaPeRKtwvP+vzjSbh0PFie320atH0yO3BJUDII=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-2-00077', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC C'),
(2032, 'Ligoyligoy, Kristoffer I.', '15101780', 'YxwD2Y0m3RP5A95gZtI10BdA0TMzmHz7xgMvMyg6nOw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01780', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC C'),
(2033, 'Lubiano, John Carlo I.', '15100708', 'L/MqI/AtAXw6fcV7tqvuweTjJDt1vtsWDlB6yzFpXWc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-00708', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC B'),
(2034, 'Malolos, Jayson A.', '15101761', 'f9YSP90XWOluuDQMrBcgCLICl40vkAC11moknbSTaHs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01761', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC C'),
(2035, 'Palconit, John Louie S.', '15101481', 'C2DbwfaPeRKtwvP+vzjSbh0PFie320atH0yO3BJUDII=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01481', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC C'),
(2036, 'Palconit, Leo Erwin C.', '15101238', '5QIu2dqIFqNRW4Ggq8U1CZmMOnd5vb2313cwtSgf06A=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01238', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC C'),
(2037, 'Recuerdo, Ronre Jay T.', '15102298', 'LHnuGagUD+flcvZId6/IPlsVLsNgi9X9wzNDHHRut1I=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-02298', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC C'),
(2038, 'Salvo, Hector V.', '15101527', '1EEO4obZkqiueiY9kZwQmoNgdc6vMcBLg/+MaaqRzq8=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01527', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC'),
(2039, 'Tuazon, Rollie Kimber A.', '15101256', 'CN+q97r5k06kiVCE/O5GZZfw2/lbkN1PoDAd0RcvuVs=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '15-1-01256', 'M', 'BSIT-RAC', '2', 'BSIT 2-RAC C'),
(2040, 'Atok, Carter', '14101810', 'VZS7FHhxt63YuzX0Xm/HgFj8P3E8T7BmPqnrLok7ljE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01810', 'M', 'BSIT-RAC', '3', 'BSIT 3-RAC B'),
(2041, 'Bernal, Regor Morales', '13101860', 'KFplcOzP66V95QMe2mEX2tkhscws2cn8mZteMFgxdd4=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01860', 'M', 'BSIT-RAC', '3', 'BSIT 3-RAC'),
(2042, 'Calvez, Rav S.', '14100606', '1fK67OIcwbTTc4BzjNd7uSlApLVAlpMQUKVpcb2cr0Y=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00606', 'M', 'BSIT-RAC', '3', 'BSIT 3-RAC'),
(2043, 'Docallos, Aldwin Batcho', '04101159', 'ajjMt29NxnXyPuUKXvOfli+iUz158gMeOnr6bv9XpBE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '04-1-01159', 'M', 'BSIT-RAC', '3', 'BSIT 3-AUTO'),
(2044, 'Isaga, Mel Jhon Angcahan', '14101475', 'CB010ZNjKQidukxdotKIX9UDCuUH1h5hD6ZqMtbap+E=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01475', 'M', 'BSIT-RAC', '3', 'BSIT 3-RAC B'),
(2045, 'Pecajas, Jayson S.', '14100257', 'EbpMaVX8EfzRJJRqkGt/vzkt4QarxYawJ4JNfVTHf+g=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00257', 'M', 'BSIT-RAC', '3', 'BSIT 3-RAC'),
(2046, 'SAbong, John Kennel T.', '14101154', 'GGZ28nWiFsWbv2Y5ve3+jXlZu1oGm2XLRNywaYO87PA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01154', 'M', 'BSIT-RAC', '3', 'BSIT 3-RAC'),
(2047, 'Salas, Ramy S.', '14100236', 'Re+ePDMl1j8MbdQJctK+P/Y/cTGMcfRr/gOMKfnUPlA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-00236', 'M', 'BSIT-RAC', '3', 'BSIT 3-RAC B'),
(2048, 'Sale, Charlie  Abinedo', '11102053', '9/+9AIfWtAx4p/s35vpTRQmbhfqu9xu7P1LDB73VrXE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '11-1-02053', 'M', 'BSIT-RAC', '3', 'BSIT 3-RAC B'),
(2049, 'Sudario, Felmar Mejarito', '13100333', 'napcDvEHTpyDpdKV4Is72pOUh7JzOOhA59IwaSk85Jc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00333', 'M', 'BSIT-RAC', '3', 'BSIT 3-AUTO B'),
(2050, 'Sumalinog, Feddie C.', '14102247', 'HZqaAx0Rw2C29aLHLnIxMPfSL1EZliirBiyNKnmyosw=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-02247', 'M', 'BSIT-RAC', '3', 'BSIT 3-RAC B'),
(2051, 'Villamor, Judy R.', '14101779', '31crnh1HAQ0e6TMeD3Xyp6jTYHze7+d6ID3zVxmkL/k=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14-1-01779', 'M', 'BSIT-RAC', '3', 'BSIT 3-RAC B'),
(2052, 'Ancero, Billy Mark D.', '12102055', '+eGbbYN2wJ57Y5IwGXM9PnG8lDYLds0260lHXIOmPR0=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-02055', 'M', 'BSIT-RAC', '4', 'BSIT 4-RAC'),
(2053, 'Dorado, Fedric Udtohan', '13101917', '6KTuNxNZkrmpwIZ5Z0xFirRsCVP7+8Q15v0D0cOVWck=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-01917', 'M', 'BSIT-RAC', '4', 'BSIT 4-RAC'),
(2054, 'Locahin, Arben Susaya', '12101221', 'GGZ28nWiFsWbv2Y5ve3+jXlZu1oGm2XLRNywaYO87PA=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '12-1-01221', 'M', 'BSIT-RAC', '4', 'BSIT 4-RAC'),
(2055, 'Macapinig, Jaffey Orito', '13100300', 'kU9DpvcMJ/hL8aq2ddGKXHgJX9ogsLYguTWaLP2Ce80=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-00300', 'M', 'BSIT-RAC', '4', 'BSIT 4-RAC'),
(2056, 'Ochea, Wenzyl Rebayla', '13102364', 'Va5w7U5HF98QxEkPJAJ/uUgqFHvKiQLUDlNmfYGPA8M=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02364', 'M', 'BSIT-RAC', '4', 'BSIT 4-AUTO'),
(2057, 'Sabong, Kevin Macapinig', '10102082', '12qjVh44NtNL+uRB2pgrRzeeq1XPfKjXm3/JCFNquIE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '10-1-02082', 'M', 'BSIT-RAC', '4', 'BSIT 4-RAC'),
(2058, 'Sabordo, Normandy  Sale', '07100474', 'EtUfkvSFMJDoXY738NlFaP9yqb+Hz09HV3ydizbj8Qc=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '07-1-00474', 'M', 'BSIT-RAC', '4', 'BSIT 4-RAC'),
(2059, 'Salvo, Homer Villas', '13102192', 'FjydaMZwCD4+PCCEqmqBMJrMGh2dokmY5ToVw05ww6U=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02192', 'M', 'BSIT-RAC', '4', 'BSIT 4-RAC'),
(2060, 'Denosta, Mark Devin Sumayan.', '13102198', 'VZS7FHhxt63YuzX0Xm/HgFj8P3E8T7BmPqnrLok7ljE=', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '13-1-02198', 'M', 'BSCS', '4', 'BSCS 4-A');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_teachers`
--

CREATE TABLE `tbl_teachers` (
  `id` int(255) NOT NULL,
  `name` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `username` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `contact_number` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `birthdate` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `type` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `detail` text CHARACTER SET latin1,
  `active` int(255) NOT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `CODE` varchar(10000) CHARACTER SET latin1 DEFAULT NULL,
  `SEX` varchar(10000) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `tbl_teachers`
--

INSERT INTO `tbl_teachers` (`id`, `name`, `username`, `password`, `email`, `contact_number`, `birthdate`, `type`, `detail`, `active`, `user_id`, `date_added`, `date_modified`, `CODE`, `SEX`) VALUES
(1, 'fernando quiroz', 'quiroz', 'muHxjrjm4UEplENV9URMeRCbmIEgLlBxi/EunxlPWz0=', 'quiroz@gmail.com', NULL, '', NULL, NULL, 1, 1, '2016-11-07 23:05:29', '2016-11-09 21:01:03', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(255) NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `username` varchar(1000) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `email` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `detail` text,
  `active` int(255) NOT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `contact_number` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `name`, `username`, `password`, `email`, `type`, `detail`, `active`, `user_id`, `date_added`, `date_modified`, `contact_number`) VALUES
(1, 'administrator', 'admin', 'muHxjrjm4UEplENV9URMeRCbmIEgLlBxi/EunxlPWz0=', 'administrator@gad.nsu.edu.ph', 'administrator', NULL, 1, NULL, '2016-10-21 00:00:00', '2016-10-21 00:00:00', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_calendar`
--
ALTER TABLE `tbl_calendar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_downloads`
--
ALTER TABLE `tbl_downloads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_forum`
--
ALTER TABLE `tbl_forum`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_news`
--
ALTER TABLE `tbl_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_students`
--
ALTER TABLE `tbl_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_teachers`
--
ALTER TABLE `tbl_teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_calendar`
--
ALTER TABLE `tbl_calendar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_downloads`
--
ALTER TABLE `tbl_downloads`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_forum`
--
ALTER TABLE `tbl_forum`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_news`
--
ALTER TABLE `tbl_news`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_students`
--
ALTER TABLE `tbl_students`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2061;
--
-- AUTO_INCREMENT for table `tbl_teachers`
--
ALTER TABLE `tbl_teachers`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
