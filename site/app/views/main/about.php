
    <div class="contentArea">

        <div class="divPanel notop page-content">

            <div class="breadcrumbs">
                <a href="index.html">Home</a> &nbsp;/&nbsp; <span>About Us</span>
            </div>

            <div class="row-fluid">
			<!--Edit Main Content Area here-->
                <div class="span8" id="divMain">

                    <h1>About Us</h1>
					<hr>
					<h3>Gender and Development Office</h3>
                                                     
                    					 
  <p>					
<strong>The Gender and Development (GAD) Office </strong>is responsible for the gender mainstreaming efforts of the University with the strong support of  stakeholders and the members of the Gender and Development Focal Point system ( GFPS) headed by the OIC University President. The GAD office promotes the general well-being of every stakeholder and integrates the principle of equality in the trilogical functions of the university: curriculum development, gender responsive research programs, and gender responsive extension programs. The office also initiates programs, activities and projects, and formulates policies on gender equality, women empowerment in the campus and to the community.
  </p>         
<hr>
<h3>Objectives</h3>
<p>
1.&nbsp;&nbsp;&nbsp;Develop stakeholders to be gender sensitive and responsive individuals through capability building programs such as gender sensitivity, gender and development, peace, human rights, health and sexuality education, and other specific gender-related issues;
</p>  
<p>
2.  &nbsp;&nbsp;&nbsp;Promote and integrate the use of gender fair language in all curricular programs and communications of the university;
</p>  
<p>
3.  &nbsp;&nbsp;&nbsp;Establish a research center on gender research and women’s studies in all course programs of the university; gender research on specific topics in light of social issues raised by the MCW; emerging gender issues such as but not limited to sexual orientation and gender identities and GAD research program for GAD planning and Budgeting;
</p>  
<p>
4. &nbsp;&nbsp;&nbsp; Formulate and implement policies that address gender biases and other gender related issues;
</p>  
<p>
5. &nbsp;&nbsp;&nbsp; Integrate the principles of gender equality in the trilogical functions of the university: curriculum and instruction development, gender responsive research programs and gender responsive extension programs;
</p>  
<p>
6. &nbsp;&nbsp;&nbsp; Promote gender equality and women empowerment to the communities for socio-economic gains through extension service programs;
</p>  
<p>
7. &nbsp;&nbsp;&nbsp; Recognize, protect and promote women and men from all forms of discrimination and violation of their rights in the school premises;
</p>  
<p>
8.  &nbsp;&nbsp;&nbsp;Provide assistance to stakeholders who are victims of violence, sexual harassment and other gender –related offenses;
</p>  
<p>
9. &nbsp;&nbsp;&nbsp; Institutionalize gender mainstreaming in all undertakings of the university.
</p>		
<hr>
 <h3  >Contact Information</h3>

    <p>
                        <address><strong><?=COMPANY_NAME;?> OFFICE</strong><br />
                        Naval, Biliran<br />
                        Biliran, Philippines<br />
                        <abbr title="Phone">P:</abbr> (123) 456-7890</address> 
                        <address>  <strong>Email</strong><br />
                        <a href="mailto:#">administrator@gad.nsu.edu.ph</a></address>  
                    </p>     
 <hr>

  <h3  >Location</h3>
<iframe width="100%" height="500" frameborder="0" style="border:0" allowfullscreen src="https://www.google.com/maps/embed/v1/search?q=Naval+State+University,+Naval,+Eastern+Visayas,+Philippines&key=AIzaSyD_WCqHWRlUHlcZwV4TGMv3tcyGLvcSurM" allowfullscreen></iframe>

                 

                </div>
				<!--End Main Content Area here-->
				
				<!--Edit Sidebar Content here-->
                <div class="span4 sidebar">

                    <?php $this->view('main/sidebar'); ?>
                    
                </div>
				<!--End Sidebar Content here-->
            </div>






            

            <div id="footerInnerSeparator"></div>
        </div>
    </div>
 