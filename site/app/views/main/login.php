
    <div class="contentArea">

        <div class="divPanel notop page-content">
            
        	<div class="row-fluid">
			<!--Edit Main Content Area here-->
                <div class="span12" id="divMain">
 
				 <hr>
                </div>
				 <div class="span4" id="">
 
			 
                </div>
				<div class="span4" id="divMain">
		
					<h2 class="form-signin-heading">Sign In.</h2><hr />
							
					   

					 <?php
        
                              
                                if ( !empty($data['errors'])  ){
                                    
                                     
                                    echo "<div class='alert alert-danger'>";
                                    foreach ( $data['errors'] as $error )
                                    echo "$error\n";
                                    echo "</div>";
                                }


                                  if ( !empty($data['success'])  ){
                                    
                                     
                                    echo "<div class='alert alert-success'>";
                                    foreach ( $data['success'] as $error )
                                    echo "$error\n";
                                    echo "</div>";
                                }
                                ?>   
	
			<div class="accordion" id="accordion2">

                	<div class="accordion-group">
                    	<div class="accordion-heading">
                        	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                            	Students
                        	</a>
                    	</div>

                    	<div id="collapseOne" class="accordion-body collapse <?=(isset($_POST['option']) AND $_POST['option'] =='student')   ? 'in' : '';?>   <?=(isset($_POST['option'])==false) ? 'in' : '';?>   ">
                        	<div class="accordion-inner">
                        		<br>
		                           	<form  method="post" action="">
									Username </br> 
							 		<input  type="text" name="username" id="student" value=""  class="input-block-level" placeholder="Username" autocomplete="off" /></br>
							 		Password </br> 
									<input type="password" name="password" id="password" value="" class="input-block-level" placeholder="Password" /> </br>
								 	<input type="hidden" name="option"   value="student"  />
									<input type="submit" value="Login" name="submit"   class="btn btn-default pull-left" title="Login!" />
									</form> 
                        		<br> 
							</div>
                        </div>
                    </div>



                    <div class="accordion-group">
                        <div class="accordion-heading">
                        	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                            	Teachers
                        	</a>
                        </div>
                        
                        <div id="collapseTwo" class="accordion-body collapse <?=(isset($_POST['option']) AND $_POST['option'] =='teacher')   ? 'in' : '';?>">
                        	<div class="accordion-inner">
                        		<br>
		                           <form  method="post" action="">
									Username </br> 
							 		<input type="text" name="username" id="name" value=""  class="input-block-level" placeholder="Username"  autocomplete="off" /> </br>
							 		Password </br> 
									<input type="password" name="password" id="email" value="" class="input-block-level" placeholder="Password" /> </br>
								 	<input type="hidden" name="option"   value="teacher"  />
									<input type="submit" value="Login" name="submit"   class="btn btn-default pull-left" title="Login!" />
									</form> 
								<br>     
                        	</div>
                        </div>
                    </div>


                     <div class="accordion-group">
                        <div class="accordion-heading">
                        	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                            	Administrator
                        	</a>
                        </div>
                        
                        <div id="collapseThree" class="accordion-body collapse <?=(isset($_POST['option']) AND $_POST['option'] =='admin')   ? 'in' : '';?>">
                        	<div class="accordion-inner">
                        		<br>
		                           <form  method="post" action="">
									Username </br> 
							 		<input type="text" name="username" id="name" value=""  class="input-block-level" placeholder="Username"  autocomplete="off" /> </br>
							 		Password </br> 
									<input type="password" name="password" id="email" value="" class="input-block-level" placeholder="Password" /> </br>
								 	<input type="hidden" name="option"   value="admin"  />
									<input type="submit" value="Login" name="submit"   class="btn btn-default pull-left" title="Login!" />
									</form> 
								<br>     
                        	</div>
                        </div>
                    </div>

 

                </div>  <!-- accordion -->
 

			 
                </div> <!-- span4 divMain -->
				 
			 
                <div class="span4 sidebar">

                  
                </div>
				 
            </div>
 

            <div id="footerInnerSeparator"></div>
        </div>
    </div>
