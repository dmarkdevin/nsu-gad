
    <div class="contentArea">

        <div class="divPanel notop page-content">

            <div class="breadcrumbs">
                <a href="index.html">Home</a> &nbsp;/&nbsp; <span>Statistics</span>
            </div>

            <div class="row-fluid">
			<!--Edit Main Content Area here-->
                <div class="span8" id="divMain">

                   
					<h1  >Statistics</h1>

                     
                    <hr>  

                  






<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>CIICT</th>
            <th>Male</th>
            <th>Female</th>
             
            
        </tr>
    </thead>
    <tbody>
        <tr >
            <td>BSCS</td>
            <td><?=$this->pdo->countData('tbl_students',array('SEX'=>'M','CRSCD'=>'BSCS'))?></td>
            <td><?=$this->pdo->countData('tbl_students',array('SEX'=>'F','CRSCD'=>'BSCS'))?></td>
        </tr>
        <tr >
            <td>BSIS</td>
            <td><?=$this->pdo->countData('tbl_students',array('SEX'=>'M','CRSCD'=>'BSIS'))?></td>
            <td><?=$this->pdo->countData('tbl_students',array('SEX'=>'F','CRSCD'=>'BSIS'))?></td>
        </tr>
        <tr >
            <td>BSIT</td>
            <td>

                <?=$this->pdo->countQUERY("SELECT * FROM tbl_students WHERE SEX='M' AND (CRSCD='BSIT' OR CRSCD='BSIT-DRAFTIN' OR CRSCD='BSIT-FOODS' OR CRSCD='BSIT-GARMENT' OR CRSCD='BSIT-AUTO' OR CRSCD='BSIT-ELECTRO' OR CRSCD='BSIT-RAC')")?> 
            </td>
            <td>
            <?=$this->pdo->countQUERY("SELECT * FROM tbl_students WHERE SEX='F' AND (CRSCD='BSIT' OR CRSCD='BSIT-DRAFTIN' OR CRSCD='BSIT-FOODS' OR CRSCD='BSIT-GARMENT' OR CRSCD='BSIT-AUTO' OR CRSCD='BSIT-ELECTRO' OR CRSCD='BSIT-RAC') ")?>
            </td>
        </tr>
   
    
    </tbody>
</table>



<br>

<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>CAS</th>
            <th>Male</th>
            <th>Female</th>
             
            
        </tr>
    </thead>
    <tbody>
        <tr >
            <td>AB COMARTS  </td>
            <td><?=$this->pdo->countData('tbl_students',array('SEX'=>'M','CRSCD'=>'AB COMARTS  '))?></td>
            <td><?=$this->pdo->countData('tbl_students',array('SEX'=>'F','CRSCD'=>'AB COMARTS  '))?></td>
        </tr>
        <tr >
            <td>AB ECON</td>
            <td><?=$this->pdo->countData('tbl_students',array('SEX'=>'M','CRSCD'=>'AB ECON'))?></td>
            <td><?=$this->pdo->countData('tbl_students',array('SEX'=>'F','CRSCD'=>'AB ECON'))?></td>
        </tr>

        <tr >
            <td>BSBA</td>
            <td><?=$this->pdo->countData('tbl_students',array('SEX'=>'M','CRSCD'=>'BSBA'))?></td>
            <td><?=$this->pdo->countData('tbl_students',array('SEX'=>'F','CRSCD'=>'BSBA'))?></td>
        </tr>

        <tr >
            <td>BSBA-FEM </td>
            <td><?=$this->pdo->countData('tbl_students',array('SEX'=>'M','CRSCD'=>'BSBA-FEM'))?></td>
            <td><?=$this->pdo->countData('tbl_students',array('SEX'=>'F','CRSCD'=>'BSBA-FEM'))?></td>
        </tr>
       
   
    
   



    </tbody>
</table>



































  
                </div>
                <!--End Main Content Area here-->
                
                <!--Edit Sidebar Content here-->
                <div class="span4 sidebar">

                     <?php $this->view('main/sidebar'); ?> 
                    
                </div>
                <!--End Sidebar Content here-->
             

                </div>


				 
            </div>






            

            <div id="footerInnerSeparator"></div>
        </div>
    </div>
 