 
<link href='<?=ASSETS;?>fullcalendar/fullcalendar.css' rel='stylesheet' />
<!-- <link href='<?=ASSETS;?>fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' /> -->
<script src='<?=ASSETS;?>fullcalendar/lib/moment.min.js'></script>
<script src='<?=ASSETS;?>fullcalendar/lib/jquery.min.js'></script>
<script src='<?=ASSETS;?>fullcalendar/fullcalendar.min.js'></script>
<script>


 $(document).ready(function() {
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  var calendar = $('#calendar').fullCalendar({
   editable: true,
   eventLimit: true, // allow "more" link when too many events
   header: {
    left: 'prev,next today',
    center: 'title',
    right: ''
   },
   
   events: " <?php  echo EVENTS; ?>",
   
   // Convert the allDay from string to boolean
   eventRender: function(event, element, view) {
    // if (event.allDay === 'true') {
    //  event.allDay = true;
    // } else {
    //  event.allDay = false;
    // }
     event.allDay =  true;
   },
   selectable: false,
   selectHelper: false
 
   
  
 
 
  });
  
 });

</script>
 
 

  <div id='calendar'></div>
 
