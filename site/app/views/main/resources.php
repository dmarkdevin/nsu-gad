
    <div class="contentArea">

        <div class="divPanel notop page-content">

            <div class="breadcrumbs">
                <a href="index.html">Home</a> &nbsp;/&nbsp; <span>Resources</span>
            </div>

            <div class="row-fluid">
			<!--Edit Main Content Area here-->
                <div class="span8" id="divMain">

                    <h1>Resources</h1>
					<hr>

     
                <?php 

                
                foreach ($data['downloads'] as $key => $value){ 
                ?>  

                      
                     
<a  data-toggle="modal" href="#myModal<?=($value['id']); ?>" >  <i class="icon-file-alt"></i>   <?=($value['name']); ?></a> <br>
                        
                                         
                                      <div id="myModal<?=($value['id']); ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h3 id="myModalLabel">Resources</h3>
                                            </div>
                                            <div class="modal-body">
                                             <h3><?=($value['name']); ?></h3>
                                            <hr>

                                             <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                            
                                                            <tbody>
                                                               
                                                                <tr class="even gradeC">
                                                                    <td>File Name</td>
                                                                    <td><?=($value['file']); ?></td>
                                                                     
                                                                </tr>
                                                                <tr class="odd gradeA">
                                                                    <td>Date Added</td>
                                                                    <td><?=showdate($value['date_added']); ?></td>
                                                                    
                                                                </tr>
                                                                <tr class="even gradeA">
                                                                    <td>Upload by</td>
                                                                    <td><?=ucwords($this->pdo->selectData('tbl_users','name',array('id'=>$value['user_id'])) ); ?></td>
                                                                   
                                                                </tr>
                                                                <tr class="even gradeA">
                                                                    <td colspan="2">

                                                                     <a  class="btn btn-info pull-left" id="submitButton" target="_blank" href="<?=URL_ROOT;?>downloads/download/<?=$value['file'];?>?dmark=<?=$value['file'];?>">Click here to download the file!</a>


                                                                      
                                                                    </td>
                                                                     
                                                                   
                                                                </tr>
                                                               
                                                                
                                                            </tbody>
                                                        </table>                    
                                            
                                            
                                            
                                            
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn" data-dismiss="modal">Close</button>
                                               <!-- <button class="btn btn-primary">Save changes</button>-->
                                            </div>
                                        </div>
                             
                           
		        <?php } ?>
                

                </div>
				<!--End Main Content Area here-->
				
				<!--Edit Sidebar Content here-->
                <div class="span4 sidebar">

                   <?php $this->view('main/sidebar'); ?>
                    
                </div>
				<!--End Sidebar Content here-->
            </div>






            

            <div id="footerInnerSeparator"></div>
        </div>
    </div>
 