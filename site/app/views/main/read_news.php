 
    <div class="contentArea">

        <div class="divPanel notop page-content">

            <div class="breadcrumbs">
                <a href="index.html">Home</a> &nbsp;/&nbsp; <span><?=strtoupper($fields['title']); ?></span>
            </div>

            <div class="row-fluid">
			
			    				
					                 
            	<!--Edit Main Content Area here-->
                <div class="span8" id="divMain">

                  
                    <h3><?=strtoupper($fields['title']); ?></h3>
                    <p><strong>BY ADMINISTRATOR <?=showdate($fields['date_added']); ?></strong></p> 
					<hr>	

    
                    <p><img src="<?=NEWS;?><?=$fields['file']; ?>" class="img-polaroid" style=" margin:0 auto;" alt=""></p>
                    <br> 
					<p><?=nl2br($fields['content']); ?></p>
				</div>	                             
                    					                  
				<!--/End Main Content Area here-->	   

                <!--Edit Sidebar Content here-->
                <div class="span4">                    
                  <?php $this->view('main/sidebar',$data); ?>                
                </div>
                <!--/End Sidebar Content -->                 
					
							
            </div>

            <div id="footerInnerSeparator"></div>
        </div>
    </div>
 