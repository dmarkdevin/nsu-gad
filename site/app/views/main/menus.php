<div class="navbar">

<button type="button" class="btn btn-navbar-highlight btn-large btn-primary" data-toggle="collapse" data-target=".nav-collapse">
    NAVIGATION 
    <span class="icon-chevron-down icon-white"></span>
</button>


<div class="nav-collapse collapse">
<ul class="nav nav-pills ddmenu">



<li class="<?=(isset($data['title']) AND $data['title'] =='HOME') ? 'active' : '';?>"><a href="<?=URL_ROOT;?>">Home</a></li>


<!-- ####################################################################### -->

<li class="<?=(isset($data['title']) AND $data['title'] == "ABOUT US") ? 'active':'';?>">
<a href="<?=URL_ROOT;?>main/about">About Us</a>
</li> 

<!-- ####################################################################### -->

<li class="dropdown">
<a href="" class="dropdown-toggle">GFPS <b class="caret"></b></a>
<ul class="dropdown-menu">                            
<li><a href="<?=URL_ROOT;?>main/gfps#gfps">Focal Point System</a></li>
<li><a href="<?=URL_ROOT;?>main/gfps#os">Organizational Structure</a></li>


</ul>
</li>

 <!-- ####################################################################### -->

 <li class="<?=(isset($data['title']) AND $data['title'] == "STATISTICS") ? 'active':'';?>">
 <a href="<?=URL_ROOT;?>main/statistics">Statistics</a>
 </li>

 <!-- ####################################################################### -->

<li class="<?=(isset($data['title']) AND $data['title'] == "RESOURCES") ? 'active':'';?>">
<a href="<?=URL_ROOT;?>main/downloads">Resources</a></li>

<!-- ####################################################################### -->
<li class="<?=(isset($data['title']) AND $data['title'] =="CALENDAR") ? "active":"";?>">
<!--
    <a  target="_blank" href="<?=URL_ROOT;?>main/events"  onclick="window.open(this.href,'newWin','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=600')"      >Calendar of Activities</a>
--><a href="<?=URL_ROOT;?>main/calendar">Calendar of Activities</a>
	</li>
<!-- ####################################################################### -->

<li class="<?=(isset($data['title']) AND $data['title'] == "FORUM") ? 'active':'';?>">
<a href="<?=URL_ROOT;?>main/forum">Forum</a></li>
<!-- ####################################################################### -->
<!-- 
<li class="<?=(isset($data['title']) AND $data['title'] == "GFPS") ? 'active':'';?>"><a href="<?=URL_ROOT;?>main/gfps">GFPS</a></li> -->

<!--   <li class="dropdown">
<a href="" class="dropdown-toggle">GFPS <b class="caret"></b></a>
<ul class="dropdown-menu">                            
<li><a href="<?=URL_ROOT;?>main/gfps#gfps">Focal Point System</a></li>
<li><a href="<?=URL_ROOT;?>main/gfps#os">Organizational Structure</a></li>


</ul>
</li>

 -->

<!-- <li class="<?=(isset($data['title']) AND $data['title'] == "CONTACT US") ? 'active':'';?>"><a href="<?=URL_ROOT;?>main/contact">Contact</a></li>
-->








<?php if(isset($_SESSION[ID])   ) { ?>

    <?php if(($_SESSION[TYPE]) == "admin" ){ ?>
                            <li class="<?=(isset($data['title']) AND $data['title'] == "SIGN IN") ? 'active':'';?>"><a href="<?=URL_ROOT;?>page/index">Manage</a></li>
    <?php }else {  ?> 
                       <!--  <li class="<?=(isset($data['title']) AND $data['title'] == "Profile") ? 'active':'';?>"><a href="<?=URL_ROOT;?>main/profile">Profile</a></li> -->


                        <li class="<?=(isset($data['title']) AND $data['title'] == "MESSAGE") ? 'active':'';?>"><a href="<?=URL_ROOT;?>main/inbox">Message</a></li> 
   
    <?php } ?>                              
 

      <!--   <li class="<?=(isset($data['title']) AND $data['title'] == "MESSAGE") ? 'active':'';?>"><a href="<?=URL_ROOT;?>main/message">Message</a></li> -->

<?php }else{  ?>      
                             <li class="<?=(isset($data['title']) AND $data['title'] == "SIGN IN") ? 'active':'';?>"><a href="<?=URL_ROOT;?>main/login">Sign In</a></li>
<?php } ?>                  
                             	<!-- <li class="dropdown">
								<a href="" class="dropdown-toggle">Manage <b class="caret"></b></a>
								<ul class="dropdown-menu">                            
								<li><a href="<?=URL_ROOT;?>manage/news">News</a></li>
								<li><a href="<?=URL_ROOT;?>manage/events">Events</a></li>
								<li><a href="<?=URL_ROOT;?>manage/forum">Forum</a></li>
								<li><a href="<?=URL_ROOT;?>manage/downloads">Downloads</a></li>
								 
								</ul>
								</li> -->


                            <!-- <li ><a href="about.html">About</a></li>
                            <li class="dropdown">
                            <a href="page.html" class="dropdown-toggle">Page <b class="caret"></b></a>
                            <ul class="dropdown-menu">                            
                            <li><a href="full.html">Full Page</a></li>
                            <li><a href="2-column.html">Two Column</a></li>
                            <li><a href="3-column.html">Three Column</a></li>
                            <li><a href="../documentation/index.html">Documentation</a></li>
                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle">Dropdown Item &nbsp;&raquo;</a>
                            <ul class="dropdown-menu sub-menu">
                            <li><a href="#">Dropdown Item</a></li>
                            <li><a href="#">Dropdown Item</a></li>
                            <li><a href="#">Dropdown Item</a></li>
                            </ul>
                            </li>
                            </ul>
                            </li>
                            <li><a href="services.html">Services</a></li>
                            <li><a href="portfolio.html">Portfolio</a></li>                         
                            <li><a href="contact.php">Contact</a></li> -->
                            </ul>
                            </div>
                        </div>