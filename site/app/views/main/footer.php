
<a href="#" class="go-top">Go Top</a>
	
    <div id="footerOuterSeparator"></div>
	  
    <div id="divFooter" class="footerArea">

        <div class="divPanel">

            <!-- <div class="row-fluid">
                <div class="span3" id="footerArea1">
                
                    <h3>About Company</h3>

                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry�s standard dummy text ever since the 1500s.</p>
                    
                    <p> 
                        <a href="#" title="Terms of Use">Terms of Use</a><br />
                        <a href="#" title="Privacy Policy">Privacy Policy</a><br />
                        <a href="#" title="FAQ">FAQ</a><br />
                        <a href="#" title="Sitemap">Sitemap</a>
                    </p>

                </div>
                <div class="span3" id="footerArea2">

                    <h3>Recent Blog Posts</h3> 
                    <p>
                        <a href="#" title="">Lorem Ipsum is simply dummy text</a><br />
                        <span style="text-transform:none;">2 hours ago</span>
                    </p>
                    <p>
                        <a href="#" title="">Duis mollis, est non commodo luctus</a><br />
                        <span style="text-transform:none;">5 hours ago</span>
                    </p>
                    <p>
                        <a href="#" title="">Maecenas sed diam eget risus varius</a><br />
                        <span style="text-transform:none;">19 hours ago</span>
                    </p>
                    <p>
                        <a href="#" title="">VIEW ALL POSTS</a>
                    </p>

                </div>
                <div class="span3" id="footerArea3">

                    <h3>Sample Content</h3> 
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry�s standard dummy text ever since the 1500s. 
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry�s standard dummy text ever since the 1500s.
                    </p>

                </div>
                <div class="span3" id="footerArea4">

                    <h3>Get in Touch</h3>  
                                                               
                    <ul id="contact-info">
                    <li>                                    
                        <i class="general foundicon-phone icon"></i>
                        <span class="field">Phone:</span>
                        <br />
                        (123) 456 7890 / 456 7891                                                                      
                    </li>
                    <li>
                        <i class="general foundicon-mail icon"></i>
                        <span class="field">Email:</span>
                        <br />
                        <a href="mailto:info@yourdomain.com" title="Email">info@yourdomain.com</a>
                    </li>
                    <li>
                        <i class="general foundicon-home icon" style="margin-bottom:50px"></i>
                        <span class="field">Address:</span>
                        <br />
                        123 Street<br />
                        12345 City, State<br />
                        Country
                    </li>
                    </ul>

                </div>
            </div> -->

            <br /><br />

            <!-- 
            <div class="row-fluid">
                <div class="span12">
                    <p class="copyright">
                        Copyright � 2013 Your Company. All Rights Reserved.
                    </p>

                    <p class="social_bookmarks">
                        <a href="#"><i class="social foundicon-facebook"></i>�Facebook</a>
			<a href=""><i class="social foundicon-twitter"></i>�Twitter</a>
			<a href="#"><i class="social foundicon-pinterest"></i>�Pinterest</a>
			<a href="#"><i class="social foundicon-rss"></i>�Rss</a>
                    </p>
                </div>
            </div> -->

        </div>
    </div>
</div>
 
 

<script src="<?=ASSETS;?>scripts/jquery.min.js" type="text/javascript"></script> 
 <!-- fullcalendar -->
 <script src='<?=ASSETS;?>fullcalendar/fullcalendar.min.js'></script>
 <!-- sticky back to top -->
 <script src='<?=ASSETS;?>dmarkdevin/script.js'></script>


<script src="<?=ASSETS;?>scripts/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=ASSETS;?>scripts/default.js" type="text/javascript"></script>
<script src="<?=ASSETS;?>mask/jquery.maskedinput.js" type="text/javascript"></script>
 


<!-- button to top-->
 <script>
 $(document).ready(function() {
			// Show or hide the sticky footer button
			$(window).scroll(function() {
				if ($(this).scrollTop() > 200) {
					$('.go-top').fadeIn(200);
				} else {
					$('.go-top').fadeOut(200);
				}
			});
			
			// Animate the scroll to top dmarkdevin@gmail.com
			$('.go-top').click(function(event) {
				event.preventDefault();
				
				$('html, body').animate({scrollTop: 0}, 300);
			})
		});
		

$(function() {
    //$.mask.definitions['~'] = "[+-]";
    $("#student").mask("99-9-99999");
  
});
	 
 </script>



 
<script src="<?=ASSETS;?>scripts/carousel/jquery.carouFredSel-6.2.0-packed.js" type="text/javascript"></script>
<script type="text/javascript">$('#list_photos').carouFredSel({ responsive: true, width: '100%', scroll: 2, items: {width: 320,visible: {min: 2, max: 6}} });</script>
<script src="<?=ASSETS;?>scripts/camera/scripts/camera.min.js" type="text/javascript"></script>
<script src="<?=ASSETS;?>scripts/easing/jquery.easing.1.3.js" type="text/javascript"></script>
<script type="text/javascript">function startCamera() {$('#camera_wrap').camera({ fx: 'scrollLeft', time: 2000, loader: 'none', playPause: false, navigation: true, height: '35%', pagination: true });}$(function(){startCamera()});</script>

</body>
</html>