
    <div class="contentArea">

        <div class="divPanel notop page-content">

            <div class="breadcrumbs">
                <a href="index.html">Home</a> &nbsp;/&nbsp; <span>Forum</span>
            </div>

            <div class="row-fluid">
            <!--Edit Main Content Area here-->
                <div class="span8" id="divMain">

                    <h1>Forum</h1>
                    <hr>

								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                  <!--   <thead>
                                        <tr>
                                            <th>Topics</th>
                                            <th>Comment</th>
                                            <th >Added By</th>
                                            <th>Date Added</th>
                                            
                                        </tr>
                                    </thead> -->
                                    <tbody>




 <?php 



                $query = "SELECT * FROM tbl_forum ORDER BY date_added DESC";       
                $records_per_page=20;

                $newquery = $this->pdo->paging($query,$records_per_page);
                $stmt = $this->pdo->conn->prepare($newquery);
                $stmt->execute();

                if($stmt->rowCount()>0){
                    while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
                ?>


<tr>
<td><a href="<?=URL_ROOT;?>main/comment/<?=($row['id']); ?>"><?=($row['topic']); ?>

(<?=$this->pdo->countData('tbl_comment',array('forum_id'=>$row['id']))?>) </a>
</td>
<td width="20%"><?=ucwords($this->pdo->selectData('tbl_users','name',array('id'=>$row['user_id']))); ?>


</td>
<td width="20%"><?=showdate($row['date_added']); ?></td>

</tr>


                <?php
                    }
                }else{
                ?>
                <tr>
                    <td colspan="4">Nothing here...</td>
                </tr>
                     
                <?php    
                }

               
                ?>  
                            
                                    
                                    </tbody>
                                </table>

                                <?php  $this->pdo->paginglink($query,$records_per_page,"main/forum/");  ?>

              
             
                </div>
                <!--End Main Content Area here-->
                
                <!--Edit Sidebar Content here-->
                <div class="span4 sidebar">

                    <?php $this->view('main/sidebar'); ?>
                    
                </div>
                <!--End Sidebar Content here-->
            </div>






            

            <div id="footerInnerSeparator"></div>
        </div>
    </div>
 