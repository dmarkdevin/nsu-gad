
    <div class="contentArea">

        <div class="divPanel notop page-content">

            <div class="breadcrumbs">
                <a href="index.html">Home</a> &nbsp;/&nbsp; <span>About Us</span>
            </div>

            <div class="row-fluid">
			<!--Edit Main Content Area here-->
                <div class="span8" id="divMain">

                    <h1>Change Password</h1>
					<hr>


                            <?php
        
                                        if (!empty($data['errors']))
                                        {
                                            echo "<div class='alert alert-danger'>Please fix the following errors:\n<ul>";
                                            foreach ($data['errors'] as $error)
                                            echo "<li>$error</li>\n";
                                            echo "</ul></div>";
                                        }

                                        if (!empty($data['success']))
                                        {
                                            echo "<div class='alert alert-success'>";
                                             
                                            echo "<li>".$data['success']."</li>";
                                            echo "</div>";
                                        }

                                        
                                        ?>
				            <!--Start Contact form -->                                                      
<form name="enq" method="post"  onsubmit="return validation();">

                                 
                                
               


  <fieldset>
    Current Password  </br>
    <input type="password" name="current_password" id="current_password" value=""  class="input-block-level" placeholder="" /> New Password  </br>
        <input type="password" name="new_password" id="new_password" value=""  class="input-block-level" placeholder="" />
Confirm Password  </br>
            <input type="password" name="confirm_password" id="confirm_password" value=""  class="input-block-level" placeholder="" /> 


    <div class="actions">
    <input type="submit" value="Save Changes" name="submit" id="submitButton" class="btn btn-default pull-left" title="Click here to submit your message!" />
    </div>
    
    </fieldset>
</form>  
                </div>
				<!--End Main Content Area here-->
				
				<!--Edit Sidebar Content here-->
                <div class="span4 sidebar">

                    <?php $this->view('main/sidebar'); ?>
                    
                </div>
				<!--End Sidebar Content here-->
            </div>






            

            <div id="footerInnerSeparator"></div>
        </div>
    </div>
 