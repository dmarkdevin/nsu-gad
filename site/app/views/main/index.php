




    <div class="contentArea">

        <div class="divPanel notop page-content">
            
        	           <div class="row-fluid">
			<!--Edit Main Content Area here-->
                <div class="span8" id="divMain">




                <?php 



                $query = "SELECT * FROM tbl_news  ORDER BY date_added DESC";       
                $records_per_page=2;

                $newquery = $this->pdo->paging($query,$records_per_page);
                $stmt = $this->pdo->conn->prepare($newquery);
                $stmt->execute();

                if($stmt->rowCount()>0){
                    while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
                ?>


                    <a href="<?=URL_ROOT;?>main/read_news/<?=($row['id']); ?>"><h3><?=strtoupper($row['title']); ?></h3></a>
                        <p><strong>BY <?=ucwords($this->pdo->selectData('tbl_users','name',array('id'=>$row['user_id']))); ?> 
                            <?=showdate($row['date_added']); ?></strong></p>     
                        <div class="row-fluid">     
                            <div class="span4">                           
                                <img src="<?=NEWS;?><?=$row['file']; ?>" class="img-polaroid" style="margin:5px 0px 15px;" alt="">   
                            </div>          
                            <div class="span8">              
                                <p><?=readmore($row['content']); ?> ...
                                    <a href="<?=URL_ROOT?>main/read_news/<?=($row['id']); ?>">Full Story &raquo;</a>
                                </p>
                            </div>       
                        </div>
                    <hr>


                <?php
                    }
                }else{

                    echo "Nothing here..." ;

                }

                $this->pdo->paginglink($query,$records_per_page,"main/index/"); 
                ?>  
                                         

                			 
                </div>
				<!--End Main Content Area here-->
				
				<!--Edit Sidebar Content here-->
                <div class="span4 sidebar">

                   <?php $this->view('main/sidebar'); ?>
				
			

 
            </div>

            <div id="footerInnerSeparator"></div>
        </div>
    </div>
