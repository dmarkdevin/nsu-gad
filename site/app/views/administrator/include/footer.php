        </div>
        <!-- end delay -->         
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?=ASSETS;?>sb-admin-2-1.0.8/bower_components/jquery/dist/jquery.min.js" ></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=ASSETS;?>sb-admin-2-1.0.8/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?=ASSETS;?>sb-admin-2-1.0.8/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    
    <!-- DataTables JavaScript -->
    <script src="<?=ASSETS;?>sb-admin-2-1.0.8/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?=ASSETS;?>sb-admin-2-1.0.8/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>



    <!-- Morris Charts JavaScript -->
 <!--    <script src="<?=ASSETS;?>sb-admin-2-1.0.8/bower_components/raphael/raphael-min.js"></script>
    <script src="<?=ASSETS;?>sb-admin-2-1.0.8/bower_components/morrisjs/morris.min.js"></script>
    <script src="<?=ASSETS;?>sb-admin-2-1.0.8/js/morris-data.js"></script>
 -->
    <!-- Custom Theme JavaScript -->
    <script src="<?=ASSETS;?>sb-admin-2-1.0.8/dist/js/sb-admin-2.js"></script>

     <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
     
    $('#Modal').modal({backdrop: 'static', keyboard: false})  

    $(document).ready(function() {
        $('#dataTables-example,#dataTables-example2').DataTable({
                responsive: true,
                stateSave: true,
                
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
                // ,

                // "language": {
                //     "lengthMenu": "Display _MENU_ records per page",
                //     "zeroRecords": "Nothing found - sorry",
                //     "info": "Showing page _PAGE_ of _PAGES_",
                //     "infoEmpty": "No records available",
                //     "infoFiltered": "(filtered from _MAX_ total records)"
                // }

        });
    });
    </script>
    

</body>

</html>
