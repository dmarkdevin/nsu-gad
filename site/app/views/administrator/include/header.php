<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; />
    
    <meta name="description" content="">
    <meta name="author" content="">
 

    <title><?=$data['title'];?></title>



    <!-- Bootstrap Core CSS --> 
    <link href="<?=ASSETS;?>sb-admin-2-1.0.8/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    
    <!-- Custom CSS -->
    <link href="<?=ASSETS;?>sb-admin-2-1.0.8/dist/css/sb-admin-2.css" rel="stylesheet">

    
    <!-- Custom Fonts -->
    <link href="<?=ASSETS;?>sb-admin-2-1.0.8/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- ################ -->

    <!-- DataTables CSS -->
    <link href="<?=ASSETS;?>sb-admin-2-1.0.8/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<?=ASSETS;?>sb-admin-2-1.0.8/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    
    <!-- delay -->
    <link href="<?=ASSETS;?>resume/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


       <style>

.red{
    /*background: red;*/
    color:red;
}      
.red:hover{
   
    color:red;
}     

h1{
text-align:center;
font-size:28px
}
hr{
border:0;
border-bottom:1.5px solid #ccc;
margin-top:-10px;
margin-bottom:30px
}

label{
font-size:17px

}
 
       </style>

<style>
     /* to avoid double arrow at datatable sorting*/
     .dataTable > thead > tr > th[class*="sort"]::after{display: none}
</style>
     
<style>
    *{
         /*font-family: "Courier New", Courier, monospace;*/
         font-family:  "Lucida Console", Monaco, monospace;

    }
    .table-responsive{
        padding-top:20px;
    }
    .form-control,span,.panel {
        font-family: arial;
        /*border-radius: 0px;*/
    }
    label{
       font-style: normal;
       font-weight: normal;

    }
     h2 {
        width: 100%; 
        text-align: center; 
        border-bottom: 1px solid #d1ccc9; 
        line-height: 0.1em;
        margin: 10px 0 20px; 
        font-size: 14px;
        color: #4596e5;
    } 

    h2 span { 
        background:#fff; 
        padding:0 10px; 
    }
    .fieldname{
        text-align: right;
    }
    .fieldvalue{
        text-align: left;
        font-weight: bold;
    }
    address{
        padding-left:20px;
    }
  /*  input {
        text-transform:uppercase;

    }
*/
 .navbar ,.panel-default
.panel-heading{
/*background-color:   #67BCDB;*/
background-color:   ;
color:;
}

    </style>    

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">
               <?=COMPANY_NAME;?>  
                
                <!-- <img src="<?=ASSETS;?>image/cvisnet_new.jpg"    height="80px">  -->

                </a>
              
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
               
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <?=$_SESSION[NAME].' ';?><i class="fa fa-caret-down"></i> 
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?=URL_ROOT;?>page/change_password"><i class="fa fa-user fa-fw"></i> Change Password</a>
                        </li>
                        <li><a href="<?=URL_ROOT;?>main" target="_blank"> <i class="fa fa-sign-in fa-fw"></i> Site</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?=URL_ROOT;?>page/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        

                    
                         
                        <li>
                            <a href="<?=URL_ROOT;?>page/index" ><i class="fa fa-home fa-fw"></i> Main</a>
                        </li>


                         <li>
                            <a href="<?=URL_ROOT;?>messages/index"><i class="fa fa-file-o fa-fw"></i> Message</a>
                        </li>
                       

                       <li>
                            <a href="<?=URL_ROOT;?>news/index"><i class="fa fa-file-o fa-fw"></i> News Feed</a>
                        </li>
                      
                        <li>
                            <a href="<?=URL_ROOT;?>events/index"><i class="fa fa-calendar fa-fw"></i> Events</a>
                        </li> 
                        <li>
                            <a href="<?=URL_ROOT;?>downloads/index"><i class="fa fa-download fa-fw"></i> Downloads</a>
                        </li>

                        <li>
                            <a href="<?=URL_ROOT;?>forum/index"><i class="fa fa-list-alt fa-fw"></i> Forum</a>
                        </li>
                   <!--       
                        <li>
                            <a href="<?=URL_ROOT;?>category/index"><i class="fa fa-cloud fa-fw"></i> Category</a>
                        </li> -->
                         
                        <li>
                            <a href="<?=URL_ROOT;?>students/index"><i class="fa  fa-users fa-fw"></i> Students</a>
                        </li>
                         <li>
                            <a href="<?=URL_ROOT;?>teachers/index"><i class="fa  fa-users fa-fw"></i> Teachers</a>
                        </li>
                          <li>
                            <a href="<?=URL_ROOT;?>users/index"><i class="fa  fa-users fa-fw"></i> Users</a>
                        </li>
                         
                                              
                       
                        
                       

                        <li>
                            <a href="<?=URL_ROOT;?>page/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>


                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>


 
        <div id="page-wrapper">

            <div class="row">
                <div class="col-lg-12">
                 <h6>&nbsp;</h6>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        <!-- delay -->    
        <div class="markFade">
 
