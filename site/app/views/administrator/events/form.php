<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-clock-o fa-fw"></i> <?=$data['title'];?>  
            </div>
            <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="post" action="" >
                            <div class="col-lg-12"> 
                                <?php
        
                                if ( !empty($data['errors'])  ){
                                    
                                    echo "<div class='alert alert-danger'>Please fix the following errors:\n<ul>";
                                    foreach ( $data['errors'] as $error )
                                    echo "<li>$error</li>\n";
                                    echo "</ul></div>";
                                }
                                ?>                                       
              
                            </div>
                            
                            <div class="col-lg-12">
                               
                                <div class="form-group">
                                    <label>Title *</label>
                                   
                                    <input type="text" name="title" value="<?=isset($fields['title']) ? $fields['title']:'' ;?>" class="form-control" />
                                    <p class="help-block"></p>
                                </div>
                                    
                             <div class="form-group">
                                    <label>Start Day *</label>
                                   
                                    <input type="date" name="start" value="<?=isset($fields['start']) ? $fields['start']:'' ;?>" class="form-control" required />
                                    <p class="help-block"></p>
                                </div>
                                         <div class="form-group">
                                    <label>End Day *</label>
                                   
                                    <input type="date" name="end" value="<?=isset($fields['end']) ? $fields['end']:'' ;?>" class="form-control" required />
                                    <p class="help-block"></p>
                                </div>
                                       
                             
                                
                                 
                                       
                            
                                       
                            </div>

                               
                            

                            <!-- /.col-lg-6 (nested) --> 



                          
                                    
                            <!-- /.col-lg-6 (nested) -->
                            
                            <div class="col-lg-6">
                                <button type="submit" name="submit" class="btn btn-default">Submit Button</button>

                                <a href="<?=URL_ROOT.$this->class;?>/index" class="btn btn-default">Back Button</a>
                            </div>
                        </form>

                    </div>
                    <!-- /.row (nested) -->               
                </div>
                <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->






 