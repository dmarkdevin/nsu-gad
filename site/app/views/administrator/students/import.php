 

<!-- form para sa import csv file  start here-->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
              <?=$data['title'];?>  
            </div>
            <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="post" action=""  enctype="multipart/form-data">
                            <div class="col-lg-12"> 
                                <?php
        
                                if ( !empty($data['errors'])  ){
                                    
                                    echo "<div class='alert alert-danger'>Please fix the following errors:\n<ul>";
                                    foreach ( $data['errors'] as $error )
                                    echo "<li>$error</li>\n";
                                    echo "</ul></div>";
                                }
                                ?>                                       
              
                            </div>

                           <div class="col-lg-12">
                                <div class="form-group">
                                    <select name="COLLEGE">
                                        <option value="CAS">CAS</option>
                                         <option value="CIICT">CIICT</option>
                                        <option value="COE">COE</option>
                                        <option value="COED">COED</option>
                                        <option value="COT">COT</option>
                                        <option value="COME">COME</option>
                                    </select>
                                
                                </div>


                            </div>     
                            
                           <div class="col-lg-12">
                                <div class="form-group">
                                    <label>CSV FILE *</label>
                                   
                                    <input type="file" name="file" value="" >Only csv files<br />
                                    <p class="help-block">CSV FILES MUST HAVE A FORMAT   (NO|CODE|NAME|SEX|CRSCD|YEAR|SECCD) </p>
                                </div>
                                       
                              
                                       
                            </div>

                               
                            

                            <!-- /.col-lg-6 (nested) --> 



                          
                                    
                            <!-- /.col-lg-6 (nested) -->
                            
                            <div class="col-lg-6">
                                <button type="submit" name="import" class="btn btn-default">Submit Button</button>

                                <a href="<?=URL_ROOT.$this->class;?>/index" class="btn btn-default">Back Button</a>
                            </div>
                        </form>

                    </div>
                    <!-- /.row (nested) -->               
                </div>
                <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->






 