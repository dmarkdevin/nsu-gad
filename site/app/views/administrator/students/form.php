 


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
              <?=$data['title'];?>  
            </div>
            <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="post" action=""  />
                            <div class="col-lg-12"> 
                                <?php
        
                                if ( !empty($data['errors'])  ){
                                    
                                    echo "<div class='alert alert-danger'>Please fix the following errors:\n<ul>";
                                    foreach ( $data['errors'] as $error )
                                    echo "<li>$error</li>\n";
                                    echo "</ul></div>";
                                }
                                ?>                                       
              
                            </div>
                            
                            <div class="col-lg-6">
                               
                                <div class="form-group">
                                    <label>Name</label>
                                   
                                    <input type="text" name="name" value="<?=isset($fields['name']) ? $fields['name']:'' ;?>" class="form-control" />
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>Birth Date</label>
                                   
                                    <input type="date" name="birthdate" value="<?=isset($fields['birthdate']) ? $fields['birthdate']:'' ;?>" class="form-control" />
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>Email Address</label>
                                   
                                    <input type="email" name="email" value="<?=isset($fields['email']) ? $fields['email']:'' ;?>" class="form-control" />
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>username</label>
                                   
                                    <input type="text" name="username" value="<?=isset($fields['username']) ? $fields['username']:'' ;?>" class="form-control" readonly/>
                                    <p class="help-block"></p>
                                </div>
                                



                                <div class="form-group">
                                    <label>password</label>
                                   
                                    <input type="text" name="password" value="<?=isset($fields['password']) ? $fields['password']:'' ;?>" class="form-control" />
                                    <p class="help-block"></p>
                                </div>

                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>CODE</label>
                                   
                                    <input type="text" name="CODE" value="<?=isset($fields['CODE']) ? $fields['CODE']:'' ;?>" class="form-control" />
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>SEX</label>
                                   
                                    <input type="text" name="SEX" value="<?=isset($fields['SEX']) ? $fields['SEX']:'' ;?>" class="form-control" />
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>CRSCD</label>
                                   
                                    <input type="text" name="CRSCD" value="<?=isset($fields['CRSCD']) ? $fields['CRSCD']:'' ;?>" class="form-control" />
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>YEAR</label>
                                   
                                    <input type="text" name="YEAR" value="<?=isset($fields['YEAR']) ? $fields['YEAR']:'' ;?>" class="form-control" />
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>SECCD</label>
                                   
                                    <input type="text" name="SECCD" value="<?=isset($fields['SECCD']) ? $fields['SECCD']:'' ;?>" class="form-control" />
                                    <p class="help-block"></p>
                                </div>

                                <div class="form-group">
                                    <label>COLLEGE</label>
                                   
                                    <input type="text" name="COLLEGE" value="<?=isset($fields['COLLEGE']) ? $fields['COLLEGE']:'' ;?>" class="form-control" />
                                    <p class="help-block"></p>
                                </div>
                                    
                             
                             
                                
                                 
                                       
                            
                                       
                            </div>

                               
                            

                            <!-- /.col-lg-6 (nested) --> 



                          
                                    
                            <!-- /.col-lg-6 (nested) -->
                            
                            <div class="col-lg-6">
                                <button type="submit" name="submit" class="btn btn-default">Submit Button</button>

                                <a href="<?=URL_ROOT.$this->class;?>/index" class="btn btn-default">Back Button</a>
                            </div>
                        </form>

                    </div>
                    <!-- /.row (nested) -->               
                </div>
                <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
 