

<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?=$data['title'];?>

                <div class="pull-right">
                <a href="<?=URL_ROOT.$this->class;?>/import" class="btn btn-default btn-xs dropdown-toggle">Import  </a>
                
                    <a href="<?=URL_ROOT.$this->class;?>/register" class="btn btn-default btn-xs dropdown-toggle">Add New </a>
                
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <div class="table-responsive col-md-12">
                                
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th width="30%">Name</th>
                                <th width="10%">AGE</th>
                                <th width="10%">GENDER</th>
                                 <th  width="15%">COURSE</th>
                                 <th  width="15%">COLLEGE</th>
                                 
                                 
                                <th   width="20%" >Action</th>  
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                   <th width="30%">Name</th>
                                <th width="10%">AGE</th>
                                <th width="10%">GENDER</th>
                                 <th  width="15%">COURSE</th>
                                 <th  width="15%">COLLEGE</th>
                                 
                                 
                                <th   width="20%" >Action</th>  
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach ($data['list'] as $key => $value){ ?>  
                            <tr> 
                                <td> <?=($value['name']); ?></td>
                                <td> <?=(age($value['birthdate'])) != 0 ? age($value['birthdate']):''; ?></td>
                                <td> <?=($value['SEX']); ?></td>
                              <td> <?=($value['CRSCD']); ?></td>
                              <td> <?=($value['COLLEGE']); ?></td>
                              

 
                                <td >
                                <a href="<?=URL_ROOT.$this->class;?>/update/<?=($value['id']);?>">Edit</a> | 
                                
                                <a href="<?=URL_ROOT.$this->class;?>/delete/<?=$value['id'];?>" onclick="return confirm('Are you sure you want to delete this record?');">Delete</a>
                                </td>
                            </tr>       
                            <?php } ?>

                        </tbody>
                        </table>

                    </div>
                    <!-- table responsive -->
                </div>
                <!-- /.table-responsive -->
                
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

