 


            
            <div class="row">
                <div class="col-lg-12">
                     

                    <div class="panel panel-default">
                        <div class="panel-heading">
                             <?=$data['title'];?> Form
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                 <form role="form" method="post" action="" >
                                    <div class="col-lg-12"> 
                                        <?php
        
                                        if (!empty($data['errors']))
                                        {
                                            echo "<div class='alert alert-danger'>Please fix the following errors:\n<ul>";
                                            foreach ($data['errors'] as $error)
                                            echo "<li>$error</li>\n";
                                            echo "</ul></div>";
                                        }

                                        if (!empty($data['success']))
                                        {
                                            echo "<div class='alert alert-success'>";
                                             
                                            echo "<li>".$data['success']."</li>";
                                            echo "</div>";
                                        }

                                        
                                        ?>
              
                                    </div>
                                   
            
                                <div class="col-lg-12">
                                    
                                   
                                       
                                    <div class="form-group">
                                          <label>Current Password *</label>
                                          <input type="password" name="current_password" value="" class="form-control">
                                          <p class="help-block"></p>
                                    </div>
                                    <div class="form-group">
                                          <label>New Password *</label>
                                          <input type="password" name="new_password" value="" class="form-control">
                                          <p class="help-block"></p>
                                    </div>
                                    <div class="form-group">
                                          <label>Confirm Password *</label>
                                          <input type="password" name="confirm_password" value="" class="form-control">
                                          <p class="help-block"></p>
                                    </div>
                                       
                                    

                                </div>
                                
                                <div class="col-lg-12">
                                    <button type="submit" name="submit" class="btn btn-default">Submit Button</button>

                                    <a href="<?=URL_ROOT;?>page/" class="btn btn-default">Back Button</a>
                                </div>
                                </form>

                            </div>
                            <!-- /.row (nested) -->



                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
         
            </div>
            <!-- /.row -->






 