

<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?=$data['title'];?>

                <div class="pull-right">

                    <a href="<?=URL_ROOT.$this->class;?>/register" class="btn btn-default btn-xs dropdown-toggle">Add New </a>
                
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <div class="table-responsive col-md-12">
                                
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th width="40%">Title</th>
                                <th>Author</th>
                                <th>Date</th>
                                
                                <th   >Action</th>  
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Title</th>
                                <th>Author</th>
                                <th>Date</th>

                                <th   >Action</th>  
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach ($data['list'] as $key => $value){ ?>  
                            <tr> 
                                <td><?=strtoupper($value['title']); ?></td>
                                <td><?=$this->pdo->selectData('tbl_users','name',array('id'=>$value['user_id'])); ?> </td>
                                <td><?=showdate($value['date_added']); ?></td>
            

 
                                <td >
                                <a href="<?=URL_ROOT.$this->class;?>/update/<?=($value['id']);?>">Edit</a> | 
                                
                                <a href="<?=URL_ROOT.$this->class;?>/delete/<?=$value['id'];?>" onclick="return confirm('Are you sure you want to delete this record?');">Delete</a>
                                </td>
                            </tr>       
                            <?php } ?>

                        </tbody>
                        </table>

                    </div>
                    <!-- table responsive -->
                </div>
                <!-- /.table-responsive -->
                
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

