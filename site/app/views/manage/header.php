<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title> 
       <?=isset($data['title']) ? $data['title']:COMPANY_NAME;?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Html5TemplatesDreamweaver.com">
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> <!-- Remove this Robots Meta Tag, to allow indexing of site -->

    <link href="<?=ASSETS;?>scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=ASSETS;?>scripts/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Icons -->
    <link href="<?=ASSETS;?>scripts/icons/general/stylesheets/general_foundicons.css" media="screen" rel="stylesheet" type="text/css" />  
    <link href="<?=ASSETS;?>scripts/icons/social/stylesheets/social_foundicons.css" media="screen" rel="stylesheet" type="text/css" />
    <!--[if lt IE 8]>
        <link href="scripts/icons/general/stylesheets/general_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="scripts/icons/social/stylesheets/social_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
    <link rel="stylesheet" href="<?=ASSETS;?>scripts/fontawesome/css/font-awesome.min.css">
    <!--[if IE 7]>
        <link rel="stylesheet" href="scripts/fontawesome/css/font-awesome-ie7.min.css">
    <![endif]-->

    <link href="<?=ASSETS;?>scripts/carousel/style.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS;?>scripts/camera/css/camera.css" rel="stylesheet" type="text/css" />

    <link href="http://fonts.googleapis.com/css?family=Syncopate" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Abel" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Pontano+Sans" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet" type="text/css">

    <link href="<?=ASSETS;?>styles/custom.css" rel="stylesheet" type="text/css" />

						<script>(function(d, s, id) {
                          var js, fjs = d.getElementsByTagName(s)[0];
                          if (d.getElementById(id)) return;
                          js = d.createElement(s); js.id = id;
                          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1100860799944377";
                          fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
						
	               
</head>
<body id="pageBody">

<div id="divBoxed" class="container">

    <div class="transparent-bg" style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;z-index: -1;zoom: 1;"></div>

    <div class="divPanel notop nobottom">
           <div class="row-fluid">
                <div class="span12">
                    <div id="divLogo"  class="pull-left">

                             
                                <div class="span3">                           
                                     <a href="" ><img src="<?=ASSETS;?>images/gad-logo.png" alt="Site Name" title="Site Name" /></a>
                                </div>          
                                <div class="span9" >              
                                    <a href="" id="divSiteTitle">Gender and Development</a><br />
                                    <a href="" id="divTagLine">Naval State Universtiy  </a>
                                 </div>       
                           
                 
                       
                       
                 
                      
                
                        

                    </div>
                    

                    <div id="divMenuRight" class="pull-left">
                    <div class="navbar">
                        <button type="button" class="btn btn-navbar-highlight btn-large btn-primary" data-toggle="collapse" data-target=".nav-collapse">
                        NAVIGATION <span class="icon-chevron-down icon-white"></span>
                        </button>
                            <div class="nav-collapse collapse">
                            <ul class="nav nav-pills ddmenu">
                            <li class="<?=(isset($data['title']) AND $data['title'] =='HOME') ? 'active' : '';?>"><a href="<?=URL_ROOT;?>">Home</a></li>
                            <li class="<?=(isset($data['title']) AND $data['title'] =="EVENTS") ? "active":"";?>"><a href="<?=URL_ROOT;?>main/events">Events</a></li>
                            <li class="<?=(isset($data['title']) AND $data['title'] == "DOWNLOADS") ? 'active':'';?>"><a href="<?=URL_ROOT;?>main/downloads">Downloads</a></li>
                            <li class="<?=(isset($data['title']) AND $data['title'] == "FORUM") ? 'active':'';?>"><a href="<?=URL_ROOT;?>main/forum">Forum</a></li>
                            <li class="<?=(isset($data['title']) AND $data['title'] == "CONTACT US") ? 'active':'';?>"><a href="<?=URL_ROOT;?>main/contact">Contact</a></li>
                            <li class="<?=(isset($data['title']) AND $data['title'] == "ABOUT US") ? 'active':'';?>"><a href="<?=URL_ROOT;?>main/about">About Us</a></li>
                            <li class="<?=(isset($data['title']) AND $data['title'] == "SIGN IN") ? 'active':'';?>"><a href="<?=URL_ROOT;?>main/login">Sign In</a></li>
                             	<li class="dropdown">
								<a href="" class="dropdown-toggle">Manage <b class="caret"></b></a>
								<ul class="dropdown-menu">                            
								<li><a href="<?=URL_ROOT;?>manage/news">News</a></li>
								<li><a href="<?=URL_ROOT;?>manage/events">Events</a></li>
								<li><a href="<?=URL_ROOT;?>manage/forum">Forum</a></li>
								<li><a href="<?=URL_ROOT;?>manage/downloads">Downloads</a></li>
								 
								</ul>
								</li>


                            <!-- <li ><a href="about.html">About</a></li>
                            <li class="dropdown">
                            <a href="page.html" class="dropdown-toggle">Page <b class="caret"></b></a>
                            <ul class="dropdown-menu">                            
                            <li><a href="full.html">Full Page</a></li>
                            <li><a href="2-column.html">Two Column</a></li>
                            <li><a href="3-column.html">Three Column</a></li>
                            <li><a href="../documentation/index.html">Documentation</a></li>
                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle">Dropdown Item &nbsp;&raquo;</a>
                            <ul class="dropdown-menu sub-menu">
                            <li><a href="#">Dropdown Item</a></li>
                            <li><a href="#">Dropdown Item</a></li>
                            <li><a href="#">Dropdown Item</a></li>
                            </ul>
                            </li>
                            </ul>
                            </li>
                            <li><a href="services.html">Services</a></li>
                            <li><a href="portfolio.html">Portfolio</a></li>                         
                            <li><a href="contact.php">Contact</a></li> -->
                            </ul>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        <?php if(isset($data['title']) && $data['title'] == "HOME" ): ?>   

        <div class="row-fluid">
            <div class="span12">

                <div id="headerSeparator"></div>

                <div class="camera_full_width">
                    <div id="camera_wrap">
                        <div data-src="<?=ASSETS;?>slider-images/banner.png" ><div class="camera_caption fadeFromBottom cap2">
                        

							<b>Strengthing GAD Advocacy and Sexual Harassment Free Campus</b>
							<p>The GAD Office conducted a gender sensitivity seminar.</p>


                        </div>
                        </div>
                        <div data-src="<?=ASSETS;?>slider-images/gad.png" >
						<div class="camera_caption fadeFromBottom cap1">Gender and Development</div>
                        </div>
						
                        
                    </div>
                    <br style="clear:both"/><div style="margin-bottom:40px"></div>
                </div>               

                <div id="headerSeparator2"></div>

            </div>
        </div>
    <?php endif; ?>


    </div>
