
    <div class="contentArea">

        <div class="divPanel notop page-content">

            <div class="breadcrumbs">
                <a href="index.html">Home</a> &nbsp;/&nbsp; <span>Contact Us</span>
            </div>

            <div class="row-fluid">
			<!--Edit Main Content Area here-->
                <div class="span8" id="divMain">

                <iframe width="700" height="450" frameborder="0" style="border:0"
                src="https://www.google.com/maps/embed/v1/search?q=Naval+State+University,+Naval,+Eastern+Visayas,+Philippines&key=AIzaSyD_WCqHWRlUHlcZwV4TGMv3tcyGLvcSurM" allowfullscreen></iframe>

                </div>
				<!--End Main Content Area here-->
				
				<!--Edit Sidebar Content here-->
                <div class="span4 sidebar">
                    <div class="sidebox">
                        <h3 class="sidebox-title">Contact Information</h3>
                    <p>
                        <address><strong><?=COMPANY_NAME;?></strong><br />
                        Address<br />
                        City, State, Zip<br />
                        <abbr title="Phone">P:</abbr> (123) 456-7890</address> 
                        <address>  <strong>Email</strong><br />
                        <a href="mailto:#">first.last@gmail.com</a></address>  
                    </p>     
                    
                </div>
				<!--End Sidebar Content here-->
            </div>






            

            <div id="footerInnerSeparator"></div>
        </div>
    </div>
 