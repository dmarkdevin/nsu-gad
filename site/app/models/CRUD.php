<?php
// platform design by mark devin denosta since 2014 revised multiple times. email me @ dmarkdevin@gmail.com or call me 09464275811


class CRUD{
	
	public $conn;

	public function __construct(){
	 
			try{
				$this->conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME,DB_USERNAME,DB_PASSWORD);
				// $this->conn->exec("set names utf8");
				  
			$this->url = $this->url();

		    }catch(PDOException $e){
				die($e->getMessage());
			}
 
	}

 	public function url()
	{
		if(isset($_GET['url'])){

			$url = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
 
			return $url;

		} 
		
	}
 
	public function viewData($table){
		try{

      
  	 	$q =  $this->conn->prepare('SELECT * FROM '.$table);
  	 	 $this->conn->exec("set names utf8");
				  
  	 	$q->execute();
  	 	$list = $q->fetchAll(PDO::FETCH_ASSOC);
      
		return $list;

		}catch(PDOException $e){
			die($e->getMessage());
		}
	}

	 public function viewQUERY($query){
		try{

      
  	 	$q =  $this->conn->prepare($query);
  	 	$q->execute();
  	 	$list = $q->fetchAll(PDO::FETCH_ASSOC);
      
		return $list;

		}catch(PDOException $e){
			die($e->getMessage());
		}
	}


	public function countQUERY($query){
		
		try{
			 
			$q =  $this->conn->prepare($query);
  	 		$q->execute();		
			if($q->rowCount()>0) {
				 return $q->rowCount(); 
			}else{
				return 0; 
			}
				
		}catch(PDOException $e){
			die($e->getMessage());
		}
		
	}	

 	 

	public function fetchtheData($table,$where){
		try{
				$w = array_keys($where);
				foreach($w as $val){	
					$field[] = $val."=?";	
				}
      
  	 	$q =  $this->conn->prepare('SELECT * FROM '.$table.' WHERE '.implode(' AND ',$field));
  	 	$q->execute(array_values($where));
  	 	$list = $q->fetchAll(PDO::FETCH_ASSOC);
      
		return $list;

		}catch(PDOException $e){
			die($e->getMessage());
		}
	}

	public function countData($table,$where){
		
		try{
			$w = array_keys($where);
				foreach($w as $val){	
					$field[] = $val."=?";	
				}
			$q = $this->conn->prepare("SELECT * FROM ".$table." WHERE ".implode(' AND ',$field));
			$q->execute(array_values($where));		
			if($q->rowCount()>0) {
				 return $q->rowCount(); 
			}else{
				return 0; 
			}
				
		}catch(PDOException $e){
			die($e->getMessage());
		}
		
	}	
	public function existData($table,$where){
		
		try{
			$w = array_keys($where);
				foreach($w as $val){	
					$field[] = $val."=?";	
				}
			$q = $this->conn->prepare("SELECT * FROM ".$table." WHERE ".implode(' AND ',$field));
			$q->execute(array_values($where));		
			if($q->rowCount()>0){
				return true; 
			}else{
				return false; 
			}

		}catch(PDOException $e){
			die($e->getMessage());
		}
		
	}
 

	public function updateData($table,$params,$where){
		
		try{
			$k = array_keys($params);	
			$w = array_keys($where);
				foreach($k as $val){	
					$fields[] = $val."=?"; 	
				}
				foreach($w as $val){	
					$field[] = $val."=?";	
				}
			$q = $this->conn->prepare("UPDATE ".$table." SET ".implode(',',$fields)." WHERE ".implode(' AND ',$field));
			$check = $q->execute(array_merge(array_values($params),array_values($where)));		
			if($check){
				return true; 
			}else{
				return false;
			}				
		}catch(PDOException $e){
			die($e->getMessage());
		}
			
	}
	
	public function insertData($table,$params){
		
		try{
			$k = array_keys($params);	
			foreach($k as $val){	$fields[]	= $val."=?"; 	}
			$q = $this->conn->prepare("INSERT INTO ".$table." SET ".implode(',',$fields));
			$check = $q->execute(array_values($params));	
			if($check){ 
				return true; 
			}else{
				return false;
			}
		}catch(PDOException $e){
			die($e->getMessage());
		}
	}
	
	public function deleteData($table,$id){
		
		try{
			$q = $this->conn->prepare("DELETE FROM $table WHERE id=:id");
			$check = $q->execute(array(':id'=>$id));
			if($check){
				return true;
			}else{
				return false; 
			}
		}catch(PDOException $e){
			die($e->getMessage());
		}
	}	
	public function selectData($table,$index,$params){
			
		try{
			$k = array_keys($params);	
				foreach($k as $val){	
					$fields[] = $val."=?"; 	
				}
			$q = $this->conn->prepare("SELECT * FROM ".$table." WHERE ".implode(' AND ',$fields));
			 $this->conn->exec("set names utf8");
		
			$q->execute(array_values($params));		
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data[$index];	
		}catch(PDOException $e){
			die($e->getMessage());
		}
	}





	public function paging($query,$records_per_page)
	{
		$starting_position=0;
		if(isset($this->url[2]))
		{
			$starting_position=($this->url[2]-1)*$records_per_page;
		}
		$query2=$query." limit $starting_position,$records_per_page";
		return $query2;
	}
	
	public function paginglink($query,$records_per_page,$page)
	{ 
		// $self = $_SERVER['PHP_SELF'];
		$self = URL_ROOT.$page;
		
 		// echo $this->url[2]; 

		$stmt = $this->conn->prepare($query);
		$stmt->execute();
		
		$total_no_of_records = $stmt->rowCount();
		
		if($total_no_of_records > 0)
		{
			?><div class="pagination"><ul ><?php
			$total_no_of_pages=ceil($total_no_of_records/$records_per_page);
			$current_page=1;


			if(isset($this->url[2]))
			{
				$current_page=$this->url[2];
			}
			if($current_page!=1)
			{
				$previous =$current_page-1;
				echo "<li><a href='".$self."1'>First</a></li>";
				echo "<li><a href='".$self."".$previous."'>Previous</a></li>";
			}
			for($i=1;$i<=$total_no_of_pages;$i++)
			{
				if($i==$current_page)
				{
					echo "<li><a href='".$self."".$i."' style='color:red;'>".$i."</a></li>";
				}
				else
				{
					echo "<li><a href='".$self."".$i."'>".$i."</a></li>";
				}
			}
			if($current_page!=$total_no_of_pages)
			{
				$next=$current_page+1;
				echo "<li><a href='".$self."".$next."'>Next</a></li>";
				echo "<li><a href='".$self."".$total_no_of_pages."'>Last</a></li>";
			}
			?></ul></div><?php
		}
	}
	
}