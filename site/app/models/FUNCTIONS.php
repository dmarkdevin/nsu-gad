<?php

class FUNCTIONS{
	
	public function url()
	{
		if(isset($_GET['url'])){

			$url = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
 
			return $url;

		} 
		
	}
	public function calendardate($date){
		if( !empty($date) AND isset($date)){
			$date = strtotime($date);  
			$date = date("Y-m-d",$date);
		}else{
			$date = "";
			
		}	
			
			return $date;
	}	

 	public function is_loggedin()
	{
		if(isset($_SESSION[ID]))
		{
			return true;
		}else{
			$this->redirect(URL_ROOT.'users/logout');
		}
	}
	
	public function redirect($url)
	{
		header("Location: $url");
	}
	
	public function logout()
	{
		session_destroy();
		unset($_SESSION[ID]);
		$this->redirect(URL_ROOT.'home/login');
	}


	public function encrypt($Data){
		$id=(double)$Data*525325.24;
			return base64_encode($id);
	}

	public function decrypt($Data){
		$url_id=base64_decode($Data);
			$id=(double)$url_id/525325.24;
				return $id;
	}


	public function numeric_month($date){

	 	if( !empty($date) AND isset($date)){
			$date = strtotime($date);  
			$date = date("m",$date);
		}else{
			$date = "";
			
		}
		return $date;
	}


	public function date_range($month,$day,$year){
		$date = strtotime($month." 1,".$year);
		$date = date("t",$date);
		
		switch($day){
			
			case 1 ;
			$result = "1-15";
			break;
			
			case 2;
			$result = "16-".$date;
			break;
			
			default:
			$result = "";
			break;
			
		} 
		
		return $result;
		
	}

	 
}