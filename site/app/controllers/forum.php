<?php


class Forum extends Controller {
	
		
	public $class = "forum";			/* to be change */
	public $table = "tbl_forum";		/* to be change */

	public function __construct() {
		
		 
		$this->pdo = $this->model('CRUD');
		$this->obj = $this->model('FUNCTIONS');
		$this->url = $this->obj->url();

		$this->default = URL_ROOT.$this->class.'/index';

		
		
		$this->obj->is_loggedin();

	}
	 
	public function index()
	{

		 
		 
		$data['title'] 	= 'MANAGE FORUM';   
		$data['list'] 	= $this->pdo->viewData($this->table);  

		$this->view('administrator/include/header',$data);
 		$this->view('administrator/'.$this->class.'/index',$data);
 		$this->view('administrator/include/footer');
 		
 	 	 
 		

		 
 	}

 	public function register()
	{
			$data['title'] 	= 'CREATE TOPIC';   
			  
		 
				if (isset($_POST['submit']))
				{

					empty($_POST['topic']) ?		$errors[] = 'You forgot to enter the topic'	: '';
					

					$field = array(
							'topic'		=>$_POST['topic'],
							'content'		=>$_POST['content'],
							'user_id'   =>$_SESSION[ID],
							'date_added' =>date(DATE_FORMAT),
							'publish' => 1

							);



					if (empty($errors)) { 	

							if($this->pdo->insertData($this->table,$field)){

								 $this->obj->redirect($this->default);
								 
							
							}else{
								echo "error";
							}

					}else{

			      		$data['errors'] = $errors;
			    
			    	}

								
				}


			$this->view('administrator/include/header',$data);
	 		$this->view('administrator/'.$this->class.'/form',$data);
	 		$this->view('administrator/include/footer');
 		
 	 	 
 		 
		 
 	}
 

 	public function update()
	{
			$data['title'] 	= "UPDATE TOPIC";   
			  
		  
 		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.$this->class.'/index');
		 
		$id = array('id' => ($this->url[2]));
		 

		if (isset($_POST['submit']))
		{

			empty($_POST['topic']) ?		$errors[] = 'You forgot to enter topic.'	: '';
			empty($_POST['content']) ? 	$errors[] = 'You forgot to enter content.'	: '';
			 
		 
 
 
			
			$fields =  array(
				'topic'		=>$_POST['topic'],
				'content'	=>$_POST['content']
				
				);
			$field = array(
							'topic'		=>$_POST['topic'],
							'content'	=>$_POST['content'],
							'user_id'   =>$_SESSION[ID],
							'date_added'=>date(DATE_FORMAT),
							'publish' 	=> 1

							);


			if (empty($errors)) { 	

				$where = $id;

				if( $this->pdo->updateData($this->table,$field,$where)){
  
					$this->obj->redirect($this->default);
				
				}

			}else{

      			$data['errors'] = $errors;
    
    		}


		}else{
			

				$fields =  array(
				 
				'topic'		=> $this->pdo->selectData($this->table,'topic',$id),
				'content'	=> $this->pdo->selectData($this->table,'content',$id)
				 
				);

				// print_r($fields);
		}



		$this->view('administrator/include/header',$data);
	 	$this->view('administrator/'.$this->class.'/form',$data,$fields);
	 	$this->view('administrator/include/footer');
 	 	 
 		 
		 
 	}


 	public function delete()
	{
		$data['title'] 	= "DELETE TOPIC";   	  
		  
 		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.$this->class.'/index');
		 
		  
				if( $this->pdo->deleteData($this->table,$this->url[2]) ){
  
 				$this->obj->redirect($this->default);

					 
				
				}else{
					 $this->obj->redirect($this->default);
				}
 	}
 

}
