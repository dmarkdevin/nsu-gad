<?php
 
class Manage extends Controller
{ 

	public function __construct(){
	 	 
		$this->pdo = $this->model('CRUD');
		$this->obj = $this->model('FUNCTIONS');
		$this->url = $this->obj->url();
		 

		isset($this->url[2]) ? $this->act = $this->url[2] :	$this->act = '';

	}
	
	public function index()
	{
		$data['title'] = "HOME";


		$this->view('main/header',$data);
		$this->view('main/index',$data);
		$this->view('main/footer');
	}  
	
  	public function news()
	{
		$data['title'] = "MANAGE NEWS";
		
		
		$this->view('main/header',$data);
		$this->view('manage/news',$data);
		$this->view('main/footer');
	}


 	public function events()
	{
		$data['title'] = "MANAGE EVENT";
		$data['result'] 	= $this->pdo->fetchData('tbl_users');  
		
		
		$this->view('main/header',$data);
		$this->view('manage/events',$data);
		$this->view('main/footer');
	}

	public function downloads()
	{
		$data['title'] = "DOWNLOADS";

		$this->view('main/header',$data);
		$this->view('main/downloads',$data);
		$this->view('main/footer');
	}
	
	 
	public function forum()
	{
		$data['title'] = "FORUM";

		$this->view('main/header',$data);
		$this->view('main/forum',$data);
		$this->view('main/footer');
	}
 
	public function manage()
	{
		$data['title'] = "MANAGE";

		$this->view('main/header',$data);
		 
		$this->view('main/footer');

	
	}

	 
	 



}

 