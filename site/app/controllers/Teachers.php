<?php


class Teachers extends Controller {
	
	public $class = "teachers";			/* to be change */
	public $table = "tbl_teachers";		/* to be change */
	
	public function __construct() {
		
		 
		$this->pdo = $this->model('CRUD');
		$this->obj = $this->model('FUNCTIONS');
		$this->url = $this->obj->url();
		
		$this->default = URL_ROOT.$this->class.'/index';

		
		
		$this->obj->is_loggedin();

	}
	 
	public function index()
	{
			 
		$data['title'] 	= 'MANAGE TEACHER';   
		$data['list'] 	= $this->pdo->viewData($this->table);  

		$this->view('administrator/include/header',$data);
 		$this->view('administrator/'.$this->class.'/index',$data);
 		$this->view('administrator/include/footer');
 		 		 
 	}

	public function register()
	{
			$data['title'] 	= 'CREATE TEACHERS';   
			  
		 
				if (isset($_POST['submit']))
				{

					empty($_POST['name']) ?		$errors[] = 'You forgot to enter the name'	: '';
					

			 
 
				$field = array(
				'name'		=>$_POST['name'], 
				'birthdate'	=>$_POST['birthdate'], 
				'email'		=>$_POST['email'], 
				
				'username'	=>$_POST['username'], 
				'password'	=>e($_POST['password']), 

				'CODE'		=>$_POST['CODE'], 
				'SEX'		=>$_POST['SEX'], 
				 
				 

							'user_id'   =>$_SESSION[ID],
							'date_added' =>date(DATE_FORMAT),
							'active' => 1
			 
			 
				);

 

			if (empty($errors)) { 	

				if($this->pdo->insertData($this->table,$field)){

					$this->obj->redirect($this->default);
				
				}else{
					echo "asdasd";
				}

			}else{

      			$data['errors'] = $errors;
    
    		}





								
			}


			$this->view('administrator/include/header',$data);
	 		$this->view('administrator/'.$this->class.'/form',$data);
	 		$this->view('administrator/include/footer');
 		
 	 	 
 		 
		 
 	}



 	public function update()
	{
			$data['title'] 	= "UPDATE TEACHERS";   
			  
		  
 		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.$this->class.'/index');
		 
		$id = array('id' => ($this->url[2]));
		 

		if (isset($_POST['submit']))
		{

			empty($_POST['name']) ?		$errors[] = 'You forgot to enter name.'	: '';
			  
		 
  
			 
			$field = array(
			 
				'name'		=>$_POST['name'], 
				'birthdate'	=>$_POST['birthdate'], 
				'email'		=>$_POST['email'], 
				
				'username'	=>$_POST['username'], 
				'password'	=>e($_POST['password']), 

				'CODE'		=>$_POST['CODE'], 
				'SEX'		=>$_POST['SEX'], 
				 

							'user_id'   =>$_SESSION[ID],
							'date_modified' =>date(DATE_FORMAT),
							'active' => 1
			 
				);

			 
			$fields  =  array(
				'name'		=>$_POST['name'], 

				'birthdate'	=>$_POST['birthdate'], 
				'email'		=>$_POST['email'], 
				
				'username'	=>$_POST['username'], 
				'password'	=>$_POST['password'], 

				'CODE'		=>$_POST['CODE'], 
				'SEX'		=>$_POST['SEX']
				 
			 			 		 
				 
				);


			 

			if (empty($errors)) { 	

				$where = $id;

				if( $this->pdo->updateData($this->table,$field,$where)){
  
					$this->obj->redirect($this->default);
				
				}

			}else{

      			$data['errors'] = $errors;
    
    		}


		}else{
			

				$fields =  array(
					 
					'name'		=> $this->pdo->selectData($this->table,'name',$id),
					'birthdate'		=> $this->pdo->selectData($this->table,'birthdate',$id),
					'email'		=> $this->pdo->selectData($this->table,'email',$id),
					
					'username'		=> $this->pdo->selectData($this->table,'username',$id),
					'password'		=> d($this->pdo->selectData($this->table,'password',$id)),
					 
					'CODE'		=> $this->pdo->selectData($this->table,'CODE',$id),
					'SEX'		=> $this->pdo->selectData($this->table,'SEX',$id)
					 
				 
				 
					 				 
				);

			 // print_r($fields);
		}



		$this->view('administrator/include/header',$data);
	 	$this->view('administrator/'.$this->class.'/form',$data,$fields);
	 	$this->view('administrator/include/footer');
 	 	 
 		 
		 
 	}


 	public function delete()
	{
		$data['title'] 	= "DELETE";   	  
		  
 		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.$this->class.'/index');
		 
		  
				if( $this->pdo->deleteData($this->table,$this->url[2]) ){
  
 				$this->obj->redirect($this->default);

					 
				
				}else{
					 $this->obj->redirect($this->default);
				}
 	}
 	public function logout()
	{
		$this->obj->logout();

	} //end of logout


 
 
}
