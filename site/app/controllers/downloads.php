<?php


class Downloads extends Controller {
	
	public $class = "downloads";			/* to be change */
	public $table = "tbl_downloads";		/* to be change */
	
	public function __construct() {
		
		 
		$this->pdo = $this->model('CRUD');
		$this->obj = $this->model('FUNCTIONS');
		$this->url = $this->obj->url();
		
		$this->default = URL_ROOT.$this->class.'/index';

		
		
		// $this->obj->is_loggedin();

	}
	 
	public function index()
	{
			 
		$data['title'] 	= 'MANAGE DOWNLOADS';   
		$data['list'] 	= $this->pdo->viewData($this->table);  

		$this->view('administrator/include/header',$data);
 		$this->view('administrator/'.$this->class.'/index',$data);
 		$this->view('administrator/include/footer');
 		 		 
 	}

 	public function download()
	{	 
		
	isset($this->url[2]) ?  '' : $this->obj->redirect($this->default);
	$this->view('administrator/'.$this->class.'/download');
 		
		 
	} // end of update




	public function register()
	{
			$data['title'] 	= 'CREATE DOWNLOADS';   
			  
		 
				if (isset($_POST['submit']))
				{

					empty($_POST['name']) ?		$errors[] = 'You forgot to enter the name'	: '';
					
				$directory = 'uploads/downloads';
			 
 
				$field = array(
			 
				'name'		=>$_POST['name'], 
				 

							'user_id'   =>$_SESSION[ID],
							'date_added' =>date(DATE_FORMAT),
							'publish' => 1
			 
				);

 

		 




			if (empty($errors)) { 	

//////////////////////////////////////////////////////////////

								if($_FILES['file']['name']){
									$file_info = pathinfo($_FILES['file']['name']);

									if($file_info['extension']=="pdf" or 
									$file_info['extension']=="docx" or 
									$file_info['extension']=="doc" or 
									$file_info['extension']=="DOCX" or 
									$file_info['extension']=="DOC" or 
									$file_info['extension']=="PDF" or 
									$file_info['extension']=="xls" or 
									$file_info['extension']=="XLS"){
									
										$fileName = $_FILES['file']['name'];
										$tmpName  = $_FILES['file']['tmp_name'];
										$fileSize = $_FILES['file']['size'];
										$fileType = $_FILES['file']['type'];

									 
						 				// if($this->pdo->countData('tbl_lesson',array('file'=>$fileName)) == 0) {
						 				if(!file_exists($directory.'/'.$fileName)) {
						 					 
						 					 if($fileSize<100000000){

						 					 	if (empty($errors)) { 	


						 					 		move_uploaded_file($tmpName,$directory."/" . $fileName);			
													$content = $directory."/" . $fileName;

												$field = array(
												'name'		=>strtoupper($_POST['name']),
												 
												// 'content'		=>$_POST['content'] ,
												'publish'	=>"1",
												'file'		=>$fileName,
												 
												'date_added'=> date(DATE_FORMAT),
												'user_id' 	=> $_SESSION[ID]
												);


													if($this->pdo->insertData($this->table,$field)){

														$this->obj->redirect($this->default);
													
													}




												}

						 					 }else{
						 
												$errors[] = 'file size reached the limit.';	
												$data['errors'] = $errors;

											}
						 				}else{
						 
						 					$errors[] = 'File Already Exists.';		 
											$data['errors'] = $errors;

											
										}

									}else{
									
										$errors[] = 'Invalid Files.';
										 
										$data['errors'] = $errors;

									}

								}else{	 


									$errors[] = 'No file selected.';
									 
									$data['errors'] = $errors;
								}





//////////////////////////////////////////////////////////////

 

			}else{

      			$data['errors'] = $errors;
    
    		}



								
			}


			$this->view('administrator/include/header',$data);
	 		$this->view('administrator/'.$this->class.'/form',$data);
	 		$this->view('administrator/include/footer');
 		
 	 	 
 		 
		 
 	}



 	public function update()
	{
			$data['title'] 	= "UPDATE DOWNLOADS";   
			  
		  
 		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.$this->class.'/index');
		 
		$id = array('id' => ($this->url[2]));
		 

		if (isset($_POST['submit']))
		{

			empty($_POST['name']) ?		$errors[] = 'You forgot to enter name.'	: '';
			  
		 
 			$directory = 'uploads/downloads';
			 
			 
			$field = array(
			 
				'name'		=>$_POST['name'], 
				 

							'user_id'   =>$_SESSION[ID],
							'date_added' =>date(DATE_FORMAT),
							'publish' => 1
			 
				);

			 
			$fields  =  array(
				'name'		=>$_POST['name'] 
			 			 
				 
				);


			 

			if (empty($errors)) { 	




//////////////////////////////////////////////////////////////

								if($_FILES['file']['name']){
									$file_info = pathinfo($_FILES['file']['name']);

									if($file_info['extension']=="pdf" or 
									$file_info['extension']=="docx" or 
									$file_info['extension']=="doc" or 
									$file_info['extension']=="DOCX" or 
									$file_info['extension']=="DOC" or 
									$file_info['extension']=="PDF" or 
									$file_info['extension']=="xls" or 
									$file_info['extension']=="XLS"){
									
										$fileName = $_FILES['file']['name'];
										$tmpName  = $_FILES['file']['tmp_name'];
										$fileSize = $_FILES['file']['size'];
										$fileType = $_FILES['file']['type'];

									 
						 				// if($this->pdo->countData('tbl_lesson',array('file'=>$fileName)) == 0) {
						 				if(!file_exists($directory.'/'.$fileName)) {
						 					 
						 					 if($fileSize<100000000){

						 					 	if (empty($errors)) { 	


						 					 		move_uploaded_file($tmpName,$directory."/" . $fileName);			
													$content = $directory."/" . $fileName;

												$field = array(
												'name'		=>strtoupper($_POST['name']),
												 
												// 'content'		=>$_POST['content'] ,
												'publish'	=>"1",
												'file'		=>$fileName,
												 
												'date_modified'=> date(DATE_FORMAT),
												'user_id' 	=> $_SESSION[ID]
												);

												$where = $id;

													if($this->pdo->updateData($this->table,$field,$where)){

														$this->obj->redirect($this->default);
													
													}




												}

						 					 }else{
						 
												$errors[] = 'file size reached the limit.';	
												$data['errors'] = $errors;

											}
						 				}else{
						 
						 					$errors[] = 'File Already Exists.';		 
											$data['errors'] = $errors;

											
										}

									}else{
									
										$errors[] = 'Invalid Files.';
										 
										$data['errors'] = $errors;

									}

								}else{	 
										$field = array(
												'name'		=>strtoupper($_POST['name']),
												 
												 
												'publish'	=>"1",
												 
												 
												'date_modified'=> date(DATE_FORMAT),
												'user_id' 	=> $_SESSION[ID]
												);
												


										$where = $id;

										if( $this->pdo->updateData($this->table,$field,$where)){
						  
											$this->obj->redirect($this->default);
										
										}

								}





//////////////////////////////////////////////////////////////

 

			
			}else{

      			$data['errors'] = $errors;
    
    		}


		}else{
			

				$fields =  array(
					 
					'name'		=> $this->pdo->selectData($this->table,'name',$id)
				 
					 				 
				);

			 // print_r($fields);
		}



		$this->view('administrator/include/header',$data);
	 	$this->view('administrator/'.$this->class.'/form',$data,$fields);
	 	$this->view('administrator/include/footer');
 	 	 
 		 
		 
 	}


 	public function delete()
	{
		$data['title'] 	= "DELETE";   	  
		  
 		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.$this->class.'/index');
		 
		  
				if( $this->pdo->deleteData($this->table,$this->url[2]) ){
  
 				$this->obj->redirect($this->default);

					 
				
				}else{
					 $this->obj->redirect($this->default);
				}
 	}
 
 
}
