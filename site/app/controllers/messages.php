<?php


class Messages extends Controller {
	
		
	public $class = "messages";			/* to be change */
	public $table = "tbl_messages";		/* to be change */

	public function __construct() {
		
		 
		$this->pdo = $this->model('CRUD');
		$this->obj = $this->model('FUNCTIONS');
		$this->url = $this->obj->url();

		$this->default = URL_ROOT.$this->class.'/index';

		
		
		$this->obj->is_loggedin();

	}
	 
	public function index()
	{

		$data['title'] 	= 'MANAGE MESSAGE';   
	 

		$data['list'] 	= $this->pdo->viewQUERY("SELECT * FROM tbl_messages WHERE receiver_id = ".$_SESSION[ID]."  AND receiver_type = '".$_SESSION[TYPE]."' ORDER BY date_added DESC");  
 



		$this->view('administrator/include/header',$data);
 		$this->view('administrator/'.$this->class.'/index',$data);
 		$this->view('administrator/include/footer');
 	}



public function read()
	{

		$data['title'] 	= 'READ MESSAGE';   
	 

	   
 		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.$this->class.'/index');
		 
		$id = array('id' => ($this->url[2]));


		$this->view('administrator/include/header',$data);
 		$this->view('administrator/'.$this->class.'/read',$data);
 		$this->view('administrator/include/footer');
 	}

 	public function register()
	{
			$data['title'] 	= 'CREATE NEWS';   
			
				if (isset($_POST['submit'])){

					 
					
					$directory = 'uploads/news';
			 	
			 		if (empty($errors)) { 	

//////////////////////////////////////////////////////////////
 
$field = array(
			  // 'subject' => $_POST['subject'],
			  'message' => $_POST['message'],
			  'sender_id' => $_SESSION[ID],
			  'sender_type' => $_SESSION[TYPE],
			  'receiver_id' => $_POST['receiver_id'],
			  'receiver_type' => $_POST['receiver_type'],
			 
			   
			     
 
 
			 
				'date_added' => date(DATE_FORMAT)

			);
		 
			if($this->pdo->insertData($this->table,$field)){
							$success[] = 'Succesfully Save';
							$data['success'] = $success;

					// $this->obj->redirect($this->default);
		 	}
	 



//////////////////////////////////////////////////////////////

 
 
			}else{

      			$data['errors'] = $errors;
    
    		} 


								
			}


			$this->view('administrator/include/header',$data);
	 		$this->view('administrator/'.$this->class.'/form',$data);
	 		$this->view('administrator/include/footer');
 		
 	 	 
 		 
		 
 	}
 
 

 	public function delete()
	{
		$data['title'] 	= "DELETE NEWS";   	  
		  
 		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.$this->class.'/index');
		 
		  
				if( $this->pdo->deleteData($this->table,$this->url[2]) ){
  
 				$this->obj->redirect($this->default);

					 
				
				}else{
					 $this->obj->redirect($this->default);
				}
 	}
 

}
