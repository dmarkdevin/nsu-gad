<?php


class News extends Controller {
	
		
	public $class = "news";			/* to be change */
	public $table = "tbl_news";		/* to be change */

	public function __construct() {
		
		 
		$this->pdo = $this->model('CRUD');
		$this->obj = $this->model('FUNCTIONS');
		$this->url = $this->obj->url();

		$this->default = URL_ROOT.$this->class.'/index';

		
		
		$this->obj->is_loggedin();

	}
	 
	public function index()
	{

		$data['title'] 	= 'MANAGE NEWS';   
		$data['list'] 	= $this->pdo->viewData($this->table);  

		$this->view('administrator/include/header',$data);
 		$this->view('administrator/'.$this->class.'/index',$data);
 		$this->view('administrator/include/footer');
 	}

 	public function register()
	{
			$data['title'] 	= 'CREATE NEWS';   
			
				if (isset($_POST['submit'])){

					empty($_POST['title']) ?		$errors[] = 'You forgot to enter the title'	: '';
					
					$directory = 'uploads/news';
			 	
			 		if (empty($errors)) { 	

//////////////////////////////////////////////////////////////

								if($_FILES['file']['name']){
									$file_info = pathinfo($_FILES['file']['name']);

									if($file_info['extension']=="jpeg" or 
									$file_info['extension']=="jpg" or 
									$file_info['extension']=="png" or 
									$file_info['extension']=="gif" or 
									$file_info['extension']=="JPEG" or 
									$file_info['extension']=="JPG" or 
									$file_info['extension']=="PNG" or 
									$file_info['extension']=="GIF"){
									
										$fileName = $_FILES['file']['name'];
										$tmpName  = $_FILES['file']['tmp_name'];
										$fileSize = $_FILES['file']['size'];
										$fileType = $_FILES['file']['type'];

									 
						 				 
						 				if(!file_exists($directory.'/'.$fileName)) {
						 					 
						 					if($fileSize<100000000){

						 					 	if (empty($errors)) { 	


						 					 		move_uploaded_file($tmpName,$directory."/" . $fileName);			
													$content = $directory."/" . $fileName;

													$field = array(
													'title'		=>strtoupper($_POST['title']),
													'content'	=>$_POST['content'] ,
													'publish'	=>1,
													'file'		=>$fileName,
													 
													'date_added'=> date(DATE_FORMAT),
													'user_id' 	=> $_SESSION[ID]
													
													 
													);


													if($this->pdo->insertData($this->table,$field)){
     													$this->obj->redirect($this->default);
												 	}
												}

						 					}else{
						 
												$errors[] = 'file size reached the limit.';	
												$data['errors'] = $errors;

											}
						 				}else{
						 
						 					$errors[] = 'File Already Exists.';		 
											$data['errors'] = $errors;

											
										}

									}else{
									
										$errors[] = 'Invalid Files.';
										 
										$data['errors'] = $errors;

									}

								}else{	 	
								
									$errors[] = 'No file selected.';
								
									$data['errors'] = $errors;
								}





//////////////////////////////////////////////////////////////

 
 
			}else{

      			$data['errors'] = $errors;
    
    		} 


								
			}


			$this->view('administrator/include/header',$data);
	 		$this->view('administrator/'.$this->class.'/form',$data);
	 		$this->view('administrator/include/footer');
 		
 	 	 
 		 
		 
 	}
 

 	public function update()
	{
			$data['title'] 	= "UPDATE NEWS";   
			  
		  
 		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.$this->class.'/index');
		 
		$id = array('id' => ($this->url[2]));
		 

		if (isset($_POST['submit']))
		{

			empty($_POST['title']) ?		$errors[] = 'You forgot to enter Account Name.'	: '';
			empty($_POST['content']) ? 	$errors[] = 'You forgot to enter Email Address.'	: '';
			 
		 
 			$directory = 'uploads/news';
 
			
			$fields =  array(
				'title'		=>$_POST['title'],
				'content'	=>$_POST['content']
				
				);
			$field = array(
							'title'		=>$_POST['title'],
							'content'	=>$_POST['content'],
							'user_id'   =>$_SESSION[ID],
							'date_added'=>date(DATE_FORMAT),
							'publish' 	=> 1

							);


			if (empty($errors)) { 	

//////////////////////////////////////////////////////////////
			 			
								if($_FILES['file']['name']){
									$file_info = pathinfo($_FILES['file']['name']);

									if($file_info['extension']=="jpeg" or 
									$file_info['extension']=="jpg" or 
									$file_info['extension']=="png" or 
									$file_info['extension']=="gif" or 
									$file_info['extension']=="JPEG" or 
									$file_info['extension']=="JPG" or 
									$file_info['extension']=="PNG" or 
									$file_info['extension']=="GIF"){
									
										$fileName = $_FILES['file']['name'];
										$tmpName  = $_FILES['file']['tmp_name'];
										$fileSize = $_FILES['file']['size'];
										$fileType = $_FILES['file']['type'];

									 
						 				 
						 				if(!file_exists($directory.'/'.$fileName)) {
						 					 
						 					if($fileSize<100000000){

						 					 	if (empty($errors)) { 	


						 					 		move_uploaded_file($tmpName,$directory."/" . $fileName);			
													$content = $directory."/" . $fileName;

													$field = array(
													'title'		=>strtoupper($_POST['title']),
													'content'	=>$_POST['content'] ,
													'publish'	=>1,
													'file'		=>$fileName,
													 
													'date_modified'=> date(DATE_FORMAT)
													
													 
													);

													$where = $id;


													if( $this->pdo->updateData($this->table,$field,$where)){
						  								$this->obj->redirect($this->default);
												 	}
												}

						 					}else{
						 
												$errors[] = 'file size reached the limit.';	
												$data['errors'] = $errors;

											}
						 				}else{
						 
						 					$errors[] = 'File Already Exists.';		 
											$data['errors'] = $errors;

											
										}

									}else{
									
										$errors[] = 'Invalid Files.';
										 
										$data['errors'] = $errors;

									}

								}else{	 	
										$field = array(
													'title'		=>strtoupper($_POST['title']),
													'content'	=>$_POST['content'] ,
													'publish'	=>1,
													 
													 
													'date_modified'=> date(DATE_FORMAT)
													
													 
													);
								
										$where = $id;

										if( $this->pdo->updateData($this->table,$field,$where)){
						  
											$this->obj->redirect($this->default);
										
										}

								}





//////////////////////////////////////////////////////////////



			
/////////////////////////////////////

			}else{

      			$data['errors'] = $errors;
    
    		}


		}else{
			

				$fields =  array(
				 
				'title'		=> $this->pdo->selectData($this->table,'title',$id),
				'content'	=> $this->pdo->selectData($this->table,'content',$id)
				 
				);

				// print_r($fields);
		}



		$this->view('administrator/include/header',$data);
	 	$this->view('administrator/'.$this->class.'/form',$data,$fields);
	 	$this->view('administrator/include/footer');
 	 	 
 		 
		 
 	}


 	public function delete()
	{
		$data['title'] 	= "DELETE NEWS";   	  
		  
 		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.$this->class.'/index');
		 
		  
				if( $this->pdo->deleteData($this->table,$this->url[2]) ){
  
 				$this->obj->redirect($this->default);

					 
				
				}else{
					 $this->obj->redirect($this->default);
				}
 	}
 

}
