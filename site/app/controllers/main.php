<?php
 
class Main extends Controller
{ 
public $class = "Main";			/* to be change */
	public function __construct(){
	 	 
		$this->pdo = $this->model('CRUD');
		$this->obj = $this->model('FUNCTIONS');
		$this->url = $this->obj->url();
		 

		isset($this->url[2]) ? $this->act = $this->url[2] :	$this->act = '';

		$this->default = URL_ROOT.$this->class.'/index';



	}
	public function test(){
		 
	$data['title'] = "dmarkdevin";

		$this->view('main/header',$data);
		$this->view('test',$data);
		$this->view('main/footer');

	}
	
	public function index()
	{
		$data['title'] = "HOME";

		
		$data['news']     = $this->pdo->viewQUERY('SELECT * FROM tbl_news LIMIT 300');



		
		$this->view('main/header',$data);
		$this->view('main/index',$data);
		$this->view('main/footer');
	}  
	
	public function gfps()
	{
		$data['title'] = "GFPS";

		

		
		$this->view('main/header',$data);
		$this->view('main/gfps',$data);
		$this->view('main/footer');
	}  
	


	public function inbox()
	{
		$data['title'] = "INBOX";

		

		
		$this->view('main/header',$data);
		$this->view('main/inbox',$data);
		$this->view('main/footer');
	}  
	


	public function statistics()
	{
		$data['title'] = "STATISTICS";

		

		
		$this->view('main/header',$data);
		$this->view('main/statistics',$data);
		$this->view('main/footer');
	}  
	
	
	public function news()
	{
		$data['title'] = "News";


		$this->view('main/header',$data);
		$this->view('main/index',$data);
		$this->view('main/footer');
	} 
	public function read_news()
	{
		$data['title'] = "Read News";

		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.'main/index');
		 
		$id = array('id' => ($this->url[2]));

		$fields =  array(
				 
				'title'		=> $this->pdo->selectData('tbl_news','title',$id),
				'content'	=> $this->pdo->selectData('tbl_news','content',$id),
				'file'	=> $this->pdo->selectData('tbl_news','file',$id),
				'date_added'	=> $this->pdo->selectData('tbl_news','date_added',$id)
				 
				);


		$this->view('main/header',$data);
		$this->view('main/read_news',$data,$fields);
		$this->view('main/footer');
	} 


 	public function events()
	{
		$data['title'] = "EVENTS";

		$this->view('main/calendar',$data);
		// $this->view('main/header',$data);
		// $this->view('main/events',$data);
		// $this->view('main/footer');
	}
	public function calendar()
	{
		$data['title'] = "CALENDAR";

		//$this->view('main/calendar',$data);
		 $this->view('main/header',$data);
		  $this->view('main/calendar_page',$data);
		 $this->view('main/footer');
	}
	public function downloads()
	{
		$data['title'] = "RESOURCES";


		$data['downloads']     = $this->pdo->viewQUERY('SELECT * FROM tbl_downloads  ORDER BY name ASC');

		 

		$this->view('main/header',$data);
		$this->view('main/resources',$data);
		$this->view('main/footer');
	}
	
	public function comment()
	{
		$data['title'] = "COMMENT";

		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.'main/index');
		 
		$id = array('id' => ($this->url[2]));

		$fields =  array(
				 'id'=>$this->url[2],
				'topic'		=> $this->pdo->selectData('tbl_forum','topic',$id),
				'content'	=> $this->pdo->selectData('tbl_forum','content',$id),
				'user_id'	=> $this->pdo->selectData('tbl_users','name',array('id'=>$this->pdo->selectData('tbl_forum','user_id',$id))),
				'date_added'	=> $this->pdo->selectData('tbl_forum','date_added',$id)
				 
				);

		$data['comment']     = $this->pdo->viewQUERY('SELECT * FROM tbl_comment WHERE forum_id= '.$this->url[2].' ');



		if (isset($_POST['submit']))
		{

			empty($_POST['comment']) ?		$errors[] = 'You forgot to enter Comment.'	: '';
			 
		 	

			if (empty($errors)) { 	

		 


			$field = array(
				'comment'		=>$_POST['comment'],
				'forum_id'		=>($this->url[2]),
				'user_id'		=>$_SESSION[ID],
				'type'			=>$_SESSION[TYPE],
				'date_added'		=> date(DATE_FORMAT)

				);


				if($this->pdo->insertData('tbl_comment',$field)){

					$this->obj->redirect(URL_ROOT.'main/comment/'.$this->url[2]);
				
				}

			}else{

      			$data['errors'] = $errors;
    
    		}


		} 





		$this->view('main/header',$data);
		$this->view('main/comment',$data,$fields);
		$this->view('main/footer');
	}
	
	public function forum()
	{
		$data['title'] = "FORUM";



		$this->view('main/header',$data);
		$this->view('main/forum',$data);
		$this->view('main/footer');
	}

	public function about()
	{
		$data['title'] = "ABOUT US";

		$this->view('main/header',$data);
		$this->view('main/about',$data);
		$this->view('main/footer');
	}  
	 
	 
	public function contact()
	{
		$data['title'] = "CONTACT US";

		$this->view('main/header',$data);
		$this->view('main/contact',$data);
		$this->view('main/footer');

	
	}
	public function manage()
	{
		$data['title'] = "MANAGE";

		$this->view('main/header',$data);
		 
		$this->view('main/footer');

	
	}



	public function message()
	{
		$data['title'] = "Message";

		 if (isset($_POST['submit'])){

					 
					
					$directory = 'uploads/news';
			 	
			 		if (empty($errors)) { 	

//////////////////////////////////////////////////////////////
 
$field = array(
			  // 'subject' => $_POST['subject'],
			  'message' => $_POST['message'],
			  'sender_id' => $_SESSION[ID],
			  'sender_type' => $_SESSION[TYPE],
			  'receiver_id' => $_POST['receiver_id'],
			  'receiver_type' => $_POST['receiver_type'],
			 
			   
			     
 
 
			 
				'date_added' => date(DATE_FORMAT)

			);
		 
			if($this->pdo->insertData('tbl_messages',$field)){
							$success[] = 'Succesfully Save';
							$data['success'] = $success;

					// $this->obj->redirect($this->default);
		 	}
	 



//////////////////////////////////////////////////////////////

 
 
			}else{

      			$data['errors'] = $errors;
    
    		} 


								
			}

		$this->view('main/header',$data);
		$this->view('main/message',$data);
		$this->view('main/footer');
	}
	

	 
	// public function login(){
	
	// $data['title']	= 'SIGN IN';
	
	 
	
	// if (isset($_POST['submit'])){

 // 		empty($_POST['username']) ? $errors[] = 'You forgot to enter Account Username.'	: '';
	// 	empty($_POST['password']) ? $errors[] = 'You forgot to enter Account Password.'	: '';
	 
	//  	if (empty($errors)) { 	
			
	// 		$table = 'tbl_users';

	// 		$params = array(
	// 			'username'=>$_POST['username'],
	// 			'password'=>MD5($_POST['password']),
				 
	// 			'active'=>1
	// 			);
 		 	

	// 		if($this->pdo->existData($table,$params)){
				
	// 			$_SESSION[ID] 	= $this->pdo->selectData($table,'id',$params);
	// 			$_SESSION[TYPE] = $this->pdo->selectData($table,'type',$params);
	// 			$_SESSION[NAME] = $this->pdo->selectData($table,'name',$params);
				
	// 			header('location: '.URL_ROOT.'page');

	// 		}else{
	// 			$errors[] = 'Username and Password did not match.';
	// 			$data['errors'] = $errors;
	// 		}


	// 	}else{
	// 		$data['errors'] = $errors;
	// 	}

	// 	}


	 
	// 	$this->view('main/header',$data);
	// 	$this->view('main/login',$data);
	// 	$this->view('main/footer');
	 
	// }  


	 
	// public function login(){
	
	// $data['title']	= 'SIGN IN';
	
	 
	

	// if (isset($_POST['submit'])){

 // 		empty($_POST['username']) ? $errors[] = 'You forgot to enter Account Username.'	: '';
	 
	//  	if (empty($errors)) { 	
			
	// 		$table = 'tbl_users';

	// 		$params = array(
	// 			'username'=>$_POST['username'],
				 
				 
	// 			'active'=>1
	// 			);
 		 	

	// 		if($this->pdo->existData($table,$params)){
				
	// 			 header('location: '.URL_ROOT.'main/verifyusername/'.$params['username'].'#signin');

	// 		}else{
	// 			$errors[] = "Sorry, Our System doesn't recognize that username.";
	// 			$data['errors'] = $errors;
	// 		}


	// 	}else{
	// 		$data['errors'] = $errors;
	// 	}

	// 	} 


	 
		
	// 	$this->view('main/header',$data);
	// 	$this->view('main/login/username',$data);
	// 	$this->view('main/footer');
	 
	// }  

	// public function verifyusername(){
	
	// $data['title']	= 'SIGN IN';

	// 		$table = 'tbl_users';

	
	// isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.'main/login');

	// ($this->pdo->existData($table,array('username'=>$this->url[2]))) ? '' : $this->obj->redirect(URL_ROOT.'main/login');
	
	// $data['name'] = strtoupper($this->pdo->selectData($table,'name',array('username'=>$this->url[2])));
	// $data['username'] = $this->pdo->selectData($table,'username',array('username'=>$this->url[2]));
	

	// $username=$this->url[2];

 

	// if (isset($_POST['submit'])){

 // 		empty($_POST['password']) ? $errors[] = 'Please enter your password.'	: '';
	 
	//  	if (empty($errors)) { 	
			
	// 		$params = array(
	// 			'username'=>$username,
	// 			'password'=>md5($_POST['password']) 
	// 			);
 		 	

	// 		if($this->pdo->existData($table,$params)){
				
	// 			$_SESSION[ID] 	= $this->pdo->selectData($table,'id',$params);
	// 			$_SESSION[TYPE] = $this->pdo->selectData($table,'type',$params);
	// 			$_SESSION[NAME] = $this->pdo->selectData($table,'name',$params);
				
	// 			header('Refresh: 0 url='.URL_ROOT.'page');


	// 		}else{
	// 			$errors[] = "Wrong password. Try again.";
	// 			$data['errors'] = $errors;

	// 			header('Refresh: 2; url='.URL_ROOT.'main/login#signin');

	// 		}


	// 	}else{
	// 		$data['errors'] = $errors;
	// 	}

	// 	} 


	 
		
	// 	$this->view('main/header',$data);
	// 	$this->view('main/login/password',$data);
	// 	$this->view('main/footer');
	 
	// }  





	public function login(){
	
	$data['title']	= 'SIGN IN';
	
	 
	

	if (isset($_POST['submit'])){

 		empty($_POST['username']) ? $errors[] = 'You forgot to enter Account Username.'	: '';
	 
	 	if (empty($errors)) { 	
			

	 		if(isset($_POST['option'])){

				switch($_POST['option']){
					case 'student':
					$table = 'tbl_students';
					$type = 'student';

					break;

					case 'teacher':
					$table = 'tbl_teachers';
					$type = 'teacher';

					break;
					
					case 'admin':
					$table = 'tbl_users';
					$type = 'admin';

					break;

					default:
					$table = 'tbl_users';
					$type = 'admin';
					break;

				}
			}


			 
			$password = $_POST['password'];

			$params = array('username'=>username($_POST['username']),'password'=>e($password));
 		 	
			 

			if($this->pdo->existData($table,$params)){


				$_SESSION[ID] 	= $this->pdo->selectData($table,'id',$params);
				$_SESSION[TYPE] = $type;
				$_SESSION[NAME] = $this->pdo->selectData($table,'name',$params);
				
				 
				// $this->obj->redirect(URL_ROOT.'page');
				// $this->obj->redirect(URL_ROOT.'main/profile');

				$success[] = "Login Successfull";
				 $data['success'] = $success;
					if($_POST['option'] == "admin" ){
						$profile = URL_ROOT.'page';
					}else{
						$profile = URL_ROOT.'main/about';
				
						// $profile = URL_ROOT.'main/profile';

					}
				header('Refresh: 3; url='.$profile);
				 

			}else{
				$errors[] = "Username and Password did not match. Try again.";
				$data['errors'] = $errors;


// print_r($_POST);
				// $this->obj->redirect(URL_ROOT.'main/login');
			}


		}else{
			$data['errors'] = $errors;
		}

		} 


	 
		
		$this->view('main/header',$data);
		$this->view('main/login',$data);
		$this->view('main/footer');
	 
	}  















public function change_password()
	{
		
		$this->obj->is_loggedin();
		$data['title']	= 'Change Password';	   
 
 		$id = array('id' => $_SESSION[ID]);


		if (isset($_POST['submit']))
		{

			empty($_POST['current_password']) ?		$errors[] = 'You forgot to enter Current Password.'	: '';
			empty($_POST['new_password']) ?		$errors[] = 'You forgot to enter New Password.'	: '';
			empty($_POST['confirm_password']) ?		$errors[] = 'You forgot to enter Confirm Password.'	: '';

				if($_SESSION[TYPE] == "student"){

					$table = 'tbl_students';

				}else if($_SESSION[TYPE] == "teacher"){
					$table = 'tbl_teachers';

				}else if($_SESSION[TYPE] == "admin"){

					$table = 'tbl_users';
				}


 
			$current_password = $this->pdo->selectData($table,'password',$id);
		


			// $currentpassword = MD5(SHA1($_POST['current_password']));
			// $new_password = MD5(SHA1($_POST['new_password']));
		 	$currentpassword = (($_POST['current_password']));
			$new_password = (($_POST['new_password']));
		 
				$fields =  array(
					'current_password'	=> '',
					'new_password'		=> '',
					'confirm_password'	=> ''
				 
				);
			 
				$field  =  array('password'	=>$new_password);

				if (empty($errors)) { 	

					if($current_password != $currentpassword){
						$errors[] = 'You Input Wrong Current Password.' ;
						$data['errors'] = $errors;

					}else if($current_password == $new_password){
						$errors[] = 'Current Password and New Password match.';
						$data['errors'] = $errors;

					}else if($_POST['new_password'] != $_POST['confirm_password']){
						$errors[] = 'New Password and Confirm Password did not match.';
						$data['errors'] = $errors;

					}else{
						$where = $id;

						if( $this->pdo->updateData($table,$field,$where)){
							$data['success'] = 'Change Password Successfully';
							 
						}
					}

					 
				}else{

					$data['errors'] = $errors;

				}
		}else{
			
				$fields =  array(
					'current_password'	=> '',
					'new_password'		=> '',
					'confirm_password'	=> ''
				 
				);
		}

 


		
		$this->view('main/header',$data);
		$this->view('main/change_password',$data);
		$this->view('main/footer');
		
	} // end of change password











public function profile(){

	$data['title']	= 'Profile';
	
	$this->view('main/header',$data);
	$this->view('main/profile',$data);
		 
		$this->view('main/footer');
}

 	public function delete()
	{
		$data['title'] 	= "DELETE NEWS";   	  
		  
 		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.$this->class.'/index');
		 
		  
				if( $this->pdo->deleteData($this->table,$this->url[2]) ){
  
 				$this->obj->redirect($this->default);

					 
				
				}else{
					 $this->obj->redirect($this->default);
				}
 	}
 

}

 