<?php


class Students extends Controller {
	
	public $class = "students";			/* to be change */
	public $table = "tbl_students";		/* to be change */
	
	public function __construct() {
		
		 
		$this->pdo = $this->model('CRUD');
		$this->obj = $this->model('FUNCTIONS');
		$this->url = $this->obj->url();
		
		$this->default = URL_ROOT.$this->class.'/index';

		
		
		$this->obj->is_loggedin();

	}
	 
	public function index()
	{
			 
		$data['title'] 	= 'MANAGE STUDENT';   
		$data['list'] 	= $this->pdo->viewDATA($this->table);  

		$this->view('administrator/include/header',$data);
 		$this->view('administrator/'.$this->class.'/index',$data);
 		$this->view('administrator/include/footer');
 		 		 
 	}

	public function register()
	{
			$data['title'] 	= 'CREATE STUDENTS';   
			  
		 
				if (isset($_POST['submit']))
				{

					empty($_POST['name']) ?		$errors[] = 'You forgot to enter the name'	: '';
					

			 
 
				$field = array(
			 
				'name'		=>$_POST['name'], 
				'birthdate'	=>$_POST['birthdate'], 
				'email'		=>$_POST['email'], 
				
				'username'	=>$_POST['username'], 
				'password'	=>e($_POST['password']), 

				'CODE'		=>$_POST['CODE'], 
				'SEX'		=>$_POST['SEX'], 
				'CRSCD'		=>$_POST['CRSCD'], 
				'YEAR'		=>$_POST['YEAR'], 
				'SECCD'		=>$_POST['SECCD'],
				'COLLEGE'		=>$_POST['COLLEGE'],
				 

							'user_id'   =>$_SESSION[ID],
							'date_added' =>date(DATE_FORMAT),
							'active' => 1
			 
				);
 

			if (empty($errors)) { 	

				if($this->pdo->insertData($this->table,$field)){

					$this->obj->redirect($this->default);
				
				}else{
					echo "asdasd";
				}

			}else{

      			$data['errors'] = $errors;
    
    		}





								
			}


/////////////////////////////////

if(isset($_POST['import'])){
					 
						if($_FILES['file']['error']==0){
							$name = basename($_FILES['file']['name']);
							$size = $_FILES['file']['size'];
							$type = $_FILES['file']['type'];
							$ext = substr($name,strrpos($name,".")+1);
										
							if(($size/1024/1024)<1){
								if($type=="application/vnd.ms-excel"){
									if ( isset( $_FILES['file'] ) ){
									
									  $csv_file = $_FILES['file']['tmp_name'];

									  if ( ! is_file( $csv_file ) )
									  exit('File not found.');
									  $sql = '';

									 if (($handle = fopen( $csv_file, "r")) !== FALSE)
									 {
									      while (($datax = fgetcsv($handle, 1000, ",")) !== FALSE)
									      {
										  if($this->pdo->countData($this->table,array('name'=>$datax[2]) ) == 0){
  
											$fn= ($datax[1]);
											$ln= ($datax[2]);  
											$yl= ($datax[3]);
											$e= ($datax[4]);

											$field = array(
												'name'=>$datax[2]
												);

											$this->pdo->insertData($this->table,$field);

										}else{
											 // print_r($datax); echo 'update';
											// $this->pdo->updateData('tbl_students',array('year_level'=>$datax[3]),array('id_number'=>$datax[0]));
										}
								      }
									  $this->obj->redirect($this->default);
								     fclose($handle);
				 				 }

				  
								}
								
								
								
							}else{
								$errors[] = ("Invalid file");
								$data['errors'] = $errors;
							}
						}else{
							$errors[] = ("You reached file limit 1 mb");
							$data['errors'] = $errors;
						}
					}else{
						$errors[] = ("File upload error");
						$data['errors'] = $errors;
					}
				}	
/////////////////////////////////			


			$this->view('administrator/include/header',$data);
	 		$this->view('administrator/'.$this->class.'/form',$data);
	 		$this->view('administrator/include/footer');
 		
 	 	 
 		 
		 
 	}



 	public function update()
	{
			$data['title'] 	= "UPDATE STUDENTS";   
			  
		  
 		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.$this->class.'/index');
		 
		$id = array('id' => ($this->url[2]));
		 

		if (isset($_POST['submit']))
		{

			empty($_POST['name']) ?		$errors[] = 'You forgot to enter name.'	: '';
			  
		 
  
			 
			$field = array(
			 
				'name'		=>$_POST['name'], 
				'birthdate'	=>$_POST['birthdate'], 
				'email'		=>$_POST['email'], 
				
				'username'	=>$_POST['username'], 
				'password'	=>e($_POST['password']), 

				'CODE'		=>$_POST['CODE'], 
				'SEX'		=>$_POST['SEX'], 
				'CRSCD'		=>$_POST['CRSCD'], 
				'YEAR'		=>$_POST['YEAR'], 
				'SECCD'		=>$_POST['SECCD'],
				'COLLEGE'		=>$_POST['COLLEGE'],
				 

							'user_id'   =>$_SESSION[ID],
							'date_modified' =>date(DATE_FORMAT),
							'active' => 1
			 
				);

			 
			$fields  =  array(
				'name'		=>$_POST['name'], 

				'birthdate'	=>$_POST['birthdate'], 
				'email'		=>$_POST['email'], 
				
				'username'	=>$_POST['username'], 
				'password'	=>d($_POST['password']), 

				'CODE'		=>$_POST['CODE'], 
				'SEX'		=>$_POST['SEX'], 
				'CRSCD'		=>$_POST['CRSCD'], 
				'YEAR'		=>$_POST['YEAR'], 
				'SECCD'		=>$_POST['SECCD'],
				'COLLEGE'		=>$_POST['COLLEGE'],
				
			 			 
				 
				);

	 

			if (empty($errors)) { 	

				$where = $id;

				if( $this->pdo->updateData($this->table,$field,$where)){
  
					$this->obj->redirect($this->default);
				
				}

			}else{

      			$data['errors'] = $errors;
    
    		}


		}else{
			
	
				
				$fields =  array(
					 
					'name'		=> $this->pdo->selectData($this->table,'name',$id),
					'birthdate'		=> $this->pdo->selectData($this->table,'birthdate',$id),
					'email'		=> $this->pdo->selectData($this->table,'email',$id),
					
					'username'		=> $this->pdo->selectData($this->table,'username',$id),
					'password'		=> d($this->pdo->selectData($this->table,'password',$id)),
					 
					'CODE'		=> $this->pdo->selectData($this->table,'CODE',$id),
					'SEX'		=> $this->pdo->selectData($this->table,'SEX',$id),
					'CRSCD'		=> $this->pdo->selectData($this->table,'CRSCD',$id),
					'YEAR'		=> $this->pdo->selectData($this->table,'YEAR',$id),
					'SECCD'		=> $this->pdo->selectData($this->table,'SECCD',$id),
				 'COLLEGE'		=> $this->pdo->selectData($this->table,'COLLEGE',$id)
				 
					 				 
				);
 
			 //print_r($fieldx);
		}

 

		$this->view('administrator/include/header',$data);
	 	$this->view('administrator/'.$this->class.'/form',$data,$fields);
	 	$this->view('administrator/include/footer');
 	 	 
 		 
		 
 	}





	public function import()
	{
			$data['title'] 	= 'IMPORT STUDENTS RECORD';   
			  
		 
				if (isset($_POST['submit']))
				{

					empty($_POST['name']) ?		$errors[] = 'You forgot to enter the name'	: '';
					

			 
 
				$field = array(
			 
				'name'		=>$_POST['name'], 
				  

							'user_id'   =>$_SESSION[ID],
							'date_added' =>date(DATE_FORMAT),
							'active' => 1
			 
				);

 

			if (empty($errors)) { 	

				if($this->pdo->insertData($this->table,$field)){

					$this->obj->redirect($this->default);
				
				}else{
					echo "asdasd";
				}

			}else{

      			$data['errors'] = $errors;
    
    		}





								
			}


/////////////////////////////////

if(isset($_POST['import'])){
					 
						if($_FILES['file']['error']==0){
							$name = basename($_FILES['file']['name']);
							$size = $_FILES['file']['size'];
							$type = $_FILES['file']['type'];
							$ext = substr($name,strrpos($name,".")+1);
										
							if(($size/1024/1024)<1){
								if($type=="application/vnd.ms-excel"){
									if ( isset( $_FILES['file'] ) ){
									
									  $csv_file = $_FILES['file']['tmp_name'];

									  if ( ! is_file( $csv_file ) )
									  exit('File not found.');
									  $sql = '';

									 if (($handle = fopen( $csv_file, "r")) !== FALSE)
									 {
									      while (($datax = fgetcsv($handle, 1000, ",")) !== FALSE)
									      {

									      	$variable = $this->pdo->countData($this->table,array('CODE'=>$datax[1]) );
											 
											 

										  if( $variable == 0    ){
    
											   
											$field = array(
												'CODE'=>($datax[1]),
												'name'=>($datax[2]),
												'SEX'=>($datax[3]),
												'CRSCD'=>($datax[4]),
												'YEAR'=>($datax[5]),
												'SECCD'=>($datax[6]),
'COLLEGE'=>($_POST['COLLEGE']),

												'username'=>username($datax[1]),
												'password'=>e(password())

												);

											$this->pdo->insertData($this->table,$field);

											 // print_r($datax); echo 'update';
											// $this->pdo->updateData('tbl_students',array('year_level'=>$datax[3]),array('id_number'=>$datax[0]));
										}else{
											$field = array(
												'CODE'=>($datax[1]),
												'name'=>($datax[2]),
												'SEX'=>($datax[3]),
												'CRSCD'=>($datax[4]),
												'YEAR'=>($datax[5]),
												'SECCD'=>($datax[6]),
												'COLLEGE'=>($_POST['COLLEGE'])

												);

											$this->pdo->updateData($this->table,$field,array('CODE'=>$datax[1]) );

										}
								      }
									  $this->obj->redirect($this->default);
								     fclose($handle);
				 				 }

				  
										}else{
								}
								
								
								
							}else{
								$errors[] = ("Invalid file");
								$data['errors'] = $errors;
							}
						}else{
							$errors[] = ("You reached file limit 1 mb");
							$data['errors'] = $errors;
						}
					}else{
						$errors[] = ("File upload error");
						$data['errors'] = $errors;
					}
				}	
/////////////////////////////////			


			$this->view('administrator/include/header',$data);
	 		$this->view('administrator/'.$this->class.'/import',$data);
	 		$this->view('administrator/include/footer');
 		
 	 	 
 		 
		 
 	}








 	public function delete()
	{
		$data['title'] 	= "DELETE";   	  
		  
 		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.$this->class.'/index');
		 
		  
				if( $this->pdo->deleteData($this->table,$this->url[2]) ){
  
 				$this->obj->redirect($this->default);

					 
				
				}else{
					 $this->obj->redirect($this->default);
				}
 	}
 	public function logout()
	{
		$this->obj->logout();

	} //end of logout


public function x(){



	try{
			$q = $this->pdo->conn->prepare("DELETE FROM ".$this->table);
			$check = $q->execute();
			if($check){
				$this->obj->redirect($this->default);
			}else{
				return false; 
			}
		}catch(PDOException $e){
			die($e->getMessage());
		}


}

public function y(){
	$rs = $this->pdo->conn->query('SELECT * FROM '.$this->table.' LIMIT 0');
	for ($i = 0; $i < $rs->columnCount(); $i++) {
	    $col = $rs->getColumnMeta($i);
	    $columns[] = $col['name'];
	}
	echo "<pre>";
	print_r($columns);
	echo "</pre>";
	
} 
 
}
