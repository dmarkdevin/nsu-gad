<?php


class Page extends Controller {
	
	public $class = "";			/* to be change */
	public $table = "";		/* to be change */
	
	public function __construct() {
		
		 
		$this->pdo = $this->model('CRUD');
		$this->obj = $this->model('FUNCTIONS');
		$this->url = $this->obj->url();

		 



	}
	 
	public function index()
	{ 
		$data['title'] 	= 'DASHBOARD';   
 		 

		$this->view('administrator/include/header',$data);
 		$this->view('administrator/dashboard',$data);
 		$this->view('administrator/include/footer');
 		
 		 
 	}











public function change_password()
	{
		
		$this->obj->is_loggedin();
		$data['title']	= 'Change Password';	   
 
 		$id = array('id' => $_SESSION[ID]);


		if (isset($_POST['submit']))
		{

			empty($_POST['current_password']) ?		$errors[] = 'You forgot to enter Current Password.'	: '';
			empty($_POST['new_password']) ?		$errors[] = 'You forgot to enter New Password.'	: '';
			empty($_POST['confirm_password']) ?		$errors[] = 'You forgot to enter Confirm Password.'	: '';

				if($_SESSION[TYPE] == "student"){

					$table = 'tbl_students';

				}else if($_SESSION[TYPE] == "teacher"){
					$table = 'tbl_teachers';

				}else if($_SESSION[TYPE] == "admin"){

					$table = 'tbl_users';
				}


 
			$current_password = $this->pdo->selectData($table,'password',$id);
		


			// $currentpassword = MD5(SHA1($_POST['current_password']));
			// $new_password = MD5(SHA1($_POST['new_password']));
		 	$currentpassword = (($_POST['current_password']));
			$new_password = (($_POST['new_password']));
		 
				$fields =  array(
					'current_password'	=> '',
					'new_password'		=> '',
					'confirm_password'	=> ''
				 
				);
			 
				$field  =  array('password'	=>e($new_password));

				if (empty($errors)) { 	

					if($current_password != e($currentpassword)) {
						$errors[] = 'You Input Wrong Current Password.' ;
						$data['errors'] = $errors;

					}else if($current_password == e($new_password)){
						$errors[] = 'Current Password and New Password match.';
						$data['errors'] = $errors;

					}else if($_POST['new_password'] != $_POST['confirm_password']){
						$errors[] = 'New Password and Confirm Password did not match.';
						$data['errors'] = $errors;

					}else{
						$where = $id;

						if( $this->pdo->updateData($table,$field,$where)){
							$data['success'] = 'Change Password Successfully';
							 
						}
					}

					 
				}else{

					$data['errors'] = $errors;

				}
		}else{
			
				$fields =  array(
					'current_password'	=> '',
					'new_password'		=> '',
					'confirm_password'	=> ''
				 
				);
		}

 


		$this->view('administrator/include/header',$data);
 		$this->view('administrator/change_password',$data);
 		$this->view('administrator/include/footer');
		
	} // end of change password









 	public function logout()
	{
 		session_destroy();
 		$this->obj->redirect(URL_ROOT."main/login");
 	}
 

}
