<?php


class Events extends Controller {
	
		
	public $class = "events";			/* to be change */
	public $table = "tbl_calendar";		/* to be change */

	public function __construct() {
		
		 
		$this->pdo = $this->model('CRUD');
		$this->obj = $this->model('FUNCTIONS');
		$this->url = $this->obj->url();

		$this->default = URL_ROOT.$this->class.'/index';

		
		
		$this->obj->is_loggedin();

	}
	 
	public function index()
	{

		 
		 
		$data['title'] 	= 'MANAGE EVENT';   
		$data['list'] 	= $this->pdo->viewData($this->table);  

		$this->view('administrator/include/header',$data);
 		$this->view('administrator/'.$this->class.'/index',$data);
 		$this->view('administrator/include/footer');
 		
 	 	 
 		

		 
 	}

 	public function register()
	{
			$data['title'] 	= 'CREATE EVENT';   
			  
		 
				if (isset($_POST['submit']))
				{

					empty($_POST['title']) ?		$errors[] = 'You forgot to enter the event'	: '';
					

			 
 
			$field = array(
			 
				'title'		=>$_POST['title'], 
				'start'		=>$_POST['start']." h:i:s",  
				'end'		=>$_POST['end']." h:i:s", 

							'user_id'   =>$_SESSION[ID],
							'date_added' =>date(DATE_FORMAT),
							'publish' => 1
			 
				);

 

			if (empty($errors)) { 	

				if($this->pdo->insertData($this->table,$field)){

					$this->obj->redirect($this->default);
				
				}else{
					echo "asdasd";
				}

			}else{

      			$data['errors'] = $errors;
    
    		}





								
			}


			$this->view('administrator/include/header',$data);
	 		$this->view('administrator/'.$this->class.'/form',$data);
	 		$this->view('administrator/include/footer');
 		
 	 	 
 		 
		 
 	}
 

 	public function update()
	{
			$data['title'] 	= "UPDATE EVENT";   
			  
		  
 		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.$this->class.'/index');
		 
		$id = array('id' => ($this->url[2]));
		 

		if (isset($_POST['submit']))
		{

			empty($_POST['title']) ?		$errors[] = 'You forgot to enter event.'	: '';
			  
		 
  
			 
			$field  =  array(
				'title'		=>$_POST['title'], 
				'start'		=>$_POST['start']." h:i:s", 
				'end'		=>$_POST['end']." h:i:s",
			 				
			 				'user_id'   =>$_SESSION[ID], 
							'publish' => 1

				 
				);
			 
			$fields  =  array(
				'title'		=>$_POST['title'], 
				'start'		=>$_POST['start'], 
				'end'		=>$_POST['end']
			 				
			 			 
				 
				);


			 

			if (empty($errors)) { 	

				$where = $id;

				if( $this->pdo->updateData($this->table,$field,$where)){
  
					$this->obj->redirect($this->default);
				
				}

			}else{

      			$data['errors'] = $errors;
    
    		}


		}else{
			

				$fields =  array(
					 
					'title'		=> $this->pdo->selectData($this->table,'title',$id),
					'start'	=> $this->obj->calendardate($this->pdo->selectData($this->table,'start',$id)),
					'end'	=> $this->obj->calendardate($this->pdo->selectData($this->table,'end',$id))	
					 				 
				);

			 // print_r($fields);
		}



		$this->view('administrator/include/header',$data);
	 	$this->view('administrator/'.$this->class.'/form',$data,$fields);
	 	$this->view('administrator/include/footer');
 	 	 
 		 
		 
 	}


 	public function delete()
	{
		$data['title'] 	= "DELETE TOPIC";   	  
		  
 		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.$this->class.'/index');
		 
		  
				if( $this->pdo->deleteData($this->table,$this->url[2]) ){
  
 				$this->obj->redirect($this->default);

					 
				
				}else{
					 $this->obj->redirect($this->default);
				}
 	}
 

	public function x(){



		try{
				$q = $this->pdo->conn->prepare("DELETE FROM ".$this->table);
				$check = $q->execute();
				if($check){
					$this->obj->redirect($this->default);
				}else{
					return false; 
				}
			}catch(PDOException $e){
				die($e->getMessage());
			}


	}

}
