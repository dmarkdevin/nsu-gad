<?php
 
class Users extends Controller
{
	// declaration of important variables 
	var $pdo, $data, $fields, $obj, $url, $act, $errors;
	
	public function __construct(){
	 	 
		$this->pdo = $this->model('CRUD');      
		$this->obj = $this->model('FUNCTIONS');  
		$this->url = $this->obj->url();
		
		// array variable 
		$data 	=  array();
		$errors =  array();
		$fields =  array();
		
		// default class table used of this classs
		$this->table = 'tbl_users';

	
	}
	
	public function index()
	{

		$this->active();
		
	} // end of index

	public function modal()
	{
		
		$this->obj->is_loggedin();
		$data['title']	= 'Modal';	   

	 
		$this->view('include/header',$data);
		$this->view('users/modal',$data);
		$this->view('include/footer');
		
	} // end of modal


	public function active()
	{
		
		$this->obj->is_loggedin();
		$data['title']	= 'Active Users';	   

		$params = array('active' => 1);
		$data['result'] = $this->pdo->fetchtheData( $this->table, $params );

		$this->view('include/header',$data);
		$this->view('users/users',$data);
		$this->view('include/footer');
		
	} // end of active

	public function inactive()
	{
		
		$this->obj->is_loggedin();
		$data['title']	= 'Inactive Users';	   

		$params = array('active' => 0);
		$data['result'] = $this->pdo->fetchtheData( $this->table, $params );
	
		$this->view('include/header',$data);
		$this->view('users/users',$data);
		$this->view('include/footer');
		
	} // end of inactive
	
	 
	public function registration()
	{
		
		$this->obj->is_loggedin();
		$data['title']	= 'Users Registration';	   
	
		if (isset($_POST['submit']))
		{

			empty($_POST['name']) ?		$errors[] = 'You forgot to enter Account Name.'	: '';
			empty($_POST['email']) ? 	$errors[] = 'You forgot to enter Account Email Address.'	: '';
			empty($_POST['username']) ? $errors[] = 'You forgot to enter Account Username.'	: '';
			empty($_POST['type']) ?   	$errors[] = 'You forgot to enter Account Type.'	: '';
			empty($_POST['password']) ? 		$errors[] = 'You forgot to enter Account Password.'	: '';
			empty($_POST['confirm_password']) ? $errors[] = 'You forgot to enter Account Confirm Password.'	: '';
			

			$new_password = MD5(SHA1($_POST['password']));

			if(empty($_POST['email']) == false){

				filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ? '' : $errors[] = 'You Input Invalid Email Address.';

			}

			if((empty($_POST['password']) AND empty($_POST['confirm_password'])) == false){

				$_POST['password'] == $_POST['confirm_password']  ? '' : $errors[] = 'Password and Confirm Password did not match.';

			}
			
			$this->pdo->countData($this->table,array('username'=>$_POST['username'])) == true ? $errors[] = 'Username is already used.'	: '';
			$this->pdo->countData($this->table,array('email'=>$_POST['email'])) 	  == true ? $errors[] = 'Email Address is already used.'	: '';

			
			$fields =  array(
				'name'		=>$_POST['name'],
				'username'	=>$_POST['username'],
				'password'	=>'',
				'confirm_password'	=>'',
				'email'		=>$_POST['email'],
				'type'		=>$_POST['type'],
				'active'	=>$_POST['active']
				);
			$field = array(
				'name'		=>$_POST['name'],
				'username'	=>$_POST['username'],
				'password'	=>MD5(SHA1($_POST['password'])),
				'email'		=>$_POST['email'],
				'type'		=>$_POST['type'],
				'active'	=>$_POST['active'],
				'date_added'=> date(DATE_FORMAT),
				'user_id' 	=> $_SESSION[ID]
				);

			if (empty($errors)) { 	

				if($this->pdo->insertData($this->table,$field)){

					$this->obj->redirect(URL_ROOT.'users/index');
				
				}

			}else{

      			$data['errors'] = $errors;
    
    		}


		}else{
			$fields =  array(
				'name'		=>'',
				'username'	=>'',
				'password'	=>'',
				'confirm_password'	=>'',
				'email'		=>'',
				'type'		=>''
				);
		}


		$this->view('include/header',$data);
		$this->view('users/form',$data, $fields);
		$this->view('include/footer');
		
	} // end of registration
	

	public function update()
	{
	 
		$this->obj->is_loggedin();
		$data['title']	= 'Users Update';	   
	 

		isset($this->url[2]) ?  '' : $this->obj->redirect(URL_ROOT.'users/index');
		$this->pdo->existData('tbl_users',array('id'=>$this->obj->decrypt($this->url[2]))) == true ? '' : $this->obj->redirect(URL_ROOT.'users/index');

		$id = array('id' => $this->obj->decrypt($this->url[2]));
		 

		if (isset($_POST['submit']))
		{

			empty($_POST['name']) ?		$errors[] = 'You forgot to enter Account Name.'	: '';
			empty($_POST['email']) ? 	$errors[] = 'You forgot to enter Email Address.'	: '';
			empty($_POST['username']) ? $errors[] = 'You forgot to enter Account Username.'	: '';
			empty($_POST['type']) ?   	$errors[] = 'You forgot to enter Account Type.'	: '';
			empty($_POST['password']) ? 		$errors[] = 'You forgot to enter Account Password.'	: '';
			empty($_POST['confirm_password']) ? $errors[] = 'You forgot to enter Account Confirm Password.'	: '';
			
			if(empty($_POST['email']) == false){

				filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ? '' : $errors[] = 'You Input Invalid Email Address.';

			}

			$new_password = MD5(SHA1($_POST['password']));

			if($this->pdo->selectData('tbl_users','password',$id) != $new_password){

				$_POST['password'] == $_POST['confirm_password']  ? '' : $errors[] = 'Password and Confirm Password did not match.';

			}


			$countUsers = $this->pdo->countData($this->table,array('username'=>$_POST['username']));
			$username 	= $this->pdo->selectData($this->table,'username',$id);
			
			$countEmail = $this->pdo->countData($this->table,array('email'=>$_POST['email']));
			$email  	= $this->pdo->selectData($this->table,'email',$id);
				
			($countUsers == true &&  $username == $_POST['username']) OR
			($countUsers == false && $username != $_POST['username']) ? ''	: $errors[] = 'Username is already used.';
			
			($countEmail == true &&  $email == $_POST['email']) OR
			($countEmail == false && $email != $_POST['email']) ? ''	: $errors[] = 'Email is already used.';
			
			$fields =  array(
				'name'		=>$_POST['name'],
				'username'	=>$_POST['username'],
				'password'	=>$_POST['password'],
				'confirm_password'	=>$_POST['confirm_password'],
				'email'		=>$_POST['email'],
				'type'		=>$_POST['type'],
				'active'	=>$_POST['active']
				);
			$field  =  array(
				'name'		=>$_POST['name'],
				'username'	=>$_POST['username'],
				'password'	=>$new_password,
				'email'		=>$_POST['email'],
				'type'		=>$_POST['type'],
				'active'	=>$_POST['active'],
				'date_modified'=> date(DATE_FORMAT)
				);


			if (empty($errors)) { 	

				$where = $id;

				if( $this->pdo->updateData($this->table,$field,$where)){
  
					$this->obj->redirect(URL_ROOT.'users/active');
				
				}

			}else{

      			$data['errors'] = $errors;
    
    		}


		}else{
			
				$fields =  array(
				'name'		=> $this->pdo->selectData('tbl_users','name',$id),
				'username'	=> $this->pdo->selectData('tbl_users','username',$id),
				'password'	=> $this->pdo->selectData('tbl_users','password',$id),
				'confirm_password'	=> $this->pdo->selectData('tbl_users','password',$id),
				'email'		=> $this->pdo->selectData('tbl_users','email',$id),
				'type'		=> $this->pdo->selectData('tbl_users','type',$id), 
				'active'	=> $this->pdo->selectData('tbl_users','active',$id) 
				);
		}



		$this->view('include/header',$data);
		$this->view('users/form',$data,$fields);
		$this->view('include/footer');
		
	} // end of update
	

	public function logout()
	{
		$this->obj->logout();

	} //end of logout


	public function change_password()
	{
		
		$this->obj->is_loggedin();
		$data['title']	= 'Change Password';	   
 
 		$id = array('id' => $_SESSION[ID]);


		if (isset($_POST['submit']))
		{

			empty($_POST['current_password']) ?		$errors[] = 'You forgot to enter Current Password.'	: '';
			empty($_POST['new_password']) ?		$errors[] = 'You forgot to enter New Password.'	: '';
			empty($_POST['confirm_password']) ?		$errors[] = 'You forgot to enter Confirm Password.'	: '';

				
			$current_password = $this->pdo->selectData('tbl_users','password',$id);
			$currentpassword = MD5(SHA1($_POST['current_password']));
			$new_password = MD5(SHA1($_POST['new_password']));
		 
				$fields =  array(
					'current_password'	=> '',
					'new_password'		=> '',
					'confirm_password'	=> ''
				 
				);
			 
				$field  =  array('password'	=>$new_password);

				if (empty($errors)) { 	

					if($current_password != $currentpassword){
						$errors[] = 'You Input Wrong Current Password.' ;
						$data['errors'] = $errors;

					}else if($current_password == $new_password){
						$errors[] = 'Current Password and New Password match.';
						$data['errors'] = $errors;

					}else if($_POST['new_password'] != $_POST['confirm_password']){
						$errors[] = 'New Password and Confirm Password did not match.';
						$data['errors'] = $errors;

					}else{
						$where = $id;

						if( $this->pdo->updateData($this->table,$field,$where)){
							$data['success'] = 'Change Password Successfully';
							 
						}
					}

					 
				}else{

					$data['errors'] = $errors;

				}
		}else{
			
				$fields =  array(
					'current_password'	=> '',
					'new_password'		=> '',
					'confirm_password'	=> ''
				 
				);
		}


		$this->view('include/header',$data);
		$this->view('users/change_password',$data,$fields);
		$this->view('include/footer');
		
	} // end of change password



	public function login()
	{
		
	 
	 
	 
		// $this->view('login-validation/index');
		$this->view('login');
		 
		
	} // end of login validation


}

 